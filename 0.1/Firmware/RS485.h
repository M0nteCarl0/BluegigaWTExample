#include "stm32f4xx_usart.h"
#include "stm32f4xx_gpio.h"
#include "Stack.h"
#include "Commands.h"
#include "misc.h"

#pragma once

#ifdef __cplusplus
 extern "C" {
#endif 
   
  void InitRS485(void);
  void IOInit(void);
  void InteruptRS485(void);
  void RS485DataProcess(void);
#ifdef __cplusplus
 }
#endif