
#pragma once
#include "stm32f4xx.h"
#include "Stack.h"
#include "Time.h"
//#include "MyUtilite.h"
/******************************************************************************/


/*****************************************************************************/
enum ADDRESF//������ ��� ������
{
  TRIGER_FLASH_ADDR     = 120,
  SERIAL_NUMBER_FLASH   = 140
  
  
  
};
/*****************************************************************************/


/*****************************************************************************/
typedef  enum EComamnd //�������
{
  
  WRITE_SiPM_THERSH       = 0x1,//����� ������� SimP
  WRITE_CONTROL_DOZ       = 0x2, //�������� ������� ���������
  START_STATISTIC         = 0x3, //�����, �������� ���������� ���, ���, �������, ��� ��� 13������� ����������
  CLEAR_STATE_WORD        = 0x4, //�������� ��������� �����
  SET_PARAM_OSCIL         = 0x5, //������ ��������� ������������
  SET_WAIT_OSCIL          = 0x6, //������ �������� ��������� ������������	������ �� ��������� ������ ��������, ���������� ������
  WRITE_SiMP_VOLTAGE      = 0x7, //������ ���������� �� SiPM
  WRITE_SERIAL            = 0x3F,//������ ������ ������ (FLASH)
  READ_SW_HW              = 0x40,//������ ��� � ������ ������
  READ_SiMP_THERSH        = 0x41,//����� �������� (FLASH)
  READ_CONTROL            = 0x42,//������� ����������
  READ_COMPLETE_CONV      = 0x43,//��������� ����������� �� ��������� ����������
  READ_STATE_WORD         = 0x44,//������ ��������� ����� ���������
  READ_OSC_PARAMS         = 0x45,//������ ��������� ������������
  READ_STATE_WORD_OSC     = 0x46,//������ ��������� ����� ������������ 
  READ_SiMP_VOLTAGE       = 0x47,//������ ���������� �� SiPM
  READ_PARAMS_MESURED_EXP = 0x50,//��������� ���������� ����������  
  READ_STATE_PER_CHANEL   = 0x51,//��������� ���������� ���, ���, �������, ��� ��� 13������� ����������
  READ_CURRENT_ADC_VALUES = 0x52,//������ ������� �������� ADC 13������� ����������
  READ_AMPLITUDES_OSC     = 0x53,//������ ��������� ������������
  READ_ONE_ADC_CH         = 0x7E, //������ ���� ����� ���(�����������������!)
  SET_CS_PIN              = 0x61,
  RESET_CS_PIN            = 0x62,
  READ_SERIAL             = 0x7F, //������ ����� ������ (FLASH)
  FORCE_START_STATIC      = 0x8,//������ ���������� �������� �������(2048 * 13)
  READ_ACC_BUFFER         = 0x7A,//��������� ������ ������� ������  (���������������!) 
  RESERVED                = 0xFF //��������� �������� 
  
  
  
}Command;
/*****************************************************************************/
typedef enum EStaticState
{
  
  InProgress    = 0x1,//� ��������
  Complete      = 0x0,//����������
  WatiMesure    = 0x2,  //����� ������� ������� - ������ �� ��������� ������ ��� �� ��������� ������, ������� - �� ����� ��� ��������� 
  TimeOut       = 0x3
}StaticState;
/*****************************************************************************/

/*****************************************************************************/
typedef enum ECurrentTriger //������� ������� ��� ������� ��������
{
  SIPME = 0x1,
  EXTERNAL_EXPE = 0x2
  
}CurrentTriger;
/*****************************************************************************/
 typedef struct SamplerAcc//������ �������(��������)
 {
   
   uint16_t Sample[1024];//�������
 }SamplerAcc;
/*****************************************************************************/
 typedef struct SOscilParams//��������� �������������
 {
   
   uint8_t ScaleinPixels;//������� � ��������
   uint8_t FilterInPixels;//������ � ��������
   uint8_t StateWord;     // ��������� �����
   StaticState  OscState;
   CurrentTriger SourceOfSignal;
   
 }OscilParams;
 
 /*****************************************************************************/
 typedef enum Exposition
{
  NOT_WORK = 0x0,
  WORK = 0x1
}Exposition;
 
 
 /*******UPDATED*************************************************************/


enum ContrREgValue // ������� ����������
{
	ExtSignal = 1,
	SIMP = 2

};
 /*******UPDATED*************************************************************/

enum  StateWordBits //1 -  ����� ����� 0 -  ���
{

	PhotodiodEventComlete = 0,//�������� � ���� �����
	ExternalEventComlete = 1,//������� ������ - ���� �����
	ExpositionInProgress = 2 //����������  ����� ����� - ������

	
};

 /*******UPDATED*************************************************************/

enum  OscStateWordBits // ������� ��������� ����� ����������� 
{

	StartWaitExposistion = 0,//��� �������� �� ������� 0�06,��� ��������� �� ���������� ������

	Measure = 1              //���������  ��� �������� �� ������ ���������� ��� �� ������� 0�08 ,��� ��������� �� ������� 0�06


	
};
 
 
typedef struct ExternalTrigerTime
{
  
  uint32_t Begin;
  
  uint32_t MeasuredTime;
  
}ExternalTrigerTime;

/*****************************************************************************/

 
 typedef struct ExpositionParams//��������� ����������
 {
   uint32_t  Ciclic_Count_Bef_PWR; //���������� ����� ��� �������
   uint32_t MeasuredTime;          //�����
   uint32_t Begin;                 //����� - ������
   uint32_t End;                   // �����  - �����
   uint32_t CountSamplers;
   uint64_t Count;
  Exposition  ExpoWork;//To delete
   StaticState Expostate;
 }ExpositionParams;
 /*****************************************************************************/
 
  typedef struct StatisticParams//��������� ����������
 {
   uint32_t MeasuredTime;              //����� ��������� 
   uint32_t TimeOut;                   //�������
   uint32_t Begin;                     //����� - ������
   uint32_t End;                       // �����  - �����
   StaticState StateOfMeasureStatistic;
   Exposition WorkState;
 }StatisticParams;
 
 

 
/*****************************************************************************/
typedef  struct  SDozparams //��������� ���������(52 KB)
{
  
  uint8_t     m_SourceOscp;         // 1 - Trigger
                                       // 2 - Trigger + Constant Filte                                     // 4 - Integrator
  uint8_t     m_FilterOscp ;         // 0 - 127
  uint8_t     m_StatusOscp ;                                         // 0bit - 1 == Wait is doing                                     // 1bit - 1 == Measuremenr was done
  uint32_t m_CoverTime ;
  StaticState StatState;        //��������� ���������(���������)
  uint16_t MaxPerChannel[12];   //������������ �� �����
  uint16_t MinPerChannel[12];   //����������� �� �����
  uint16_t AvgPerChannel[12];   //������� �� �����
  uint32_t RMSPerChannel[12];   //������������������ 
  uint32_t SummPerChannel[12];  //����� �� �����(����)
  uint16_t CurrentPerChannel[12];//������� ��������
  CurrentTriger  TrigEvent;      //������
  ExternalTrigerTime  TimeExt;
  SamplerAcc BufferSampler[12];  //������ �������
  uint16_t SiMP_Thershold;       //����� ������������
  uint8_t SiMP_Voltage;         //����������
  uint16_t SiMP_CurrentValue;    //������� ��������
  uint16_t SiMP_CurrentValue_with_Filter; //������� �������� ����� ������������
  uint8_t StateWordOsc;                  //���������� �����(�����������)
  uint8_t StateWord;                     //��������� ����� ���������
  uint8_t ControlReg;                   //����������� �����(
  OscilParams OcsParam;                 //����������
  Command     CurrentCommand;           //������ �������
  ExpositionParams ExpParams;           //��������� �����
  StatisticParams StaticParams;         //�������� ������
  //ExternalTrigerTime  TimeExt;
  uint16_t CounterIt;                   //����� �������
  bool Trigered;
}Dozparams;

#ifdef __cplusplus
 extern "C" {
#endif 
/*****************************************************************************/
void InitFunctorQ(void);             //����������
void ExeComand(uint8_t Index,uint8_t* Data,int Number);

inline void  SetBitf(uint8_t* Bitfeil,uint8_t Pos)
{
  
*Bitfeil|=( 1 << Pos);
  
};


inline void   ClearBitf(uint8_t* Bitfeil,uint8_t Pos)
{
  
*Bitfeil&=~( 1 << Pos);
  
};



inline u8   TestBitf(uint8_t* Bitfeil,uint8_t Pos)
{
  
 return   *Bitfeil &( 1 << Pos);
  
};
/***************************************************************************/
//void Exposition(void);
/*****************************************************************************/
void ReadVersion(uint8_t *Buff, int N);//������ ��� � ������ ������
void ReadSiMP_Thershold(uint8_t *Buff, int N);//������ ����� SiMP
void ReadControl(uint8_t *Buff, int N);       //������ ���������� ������� ���������
void ReadStatIsDone(uint8_t *Buff, int N);    //������ �� ���������� �� ����� ����������
void ReadState(uint8_t *Buff, int N);         //������ ��������� ����� ���������
void ReadOscParams(uint8_t *Buff, int N);     //������ ��������� �����������
void ReadStateOsc(uint8_t *Buff, int N);      //������ ��������� ����� ������������ 
void ReadSiMP_Voltage(uint8_t *Buff, int N);  //������ ���������� �� SiMP 
void ReadExpositionParams(uint8_t *Buff, int N);//��������� ���������� ����������
void ReadStaticPerChannel(uint8_t *Buff, int N);//��������� ���������� ���, ���, �������, ��� ��� 13������� ����������
void ReadADCValues(uint8_t *Buff, int N);       //������ ���(�������) �������� �������  
void ReadAmpsOsc(uint8_t *Buff, int N);         //������ ��������� ������������ 
void ReadModuleNumber(uint8_t *Buff, int N);
void ReadADCperCh(uint8_t *Buff, int N);
void ReadAccBuffer(uint8_t *Buff, int N); //��������� ����  ������ ������� ������
void ReadExternalTrigerTime(uint8_t *Buff, int N);
/*******************************************/

void WriteSiMP_Thershold(uint8_t *Buff, int N);//�������� ����� ������������ �������� SiMP
void WriteControlReg(uint8_t *Buff, int N);    //�������� ����������� ������� ���������
void StartStatist(uint8_t *Buff, int N);       //������ ��������� ����������
void ClearStateWord(uint8_t *Buff, int N);     //�������� ��������� �����
void WriteOscParams(uint8_t *Buff, int N);     //�������� ��������� �����������
void WaitMeasureOsc(uint8_t *Buff, int N);     //������ �������� 
void WriteSiMP_Voltage(uint8_t *Buff, int N);
void WriteModuleNumber(uint8_t *Buff, int N);
void Force_Measure(uint8_t *Buff, int N);
void Set_CS_PIN(uint8_t *Buff, int N);
void Reset_CS_PIN(uint8_t *Buff, int N);
void ADC_InjectMulti(uint8_t NumADC);
void ADC_Interupt(void);
void ADC_DMA_GET(void);
void ADC_TimeBased(void);
/*******************************************/
uint8_t CRC8 (uint8_t Crc, uint8_t data);
void MakeCRC8 (uint8_t *Data, int N, uint8_t *Crc);
uint16_t MakeCRC16 (uint8_t *Block, int Len);
void ClearIterator(void);
void Init(void);
void GetExpTrigerState(void);
void GetSiMPTrigerLevel(void);
uint8_t MeasureMeanRmsOne (void);
void CheckTrigerEvents(void);
void DoneMeasure(void);
void ComputeStatistic(Dozparams* handler);
void ComputeTrueRMS(Dozparams* handler);
void CheckStatisticTimeOut(void);
void InitDozimetrP(Dozparams* handler);
void DeviceConfInit(void);
void WordtoBytes(u16 Source ,u8 Dest1,u8 Dest2);
void  BytestoWord(u16  Dest,u8 Source1,u8 Source2);
void RestroreState(void);
void SaveState(void);
/*******************************************/
void RS485_Enable ();
void RS485_Disable ();
void TestStructure(void);
/*****************************************************************************/
 #ifdef __cplusplus
 }
#endif