#pragma once
#include "stm32f4xx.h"
typedef  struct PHeader
{
  uint8_t iAddress;
  uint8_t  iSender;
  uint32_t wSize;
  uint16_t wCRC16;
  
}PHeader;

 typedef struct PData
{
  uint8_t* iData;
  uint16_t wCRC16;
   
}PData;



typedef struct ProtocolPacket
{
  PHeader Header;
  PData Data;
  
  
  
}ProtocolPacket;

class IO
{
  
public:
  IO();
  IO( uint32_t SizeQ);
bool InputByte(uint8_t Source);
bool OutputByte(uint8_t Source); 
bool InputBytes(uint8_t* Source);
bool OutputBytes(uint8_t* Source); 
void MakePacket(ProtocolPacket& Source);
void InputPacket(ProtocolPacket& Source);
void SendDMA(ProtocolPacket& Source);
void ReceiveDMA(ProtocolPacket& Source);
 ~IO(); 
  bool TransferComplete;
  bool ReseiveComplete;
private:
 uint32_t IndexTx;
 uint32_t IndexRx;
 uint32_t Size; 
  bool RxOverFlow;
  bool RxComplete;
  bool TxComplete;
  uint8_t* Rx;
  uint8_t* Tx;
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
};