/******************************************************************************
         Bluetooth Module Driver version 0.1A
               Molotaliev A.O
            

Example:

 Driver_WT11 Bluetooth  = Driver_WT11();
 
 Bluetooth.Open();
 
 Bluetooth.Command(const char* Commmand);
 Bluetooth.Read(char Data,size_t Lenth);
 
 Bluetooth.Write(char Data,size_t Lenth);
 
 
 Bluetooth.Close(); is equal Bluetooth.~Driver_WT11();
 
 




*******************************************************************************/

#define BUFFER_SIZE_TX  128
#define BUFFER_SIZE_RX  1024

#define DRIVER_VERSION 0x01A

enum  Driver_WT11_STATE
{
Driver_WT11_STATE_SUCCESFULLY,
Driver_WT11_STATE_READY,
Driver_WT11_STATE_CONNECTED,
Driver_WT11_STATE_DISCONNECTED,
};



enum  IWRAP_Mode
{
IWRAP_Mode_COMMAND,
IWRAP_Mode_DATA
};



class Driver_WT11
{

public:
Driver_WT11();
~Driver_WT11();

void Open();
void Close();
void  Command(const char* Commmand);
void Write(const char* Data,u16 DataCount);
void Write(const char Data);
void Read(char* Data,u16 DataCount);
void Read(char* Data);
private:
IWRAP_Mode  _Mode;
Driver_WT11_STATE _InternalState;
char _Command[];
char _DataBufferTX[BUFFER_SIZE_TX];
char _DataBufferRX[BUFFER_SIZE_RX];
u16 _DataCounterTX;
u16 _DataCounterRX;
Driver_WT11_STATE ReadState(void);
IWRAP_Mode        ReadMode(void);
void ResetDataCounters(void);



}