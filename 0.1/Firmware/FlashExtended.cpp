#include "FlashExtended.hpp"


#define ADDR_FLASH_SECTOR_0     ((uint32_t)0x08000000) /* Base @ of Sector 0, 16 Kbytes */
#define ADDR_FLASH_SECTOR_1     ((uint32_t)0x08004000) /* Base @ of Sector 1, 16 Kbytes */
#define ADDR_FLASH_SECTOR_2     ((uint32_t)0x08008000) /* Base @ of Sector 2, 16 Kbytes */
#define ADDR_FLASH_SECTOR_3     ((uint32_t)0x0800C000) /* Base @ of Sector 3, 16 Kbytes */
#define ADDR_FLASH_SECTOR_4     ((uint32_t)0x08010000) /* Base @ of Sector 4, 64 Kbytes */
#define ADDR_FLASH_SECTOR_5     ((uint32_t)0x08020000) /* Base @ of Sector 5, 128 Kbytes */
#define ADDR_FLASH_SECTOR_6     ((uint32_t)0x08040000) /* Base @ of Sector 6, 128 Kbytes */
#define ADDR_FLASH_SECTOR_7     ((uint32_t)0x08060000) /* Base @ of Sector 7, 128 Kbytes */
#define ADDR_FLASH_SECTOR_8     ((uint32_t)0x08080000) /* Base @ of Sector 8, 128 Kbytes */
#define ADDR_FLASH_SECTOR_9     ((uint32_t)0x080A0000) /* Base @ of Sector 9, 128 Kbytes */
#define ADDR_FLASH_SECTOR_10    ((uint32_t)0x080C0000) /* Base @ of Sector 10, 128 Kbytes */
#define ADDR_FLASH_SECTOR_11    ((uint32_t)0x080E0000) /* Base @ of Sector 11, 128 Kbytes */
#define ADDR_FLASH_SECTOR_END   ((uint32_t)0x080FFFFF)  /*End User writable flash area*/
const uint16_t SectorBegin[] = {FLASH_Sector_0,FLASH_Sector_1, FLASH_Sector_2, FLASH_Sector_3,FLASH_Sector_4 , FLASH_Sector_5, FLASH_Sector_6, FLASH_Sector_7, FLASH_Sector_8, FLASH_Sector_9, FLASH_Sector_10, FLASH_Sector_11};
const uint32_t StartAddres[]  = {ADDR_FLASH_SECTOR_0,ADDR_FLASH_SECTOR_1,ADDR_FLASH_SECTOR_2,ADDR_FLASH_SECTOR_3,ADDR_FLASH_SECTOR_4,ADDR_FLASH_SECTOR_5,ADDR_FLASH_SECTOR_6,ADDR_FLASH_SECTOR_7,ADDR_FLASH_SECTOR_8,ADDR_FLASH_SECTOR_9,ADDR_FLASH_SECTOR_10,ADDR_FLASH_SECTOR_11, ADDR_FLASH_SECTOR_END};
/**************************************************************************/
FlashIO::FlashIO(void):_DataWriteSuccesfull(false),_VoltageRange(VoltageRange_3),_CheckWritedData(true)
{
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_CRC, ENABLE);
  _Latency  = *( uint8_t *)ACR_BYTE0_ADDRESS; 
  _PrefetchBufferState   = static_cast<FunctionalState>(FLASH->ACR & FLASH_ACR_PRFTEN);
  _DataCacheState        = static_cast<FunctionalState>(FLASH->ACR & FLASH_ACR_DCEN);                  
  _InstructionCacheState = static_cast<FunctionalState>(FLASH->ACR & FLASH_ACR_ICEN);;
  
}
/**************************************************************************/
FlashIO::~FlashIO(void)
{
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_CRC, DISABLE);
}
/**************************************************************************/
u32  FlashIO :: GetHighAdressSector(uint8_t Sector)
{
  if(Sector <= 11)
  {
    return StartAddres[Sector+1];
    
  }
  else
  {
    
    return FLASH_ERROR_BAD_SECTOR_INDEX;
    
  }
  
};
/**************************************************************************/
u32   FlashIO::  GetFarAdressSector(uint8_t Sector)
   {
     
     
     if(Sector <= 11)
  {
    return StartAddres[Sector];
    
  }
  else
  {
    
    return FLASH_ERROR_BAD_SECTOR_INDEX;
    
  }
     
     
   }
/**************************************************************************/
   uint8_t  FlashIO ::  GetSectorIndex(u32 Addres)
   {
     
     
      u8 SectorIndex = 0;  
       do
       {
 u32 RealAdress = StartAddres[SectorIndex ];  ;
 u32 EndAdress =  StartAddres[SectorIndex + 1]; 
      if(Addres  >=  RealAdress && Addres < EndAdress )
      {
       goto END;
       }  
       SectorIndex++; 
       }while(SectorIndex < 12);
 END:
       return  SectorIndex;
     
     
     
   }
/**************************************************************************/
   uint32_t  FlashIO :: GetSectorSize(u32 Addres)
   {
     
     u8 SectorIndex = GetSectorIndex(Addres);
     u32 Far =  GetHighAdressSector(SectorIndex);
     u32 Near = GetFarAdressSector(SectorIndex);
     
     if(Far!= 0 && Near!= 0)
     {
     return Far  - Near;
     }
     else
     {
      return FLASH_ERROR_BAD_SECTOR_INDEX; 
       
     }
     
   }







/**************************************************************************/
 uint16_t FlashIO :: GetSectorID(u32 Addres)
 {
   
   
  
 u8 SectorIndex = 0;  
       do
       {
 u32 RealAdress = StartAddres[SectorIndex ];  ;
 u32 EndAdress =  StartAddres[SectorIndex + 1]; 
      if(Addres  >=  RealAdress && Addres < EndAdress )
      {
       goto END;
       }  
       SectorIndex++; 
       }while(SectorIndex < 12);
 END:
       return  SectorBegin[SectorIndex];
 } 
 /**************************************************************************/
  void     FlashIO ::   SetVoltageRange(uint8_t VoltageRange)
 {          
  _VoltageRange = VoltageRange;
                     
 };
 /**************************************************************************/
 void     FlashIO :: SetLatency(uint32_t  Latency)
 {          
    _Latency  = Latency;
    FLASH_SetLatency(Latency);
                     
 };
/**************************************************************************/
 void     FlashIO :: SetPrefetchBufferState(FunctionalState PrefetchBufferState)
 {          
    _PrefetchBufferState  = PrefetchBufferState;
    FLASH_PrefetchBufferCmd(PrefetchBufferState);
                     
 };
 /**************************************************************************/
 
  void  FlashIO :: SetInstructionCacheState(FunctionalState InstructionCacheState)
 {          
    _InstructionCacheState  = InstructionCacheState;
    FLASH_InstructionCacheCmd(_InstructionCacheState);
                     
 };
 /**************************************************************************/
  void  FlashIO :: SetDataCacheState(FunctionalState  DataCacheState)
 {          
    _DataCacheState  =  DataCacheState;
    FLASH_DataCacheCmd(_DataCacheState);
                     
 };
 /**************************************************************************/
  void  FlashIO :: InstructionCacheReset(void)
 {          
    FLASH_InstructionCacheReset();
                     
 };
 /**************************************************************************/
  void  FlashIO ::DataCacheReset(void)
 {          
   FLASH_DataCacheReset();                  
 };

  /**************************************************************************/
 
  void  FlashIO ::FormatFlash(void)
 {          
   FLASH_EraseAllSectors(_VoltageRange);
 };
 /**************************************************************************/

uint8_t  FlashIO :: GetVoltageRange(void)
{
  return _VoltageRange;
  
}
/**************************************************************************/
uint32_t   FlashIO :: GetLatency(void)
{
    
    return _Latency;
    
}
/**************************************************************************/
FunctionalState  FlashIO :: GetPrefetchBufferState(void)
{
    return _PrefetchBufferState;
    
}
/**************************************************************************/
FunctionalState  FlashIO :: GetInstructionCacheState(void)
{ 
    return _InstructionCacheState;
    
}
/**************************************************************************/
FunctionalState FlashIO :: GetDataCacheState(void)
{
    
   return _DataCacheState;
    
    
}
/**************************************************************************/
 void FlashIO ::  SetCheckDataState(FunctionalState CheckState)
 {
   
 _CheckWritedData = (bool)CheckState;  
   
 }
/**************************************************************************/
 FunctionalState FlashIO ::  GetCheckDataState(void)
 {
   
  return  (FunctionalState)_CheckWritedData;   
   
 }

