#include "stm32f4xx.h"

void InitADC(void);
void InitADC_GPIO(void);
void ADCChanellConfig(void);
void ConfigADC_DMA(void);
void ADCStructInit(void);
//#ifdef _ADC_INTERUPT_MODE || _ADC_INJECT_MODE  
void InteruptADC(void);
//#endif