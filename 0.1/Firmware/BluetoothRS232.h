#ifndef __WT11_BT_
#define __WT11_BT_
#include "stm32f4xx.h"
#ifdef __cplusplus
 extern "C" {
#endif 
void InitRS232(void);
void EnableTransmit(void);
void ResetBluetooth(void);
#ifdef __cplusplus
}
#endif

#endif 
