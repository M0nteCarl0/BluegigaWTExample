#include "Timers.h"

uint16_t ComputeTimerMs(uint16_t ms)
{
   uint16_t Freq;
  
  if(ms >0)
  {
 Freq = 1/1000;
  }
   return  SystemCoreClock  / Freq * ms;
}
/*****************************************************************************/

void InitMsTimer(void)
{
  InitTimer3_Time_Counter();
  InitTimer4_50us();

};
/*****************************************************************************/
void InitTimer3_Time_Counter(void)
{

NVIC_InitTypeDef NVT;
NVT.NVIC_IRQChannel = TIM3_IRQn;
NVT.NVIC_IRQChannelCmd =ENABLE;
NVT.NVIC_IRQChannelPreemptionPriority = 0;
NVT.NVIC_IRQChannelSubPriority = 0;
NVIC_Init(&NVT);
  
  
  
  
  TIM_TimeBaseInitTypeDef T1;
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3,ENABLE);
  RCC_ClocksTypeDef RCC_Clocks;
  RCC_GetClocksFreq(&RCC_Clocks);

  T1.TIM_ClockDivision = TIM_CKD_DIV2;//42 mhz;
  T1.TIM_CounterMode = TIM_CounterMode_Up;
  T1.TIM_Prescaler = 42000 - 1; //42 000 000 000 / 42 000 = 1khz(1ms)
  T1.TIM_Period = 50; // 65535 ms = 65.635s = 1m.5.64s ;
  TIM_TimeBaseInit(TIM3,&T1);
  TIM_ITConfig(TIM3,TIM_IT_Update,ENABLE);
  TIM_Cmd(TIM3,ENABLE);
  
  
};
/*****************************************************************************/
void InitTimer4_50us(void)
{
//  #ifdef _TIMER_INTERUPT
NVIC_InitTypeDef NVT;
//  #endif
  TIM_TimeBaseInitTypeDef T1;
  
  
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2,ENABLE);
  RCC_ClocksTypeDef RCC_Clocks;
  RCC_GetClocksFreq(&RCC_Clocks);
   
////  #ifdef _TIMER_INTERUPT
NVT.NVIC_IRQChannel = TIM2_IRQn;
NVT.NVIC_IRQChannelCmd =ENABLE;
NVT.NVIC_IRQChannelPreemptionPriority = 0;
NVT.NVIC_IRQChannelSubPriority = 0;
NVIC_Init(&NVT);
//////  #endif  
//   
  T1.TIM_ClockDivision = TIM_CKD_DIV4   ;//84/4 = 21mhz
  T1.TIM_CounterMode = TIM_CounterMode_Up;
  T1.TIM_Prescaler  = 21 - 1;//1 * 10^6  tiks in second = 1 mhz = 1us;
  T1.TIM_Period =  50;// 50 * 10^6 tiks = 50 us ;
  TIM_TimeBaseInit(TIM2,&T1);
  TIM_ITConfig(TIM2,TIM_IT_Update,ENABLE);
  //TIM_SetCounter(TIM2, 0);
 
 // TIM_ARRPreloadConfig(TIM2,ENABLE);
  TIM_SelectOutputTrigger(TIM2,TIM_TRGOSource_Update);
  
//  #ifdef _ADC_INJECT_MODE 
//  TIM_SelectOutputTrigger(TIM3,TIM_TRGOSource_Update);
//  #endif
  TIM_Cmd(TIM2,ENABLE);
  
};
/*****************************************************************************/
