#include "BlueGigaComandParser.h"
#include "stm32f4xx.h"
const char* connect_mode[] = {"RFCOMM","HFP","HFP-AG","A2DP","AVRCP","HID","L2CAP","PBAP","OPP","HSP","HSP-AG","HDP"};
const char* events[] = {"RING","NO CARRIER","READY."};
extern  int Positions[3];


/*************************************************************************************************************/

IWEvents BlueGigaComandParser::GetEvents(uint16_t Position)
{

 __disable_irq(); 
 __disable_fiq();
 
string EventS(RxQ);
std::size_t foundNewLine =  EventS.rfind("\r\n");

int Max = -1;;
IWEvents CurrentEvent ;
 for(int8_t Event = 0; Event < 3; Event++)
	{
		if(EventS.rfind(events[Event],Position)!=::string::npos)
      {
			Positions[Event] =EventS.rfind(events[Event],Position);
      }
      else
      {
         
         Positions[Event] = -10*(Event+1);
         
      }
			
		
	}

  for(int8_t EventE = 0; EventE < 3; EventE++)
	{
		if(Positions[EventE] > Max)
		{
			Max = Positions[EventE];
			CurrentEvent =(IWEvents)EventE;
	

			
		}
	}
 __enable_irq(); 
 __enable_fiq();
 return CurrentEvent;
}



/*************************************************************************************************************/
void BlueGigaComandParser:: AddNewLine(char* Source)
{
	strcat(Source,"\n");

};
/*************************************************************************************************************/
void BlueGigaComandParser::Call(SBT_Adr &DeviceAdres,uint8_t Chanell,uint16_t Mtu,uint16_t PayloadSize)//RFCOMM
{
   InsertCommand(CALL);
   AddTargetAddress(DeviceAdres);
   AddNewLine(TxQ);
   #ifdef _DEBUG 
   //printf("%s\n",TxQ);
   #endif
}
/*************************************************************************************************************/
/*************************************************************************************************************/
BlueGigaComandParser::BlueGigaComandParser(void)
{
   //memset(BaseAdress,0,BufferSize);
   TxQ = new char[BufferSize];
   memset(TxQ,0,BufferSize);
   RxQ = new char[BufferSize];
}
/*************************************************************************************************************/
BlueGigaComandParser::~BlueGigaComandParser(void)
{
   //printf("Destructrir\n");
   //delete RxQ;
   //delete TxQ;
}
/*************************************************************************************************************/
void BlueGigaComandParser:: BindRx( char Rx[])
{

   //Rx = RxQ;
	strcpy(RxQ,Rx);
#ifdef _DEBUG
	//printf("RXQ =  %s\n",RxQ);
#endif

}
/*************************************************************************************************************/
