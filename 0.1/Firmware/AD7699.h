#include "stm32f4xx.h"
#ifndef _ADC_AD7699_
#define _ADC_AD7699_
#ifdef __cplusplus
 extern "C" {
#endif 
void InitAD7699(void);
void EnableTransimtAD7699(void);
void DissableTransimtAD7699(void);
void WriteAD7699(uint8_t Value);
void ReadAD7699(uint16_t Value);
#ifdef __cplusplus
 }
#endif 
#endif

