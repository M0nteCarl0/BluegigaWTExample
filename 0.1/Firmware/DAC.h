#include "stm32f4xx.h"
#ifndef _DAC_MAX1932_
#define _DAC_MAX1932_
#ifdef __cplusplus
 extern "C" {
#endif 
#define CS_PORT GPIOC
#define CS_PIN  GPIO_Pin_9
#define CS_RCC  RCC_AHB1Periph_GPIOC
void InitDAC(void);
void EnableTransimt(void);
void DissableTransimt(void);
void WriteDAC(uint8_t Value);
#ifdef __cplusplus
 }
#endif 
#endif
/******************************************************************************
                             MAX1932 8 bit DAC Driver
How use:
InitDAC();
WriteDAC(128);
******************************************************************************/
   