
#include "Stack.h"

uint8_t MakeCRC (uint8_t *Data, int N);
//------------------------------------------------------------------------------
MyStack         m_StackR;
MyStack         m_StackT;
MyCommand       m_CommandR;

int m_FlgCheckCRC = 0;     // 1 - Control CRC;   0 - Dont Make and Control CRC
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void InitStack (MyStack *Stack)
{
  Stack->Begin  = 0;
  Stack->End    = 0;
  Stack->Flg    = 1;
};
//------------------------------------------------------------------------------
int PutCharStack (uint8_t Data, MyStack *Stack)
{
  int Begin = Stack->Begin;
  
  Begin++;
  if (Begin > NST-1){
    Begin = 0;
  }
  
  if (Begin == Stack->End){
    Stack->Flg = 0;
    return Stack->Flg;
  }
  
  Stack->Begin = Begin;
  
  Stack->Data [Stack->Begin] = Data;
  
  Stack->Flg = 1;
  return Stack->Flg;
};
//------------------------------------------------------------------------------
int GetCharStack (uint8_t *Data, MyStack *Stack)
{
  if (Stack->Begin == Stack->End){
    Stack->Flg = 0;
    return Stack->Flg;
  }
  
  Stack->End++;
  if (Stack->End > NST-1)
    Stack->End = 0;

  *Data = Stack->Data [Stack->End];      
      
  Stack->Flg = 1;
  return Stack->Flg;
};
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void InitCommand (MyCommand *Comd)
{
  Comd->JC    = 0;
  Comd->Bytes = 0;
  Comd->Flg   = 1;
};
//------------------------------------------------------------------------------
/*
int ReceiveCommandL (uint8_t *Buff, int *N, MyStack *Stack, MyCommand *Comd)
{
__disable_irq();    
  
  uint8_t Data;
  int Ret = 0;
  int i;
  
  while (GetCharStack (&Data, Stack)){

    if (Comd->JC == 0){
      if (Data == COMMAND_ID){
        Comd->Data [Comd->JC] = Data;
        Comd->JC++;
      }
    }
    else{
      Comd->Data [Comd->JC] = Data;
      Comd->JC++;
    }
    
    if ((Comd->JC > 1) && (Comd->Data [1] == Comd->JC)){
      for (i = 0; i < Comd->JC; i++){
        Buff [i] = Comd->Data [i];
      }
      *N = Comd->JC;
      
      Comd->JC = 0;
      
      Ret = 1;
      break;
    }
    
    if (Comd->JC > 1){
      if (Comd->Data [1] < Comd->JC || Comd->Data [1] > NCOMD){
        Comd->JC = 0;
        break;
      }
    }
  }
  
__enable_irq();    
  
  return Ret;
};
*/
//------------------------------------------------------------------------------
int ReceiveCommandL (uint8_t *Buff, int *N, MyStack *Stack, MyCommand *Comd)
{
__disable_irq();    
  
  uint8_t Data;
  int Ret = 0;
  int i;
  
   while (GetCharStack (&Data, Stack)){

      if (Comd->JC == 0){
                                                               // ID
        if (Data == COMMAND_ID){
          Comd->Data [Comd->JC] = Data;
          Comd->JC++;
        }
      }
      else{
         Comd->Data [Comd->JC] = Data;
         Comd->JC++;
      }
                                                               // ADDR
      if (Comd->JC == 2){
         if (Comd->Data [1] != EXPO_ADR){
            Comd->JC = 0;
            break;       
         }
      }
                                                               // HEADER
      if (Comd->JC == 5){
         uint8_t CRC_Header;
         if (m_FlgCheckCRC){
            CRC_Header = MakeCRC (Comd->Data, 4);
/*           
            CRC_Header = CRC_Header ^ Comd->Data [0]; 
            CRC_Header = CRC_Header ^ Comd->Data [1]; 
            CRC_Header = CRC_Header ^ Comd->Data [2]; 
            CRC_Header = CRC_Header ^ Comd->Data [3]; 
            CRC_Header = (CRC_Header & 0x7F); 
*/            
         }
         else{
           CRC_Header = Comd->Data [Comd->JC-1];
         }
                                                               // CHECK CRC
         if (CRC_Header != Comd->Data [Comd->JC-1]){
            Comd->JC = 0;
            break;       
         }
         else{
             if (Comd->Data [3] == 5){
               for (i = 0; i < Comd->JC; i++){
                 Buff [i] = Comd->Data [i];
               }
               *N = Comd->JC;
               Comd->JC = 0;
               Ret = 1;
               break;
             }
         }
      }
                                                               // DATA
      if ((Comd->JC > 5) && (Comd->Data [3] == Comd->JC)){
//         int SUM = 0; 
//         for (i = 5; i < Comd->JC-1; i++){
//            SUM += (int) Comd->Data [i];
//         }
//         uint8_t CRC_Data = (uint8_t) (SUM & 0x007F);

                                                               // CHECK CRC
         if (0){
//         if (CRC_Data != Comd->Data [Comd->JC-1]){
            Comd->JC = 0;
            break;       
         }
         else{
            for (i = 0; i < Comd->JC; i++){
              Buff [i] = Comd->Data [i];
            }
            *N = Comd->JC;
            Comd->JC = 0;
            Ret = 1;
            break;
         }
      }
                                                               // ERROR-1
      if (Comd->JC > 3){
        if (Comd->Data [3] < Comd->JC || Comd->Data [3] > NCOMD){
          Comd->JC = 0;
          break;
        }
      }
                                                               // ERROR-2
      if (Comd->JC > 2){
        if (Comd->Data [Comd->JC-1] & 0x80){
          Comd->JC = 0;
          break;
        }
      }

   }
  
__enable_irq();    
  
  return Ret;
};
//------------------------------------------------------------------------------
int SendCommandL (uint8_t *Buff, int N, MyStack *Stack)
{
  int Ret = 1;
  int i;

  for (i = 0; i < N; i++){
    Ret = PutCharStack (Buff[i], Stack);
    if (Ret == 0)
      break;
  }

  return Ret;
};
//----------------------------------------------------------------------------
uint8_t MakeCRC (uint8_t *Data, int N)
//    ���������� CRC ����� �������� 
{
   int Summ = 0;
   for (int i = 0; i < N; i++){
      Summ += Data[i];
   }
   int SummH = (Summ >> 7) & 0x7F;

   uint8_t mCRC = (uint8_t) (SummH + (Summ & 0x7F));

   mCRC &= 0x7F;
   return mCRC;
};
//----------------------------------------------------------------------------
/*
uint8_t MakeCRC (uint8_t *Data, int N)
{
   uint8_t mCRC = 0xFF;
   for (int i = 0; i < N; i++){
      mCRC = mCRC ^ Data[i];
   }
   mCRC &= 0x7F;
   return mCRC;
};
*/
//----------------------------------------------------------------------------
void AddCRC (uint8_t *Data, int N)
{
   if (N < 5) return;

   Data [4] = MakeCRC (Data, 4);

   if (N > 6){
      Data [N-1] = MakeCRC (&Data[5], N-6);
   }
};
//----------------------------------------------------------------------------
int CheckCRC (uint8_t *Data, int N)
{
   if (N < 5) return 0;
   uint8_t mCRC;
/*
   mCRC = MakeCRC (Data, 4);
   if (mCRC != Data[4]) 
      return 0;
*/
   if (N > 6){
      mCRC = MakeCRC (&Data[5], N-6);
      if (mCRC != Data[N-1]) 
         return 0;
   }
   return 1;
};
//------------------------------------------------------------------------------
int ReceiveCommand (uint8_t *Buff, int *N)
{
   int Ret = ReceiveCommandL (Buff, N, &m_StackR, &m_CommandR);

   if (Ret && m_FlgCheckCRC){
      if (CheckCRC (Buff, *N))   
        return 1;
      else
        return 0;
   }
   return Ret;
};
//------------------------------------------------------------------------------
int SendCommand (uint8_t *Buff, int N)
{
   if (m_FlgCheckCRC)
      AddCRC (Buff, N);
   
   return SendCommandL (Buff, N, &m_StackT);
};
//------------------------------------------------------------------------------








