
#include "stm32f4xx.h"


#pragma once
#define   NST         1500
#define   NCOMD       1500
#define   COMMAND_ID  0x96                //  ������� ��� ������������ (����� ��� ������ ���������)
#define   EXPO_ADR    0x04   


typedef struct
{
  uint8_t   Data  [NST];
  int       Begin;
  int       End;
  int       Flg;
} MyStack;
//------------
typedef struct
{
  uint8_t   Data  [NCOMD];
  int       JC;
  int       Bytes;
  int       Flg;
} MyCommand;
#ifdef __cplusplus
 extern "C" {
#endif 
//------------------------------------------------------------------------------
void  InitStack (MyStack *Stack);
int   PutCharStack (uint8_t Data, MyStack *Stack);
int   GetCharStack (uint8_t *Data, MyStack *Stack);

void  InitCommand (MyCommand *Comd);
int   ReceiveCommandL (uint8_t *Buff, int *N, MyStack *Stack, MyCommand *Comd);
int   SendCommandL    (uint8_t *Buff, int N, MyStack *Stack);

int   ReceiveCommand (uint8_t *Buff, int *N);
int   SendCommand    (uint8_t *Buff, int N);


extern MyStack         m_StackR;
extern MyStack         m_StackT;
extern MyCommand       m_CommandR;
#ifdef __cplusplus
 }
#endif