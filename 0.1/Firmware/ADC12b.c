#include "ADC12b.h"

void InitADC(void)
{
  InitADC_GPIO();
  ADCStructInit();  
  ADCChanellConfig();  
//#ifdef _ADC_INTERUPT_MODE || _ADC_INJECT_MODE
  //InteruptADC();     
//#endif
  ConfigADC_DMA();                   
  ADC_Cmd(ADC1, ENABLE);        //Enable ADC1
  ADC_SoftwareStartConv(ADC1);
  //NVIC_EnableIRQ(DMA2_Stream0_IRQn);
//#ifdef _ADC_INJECT_MODE
  //:ADC_AutoInjectedConvCmd(ADC1,ENABLE);
//ADC_SoftwareStartInjectedConv(ADC1);
//#endif 

     
};



void InitADC_GPIO(void)
{
  //RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
 // RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);
  GPIO_InitTypeDef ADC_GP;
  ADC_GP.GPIO_Mode = GPIO_Mode_AN;
 // ADC_GP.GPIO_Pin = GPIO_Pin_0|GPIO_Pin_1|GPIO_Pin_2|GPIO_Pin_3|GPIO_Pin_4|GPIO_Pin_5|GPIO_Pin_6|GPIO_Pin_7;
 // GPIO_Init(GPIOA,&ADC_GP);
  //ADC_GP.GPIO_Pin = GPIO_Pin_1;
 // GPIO_Init(GPIOB,&ADC_GP);
  //ADC_GP.GPIO_Pin = GPIO_Pin_0|GPIO_Pin_1|GPIO_Pin_4|GPIO_Pin_5;
  ADC_GP.GPIO_Pin = GPIO_Pin_5;
  GPIO_Init(GPIOC,&ADC_GP);
 }
	
  void ADCStructInit(void)
 {
    
  ADC_InitTypeDef ADC_InitStruct;
  ADC_CommonInitTypeDef ADC_CommonInitStruct;  
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1,ENABLE);          // enable clocking of ADC1
  ADC_DeInit();
  ADC_CommonInit(&ADC_CommonInitStruct);
  ADC_CommonInitStruct.ADC_Mode                  = ADC_Mode_Independent;
  ADC_CommonInitStruct.ADC_Prescaler             = ADC_Prescaler_Div2;//18mhz
  ADC_CommonInitStruct.ADC_DMAAccessMode         = ADC_DMAAccessMode_Disabled;
  ADC_CommonInitStruct.ADC_TwoSamplingDelay      = ADC_TwoSamplingDelay_5Cycles;
  ADC_CommonInit(&ADC_CommonInitStruct);
  ADC_StructInit(&ADC_InitStruct);
  
 ADC_InitStruct.ADC_Resolution                  = ADC_Resolution_12b;
  //#ifdef ADC_INTERUPT_MODE ||_DMA_MODE || ADC_INJECT_MODE
  //ADC_InitStruct.ADC_ScanConvMode                = ENABLE;
 ADC_InitStruct.ADC_ScanConvMode                = DISABLE;
  //#endif
 ADC_InitStruct.ADC_ContinuousConvMode          = DISABLE;
 //ADC_InitStruct.ADC_ContinuousConvMode        = ENABLE ;
  ADC_InitStruct.ADC_DataAlign                   = ADC_DataAlign_Right;
//  #ifdef ADC_INTERUPT_MODE ||_DMA_MODE
 //ADC_InitStruct.ADC_NbrOfConversion             = 13;
//  #endif
  ADC_InitStruct.ADC_NbrOfConversion             = 1;
  //#ifdef _ADC_INJECT_MODE
// ADC_InitStruct.ADC_NbrOfConversion             = 4;
  //#endif
  
//  #ifdef ADC_INTERUPT_MODE ||_DMA_MODE 
ADC_InitStruct.ADC_ExternalTrigConv            = ADC_ExternalTrigConv_T3_TRGO;
ADC_InitStruct.ADC_ExternalTrigConvEdge        = ADC_ExternalTrigConvEdge_Rising;///ADC_ExternalTrigConvEdge_Rising;

//ADC_InitStruct.ADC_ExternalTrigConvEdge        = ADC_ExternalTrigConvEdge_Rising; 
//  #endif

  //ADC_DiscModeCmd(ADC1,DISABLE);
//ADC_InjectedSequencerLengthConfig(ADC1,4);
//ADC_ExternalTrigInjectedConvConfig(ADC1,ADC_ExternalTrigInjecConv_T2_TRGO);
//ADC_ExternalTrigInjectedConvEdgeConfig(ADC1,ADC_ExternalTrigConvEdge_Rising);
//ADC_AutoInjectedConvCmd(ADC1,ENABLE);
 
  
  
  
  
  
  
  ADC_Init(ADC1,&ADC_InitStruct);  

  //#ifdef _ADC_INJECT_MODE  
 
  //#endif
  
  
  //Total Measure time (132/36 * 13) = 47us
}

void ADCChanellConfig(void)
{
//  //#ifdef _ADC_INTERUPT_MODE ||_DMA_MODE
 // ADC_RegularChannelConfig(ADC1,ADC_Channel_0,1,ADC_SampleTime_480Cycles);      
  //ADC_RegularChannelConfig(ADC1,ADC_Channel_1,2,ADC_SampleTime_480Cycles);
  //ADC_RegularChannelConfig(ADC1,ADC_Channel_2,3,ADC_SampleTime_3Cycles);
//  ADC_RegularChannelConfig(ADC1,ADC_Channel_4,4,ADC_SampleTime_3Cycles);
//  ADC_RegularChannelConfig(ADC1,ADC_Channel_5,5,ADC_SampleTime_3Cycles);
//  ADC_RegularChannelConfig(ADC1,ADC_Channel_6,6,ADC_SampleTime_3Cycles);
//  ADC_RegularChannelConfig(ADC1,ADC_Channel_7,7,ADC_SampleTime_3Cycles);
//  ADC_RegularChannelConfig(ADC1,ADC_Channel_8,8,ADC_SampleTime_3Cycles);
//  ADC_RegularChannelConfig(ADC1,ADC_Channel_9,9,ADC_SampleTime_3Cycles);
// ADC_RegularChannelConfig(ADC1,ADC_Channel_10,10,ADC_SampleTime_84Cycles);
//  ADC_RegularChannelConfig(ADC1,ADC_Channel_11,11,ADC_SampleTime_3Cycles);
 // ADC_RegularChannelConfig(ADC1,ADC_Channel_14,12,ADC_SampleTime_480Cycles);
  ADC_RegularChannelConfig(ADC1,ADC_Channel_15,1,ADC_SampleTime_3Cycles);

};

void ConfigADC_DMA(void)
{
  //#ifdef _ADC_INTERUPT_MODE ||_DMA_MODE
 ADC_EOCOnEachRegularChannelCmd(ADC1,ENABLE);
 ADC_DMARequestAfterLastTransferCmd(ADC1, ENABLE);      
 ADC_DMACmd(ADC1, ENABLE);        //Enable ADC1 DMA
  //#endif
}

void InteruptADC(void)
{
////  #ifdef _ADC_INTERUPT_MODE
//ADC_ITConfig(ADC1,ADC_IT_EOC,ENABLE);
////  ADC_ITConfig(ADC1,ADC_IT_AWD|ADC_IT_JEOC| ADC_IT_OVR,DISABLE);
////  #endif
//  //#ifdef _ADC_INJECT_MODE
///ADC_ITConfig(ADC1,ADC_IT_JEOC,ENABLE);
// // #endif
//  //#ifdef _ADC_INTERUPT_MODE || _ADC_INJECT_MODE
//      NVIC_InitTypeDef  NVT;
//    NVT.NVIC_IRQChannel                     = ADC_IRQn;
//    NVT.NVIC_IRQChannelCmd                  = ENABLE;
//    NVT.NVIC_IRQChannelPreemptionPriority   = 0;
//    NVT.NVIC_IRQChannelSubPriority          = 0;
//    NVIC_Init(&NVT);
//    NVIC_EnableIRQ(ADC_IRQn);
// //#endif
}