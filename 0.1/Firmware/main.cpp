#include "stm32f4xx.h"
#include "BluetoothRS232.h"
#include "Commands.h"
#include "SysTickTimer.hpp"
#include "BlueGigaComandParser.h"
#include "DAC.h"
#include "AD7699.h"
//TESTED ON DISCOVERY F
//ProtocolPacket  Input;
//ProtocolPacket  Output;
uint8_t GG[21];
uint16_t ConvData[4];
u8 InputBuffer[3] = {128,64,32};
u8 OutputBuffer[3] = {0,0,0};
u8 CharBuff;  
IWEvents LastEvents;

BlueGigaComandParser Parcer;
bool DeviceConected = false;
bool _CommadMode = true;
 u16 DataCounter = 0;
char Set[] = "SET\n";
  int Positions[3];
 char DataBuff[512];
 char Commandbuff[128];
  char OutputD[512];
 int ComandLength ;
  u32   DataCounterTC = 0;
 void WriteComman(const char* Command)
 {
  ComandLength =   strlen(Command);
  strcpy(Commandbuff,Command);
  DataCounterTC = 0;
 // DataCounter = 0;
  USART_ITConfig(USART1,USART_IT_TC,ENABLE);
   
    
 };
 void ReadData( char* Data)
 { 
   //memset(Data,0,512);
   
    memcpy(Data,DataBuff,512);
    //Data =  DataBuff;
     //strcpy(DataBuff,Data);
 };
 
 
 
 
 
static  uint8_t         Buff [NCOMD];
int N = 0;
int Ret = 0;
uint16_t VL = 0;
int main()
{
   SystemInit();
    
  
   if (SysTick_Config(SystemCoreClock / 1000))
  { 
    /* Capture error */ 
    while (1);
  }
 NVIC_EnableIRQ( SysTick_IRQn);
   InitDAC();
   InitAD7699();
   u8 Counter = 0;
do
 {
WriteDAC(Counter);
Counter++;
}while(Counter!=255);
RestroreState();
   
   //WriteDAC(2);
   //ReadAD7699(VL);
   GPIO_InitTypeDef GPIO_InitStructure;                                                         
   GPIO_StructInit(&GPIO_InitStructure);
   GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
   GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
   GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_7|GPIO_Pin_6|GPIO_Pin_5 ;//|GPIO_Pin_12;
   GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
   GPIO_InitStructure.   GPIO_PuPd    =    GPIO_PuPd_DOWN;
   GPIO_Init(GPIOB, &GPIO_InitStructure);
   GPIO_ResetBits(GPIOB,GPIO_Pin_7|GPIO_Pin_6|GPIO_Pin_5);
   Parcer = BlueGigaComandParser();
   InitRS232();
   ResetBluetooth();
   Delay(1000);
   ReadData(OutputD);
   Parcer.BindRx(OutputD);
   LastEvents =   Parcer.GetEvents(DataCounter);
   if(LastEvents != EREADY)
   {
      /*WT xx Not Found or wrong Paramas RS232*/
      
      
      do
      {
         
          GPIO_ResetBits(GPIOB,GPIO_Pin_7);
         Delay(1000);
          GPIO_SetBits(GPIOB,GPIO_Pin_7);
         
         
      }while(true);
      
   }
    __disable_irq(); 
    InitStack (&m_StackR); 
    InitStack (&m_StackT);
    InitCommand (&m_CommandR);
    __enable_irq();  
     Delay(1000);
     WriteComman("SET BT NAME DO 3.1\n");
     Delay(50);
     WriteComman("SET BT AUTH * 1234\n");
     Delay(1000);
do
{
   ReadData(OutputD);
   Parcer.BindRx(OutputD);
   LastEvents =   Parcer.GetEvents(DataCounter);
 

    
    if(LastEvents == EREADY)
    {
        ::GPIO_ResetBits(GPIOB,GPIO_Pin_6|GPIO_Pin_5);
        ::GPIO_SetBits(GPIOB,GPIO_Pin_7);
       
    }
    
    
    
    if(LastEvents == ERING)
    {
        ::GPIO_ResetBits(GPIOB,GPIO_Pin_6|GPIO_Pin_5);
        ::GPIO_SetBits(GPIOB,GPIO_Pin_6);
        DeviceConected  = true;
        _CommadMode  = false;
    }
    
    
    
    
    if(LastEvents == ENO_CARRIER)
    {
       ::GPIO_ResetBits(GPIOB,GPIO_Pin_6|GPIO_Pin_7);
        ::GPIO_SetBits(GPIOB,GPIO_Pin_5);
        DeviceConected  = false;
        _CommadMode  = true;
    }
    
    if(_CommadMode  == false &&  DeviceConected  == true )
    {
    
    Ret = ReceiveCommand (Buff, &N);
    switch(Buff[2])
    {
       
    case READ_SW_HW:
       {
         ReadVersion(Buff,N);
          break;
          
       }
       
    case READ_SiMP_THERSH:
       {
          
          ReadSiMP_Thershold(Buff,N);
          
          break;
       }
          
          
       
       
       case READ_CONTROL:
       {
          
          ReadControl(Buff,N);
          
          break;
       }
       
       
       
          case READ_COMPLETE_CONV:
       {
          
          ReadStatIsDone(Buff,N);
          
          break;
       }
       
       
           case READ_STATE_WORD:
       {
          
          ReadState(Buff,N);
          
          break;
       }
       
       
           
    case    READ_OSC_PARAMS:
       {
          
          ReadOscParams(Buff,N);
          
          break;
       }
       
       
       
        case READ_STATE_WORD_OSC :
       {
          
          ReadStateOsc(Buff,N);
          
          break;
       }
       
       
         case READ_SiMP_VOLTAGE :
       {
          
          ReadSiMP_Voltage(Buff,N);
          
          break;
       }
       
       
       
       
         case READ_PARAMS_MESURED_EXP:
       {
          
         ReadExpositionParams(Buff,N);
          
          break;
       }
       
       
       
       
       
       
        
         case READ_STATE_PER_CHANEL:
       {
          
         ReadStaticPerChannel(Buff,N);
          
          break;
       }
       
       
       
       
         case READ_CURRENT_ADC_VALUES:
       {
          
         ReadADCValues(Buff,N);
          
          break;
       }
       
       
       
        case READ_AMPLITUDES_OSC:
       {
          
         ReadAmpsOsc(Buff,N);
          
          break;
       }
       
       
       
       
        
        case READ_SERIAL:
       {
          
         ReadModuleNumber(Buff,N);
          
          break;
       }
       
       
       
        case READ_ONE_ADC_CH:
       {
          
         ReadADCperCh(Buff,N);
          
          break;
       }
       
       
    case RESET_CS_PIN:
    {
       Reset_CS_PIN(Buff, N);
       break;
    }
    
    
    
    
    case SET_CS_PIN:
    {  
     Set_CS_PIN(Buff, N);       
       break;
    };
       
       
//////////////////////////////////////////////////////////////////////////////       
       
       
    case   WRITE_SiPM_THERSH:
       {
          
          
          WriteSiMP_Thershold(Buff,N);
          
          break;
       }
          
          
          
        case   WRITE_CONTROL_DOZ:
       {
          
          
          WriteControlReg(Buff,N);
          
          break;
       }
             
       
       
       
       
        case   START_STATISTIC:
       {
          
          StartStatist(Buff,N);
          
          break;
       }
       
       
         case   CLEAR_STATE_WORD:
       {
          
          ClearStateWord(Buff,N);
          
          break;
       }
       
       
       
       
          case   SET_PARAM_OSCIL:
       {
          
          WriteOscParams(Buff,N);
          
          break;
       }
       
       
       
         
          case  SET_WAIT_OSCIL:
       {
          
          WaitMeasureOsc(Buff,N);
          
          break;
       }
       
       
       
           case WRITE_SiMP_VOLTAGE:
       {
          
         WriteSiMP_Voltage(Buff,N);
          
          break;
       }

       
        
        case WRITE_SERIAL:
       {
          
          WriteModuleNumber(Buff,N);
          
          break;
       }

 
          
    }
          
       
     
       
    
    }
   
  }while(true);
}








  
  
 
