#include "DAC.h"
/******************************************************************************/
void InitDAC(void)
{

  
   RCC_APB1PeriphClockCmd(RCC_APB1Periph_SPI2,ENABLE);
   RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB,ENABLE);
   RCC_AHB1PeriphClockCmd(CS_RCC,ENABLE);
   GPIO_PinAFConfig(GPIOB, GPIO_PinSource15, GPIO_AF_SPI2);
   GPIO_PinAFConfig(GPIOB, GPIO_PinSource13, GPIO_AF_SPI2);

   GPIO_InitTypeDef GPIO_InitStructure;
   GPIO_StructInit(&GPIO_InitStructure);
   GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
   GPIO_InitStructure.GPIO_PuPd =  GPIO_PuPd_NOPULL;
   GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
   GPIO_InitStructure.GPIO_Pin = GPIO_Pin_15|GPIO_Pin_13;
   
   GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
   GPIO_Init(GPIOB, &GPIO_InitStructure);
   
   SPI_InitTypeDef SPI_InitStructS;
   SPI_StructInit(&SPI_InitStructS);   
   
   GPIO_StructInit(&GPIO_InitStructure);
   GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
   GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN ;
   GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
   GPIO_InitStructure.GPIO_Pin = CS_PIN;
   
   GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
   GPIO_Init(CS_PORT, &GPIO_InitStructure);
   
   
   DissableTransimt();  
   SPI_StructInit(&SPI_InitStructS);
   SPI_InitStructS.SPI_Direction = SPI_Direction_1Line_Tx;
   SPI_InitStructS.SPI_Mode = SPI_Mode_Master;
   SPI_InitStructS.SPI_DataSize = SPI_DataSize_8b;
   
   SPI_InitStructS.SPI_FirstBit =SPI_FirstBit_MSB;  
   SPI_InitStructS.SPI_CPOL = SPI_CPOL_Low;
   SPI_InitStructS.SPI_CPHA = SPI_CPHA_1Edge;
   SPI_InitStructS.SPI_NSS = SPI_NSS_Soft;
   SPI_InitStructS.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_64 ;
   SPI_Init(SPI2,&SPI_InitStructS);
   SPI_Cmd(SPI2,ENABLE);
   SPI_I2S_ITConfig(SPI2,SPI_I2S_IT_TXE|SPI_I2S_IT_RXNE| SPI_I2S_IT_ERR| I2S_IT_UDR|SPI_I2S_IT_TIFRFE,DISABLE);
   SPI_NSSInternalSoftwareConfig(SPI2, SPI_NSSInternalSoft_Set);


   
   
}
/******************************************************************************/
void EnableTransimt(void)
{
   GPIO_ResetBits(CS_PORT,CS_PIN);  
}
/******************************************************************************/
void DissableTransimt(void)
{
   GPIO_SetBits(CS_PORT,CS_PIN );
}
/******************************************************************************/
void WriteDAC(uint8_t Value)
{
   EnableTransimt();
   SPI_I2S_SendData(SPI2,Value);   
   DissableTransimt();
   EnableTransimt();
   
};
/******************************************************************************/