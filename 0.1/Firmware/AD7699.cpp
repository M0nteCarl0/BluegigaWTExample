#include "AD7699.h"
void InitAD7699(void)
{
   
   
   
   RCC_APB1PeriphClockCmd(RCC_APB1Periph_SPI3,ENABLE);
   RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB,ENABLE);
   RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC,ENABLE); 
   RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD,ENABLE);
   GPIO_PinAFConfig(GPIOC, GPIO_PinSource10, GPIO_AF_SPI3);
   GPIO_PinAFConfig(GPIOC, GPIO_PinSource11, GPIO_AF_SPI3);
   GPIO_PinAFConfig(GPIOC, GPIO_PinSource12, GPIO_AF_SPI3);
   
   
   GPIO_InitTypeDef GPIO_InitStructure;
   GPIO_StructInit(&GPIO_InitStructure);
   GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
   GPIO_InitStructure.GPIO_PuPd =  GPIO_PuPd_NOPULL;
   GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
   GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10|GPIO_Pin_11|GPIO_Pin_12;
   
   GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
   GPIO_Init(GPIOC, &GPIO_InitStructure);
   
   SPI_InitTypeDef SPI_InitStructS;
   SPI_StructInit(&SPI_InitStructS);   
   
   GPIO_StructInit(&GPIO_InitStructure);
   GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
   GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
   GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
   GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2;
   
   GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
   GPIO_Init(GPIOD, &GPIO_InitStructure);
   
   
   DissableTransimtAD7699();

   SPI_InitStructS.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
   SPI_InitStructS.SPI_Mode = SPI_Mode_Master;
   SPI_InitStructS.SPI_DataSize = SPI_DataSize_16b;
   
   SPI_InitStructS.SPI_FirstBit =SPI_FirstBit_MSB;  
   SPI_InitStructS.SPI_CPOL = SPI_CPOL_Low;
   SPI_InitStructS.SPI_CPHA = SPI_CPHA_1Edge;
   SPI_InitStructS.SPI_NSS = SPI_NSS_Soft;
   SPI_InitStructS.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_128 ;
   SPI_Init(SPI3,&SPI_InitStructS);
   SPI_Cmd(SPI3,ENABLE);
   SPI_I2S_ITConfig(SPI3,SPI_I2S_IT_TXE|SPI_I2S_IT_RXNE| SPI_I2S_IT_ERR| I2S_IT_UDR|SPI_I2S_IT_TIFRFE,DISABLE);
   SPI_NSSInternalSoftwareConfig(SPI3, SPI_NSSInternalSoft_Set);  
}

/******************************************************************************/
void EnableTransimtAD7699(void)
{
   GPIO_ResetBits(GPIOD,GPIO_Pin_2);
}
/******************************************************************************/
void DissableTransimtAD7699(void)
{
   GPIO_SetBits(GPIOD,GPIO_Pin_2);
}
/******************************************************************************/
void WriteAD7699(uint8_t Value)
{
   EnableTransimtAD7699();
    do
   {
   }while(SPI_I2S_GetFlagStatus(SPI3, SPI_I2S_FLAG_BSY) == SET);
   SPI_I2S_SendData(SPI3,Value);
   do
   {
   }while(SPI_I2S_GetFlagStatus(SPI3, SPI_I2S_FLAG_BSY) == SET);
   DissableTransimtAD7699();
};
/******************************************************************************/
void ReadAD7699(uint16_t Value)
{
  EnableTransimtAD7699();
   do
   {
   }while(SPI_I2S_GetFlagStatus(SPI3, SPI_I2S_FLAG_BSY) == SET);
   
  Value =   SPI_I2S_ReceiveData(SPI3);
   do
   {
   }while(SPI_I2S_GetFlagStatus(SPI3, SPI_I2S_FLAG_BSY) == SET);
   DissableTransimtAD7699();
     
}
/******************************************************************************/




