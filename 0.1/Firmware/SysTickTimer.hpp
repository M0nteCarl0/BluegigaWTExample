#include "stm32f4xx.h"


typedef  enum Resolution{
  
  ms = 1000,
  ns = 1000000000
  
  
}Resolution;


#ifdef __cplusplus
 extern "C" {
#endif
   
   
   
   
   void InitSystick(Resolution Res);
    void TimeInc(void);
     void Delay(u32 Delay);
     u32 GetTime(void);
      u32 ElapsedTime(u32 Begin,u32 End);
   
   
   
   
 #ifdef __cplusplus
 }
#endif