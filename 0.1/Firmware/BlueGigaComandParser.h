#pragma once
#include <stdint.h>
#include <string>
#include <string.h>
#ifndef _BTH_BLUEGIGA_PARCER_
#define _BTH_BLUEGIGA_PARCER_
#define BufferSize 512
#define VER "0x1A"
using namespace std;
/*************************************************************************************************************/
typedef  struct SBT_Adr
{
	uint8_t b1;
	uint8_t b2;
	uint8_t b3;
	uint8_t b4;
	uint8_t b5;
	uint8_t b6;


}SBT_Adr,*SBT_AdrPtr;
/*************************************************************************************************************/
typedef  struct Suuid128
{
uint32_t uuid32_1;
uint16_t uuid16_2;
uint16_t uuid16_3;
uint16_t uuid16_4;
uint32_t uuid32_5;
}Suuid128, *Suuid128Ptr;
/*************************************************************************************************************/
typedef  union Utarget
{

	uint16_t uuid16;
	uint32_t uuid32;
	Suuid128 uuid128;
	uint16_t L2CAP_psm;


}Utarget;
/*************************************************************************************************************/
typedef enum boot_mode
{
	IWARP               = 1,
	HCI_BCSP_115200_8E1 = 2,
	HCI_USB             = 3,
	HCI_H4_115200_8N1   = 4

}boot_mode;
/*************************************************************************************************************/
typedef enum EConnect_mode
{
	CON_RFCOMM,
	HFP,
	HFP_AG,
	A2DP,
	AVRCP,
	HID,
	CON_L2CAP,
	CON_PBAP,
	OPP,
	HSP,
	HSP_AG,
	HDP
}EConnect_mode;
/*************************************************************************************************************/
typedef enum RFCOMM_Errors
{
	RFC_CONNECTION_REJ_SECURITY    = 0x403,
	RFC_ABNORMAL_DISCONNECT        = 0x405,
	RFC_REMOTE_REFUSAL             = 0x406,
	RFC_INVALID_CHANNEL            = 0x408,
	RFC_INVALID_PAYLOAD            = 0x40a,
	RFC_INCONSISTENT_PARAMETERS    = 0x40b,
	RFC_PEER_VIOLATED_FLOW_CONTROL = 0x40c,
	RFC_RES_ACK_TIMEOUT            = 0x40d,
	RFC_L2CAP_CONNECTION_FAILED    = 0xc01,
	RFC_CONNECTION_KEY_MISSING     = 0xc02,
	RFC_L2CAP_LINK_LOSS            = 0xc0c,
	RFC_CONNECTION_REJ_SSP_AUTH_FAIL,
	L2CAP_CONNECTION_SSP_AUTH_FAIL
}RFCOMM_Errors;

/*************************************************************************************************************/
typedef enum L2CAP_Errors
{
	L2CAP_CONNECTION_NOT_READY     = 0x9000,
	L2CAP_CONNECTION_REJ_PSM       = 0x9002,
	L2CAP_CONNECTION_REJ_SECURITY  = 0x9003,
	L2CAP_CONNECTION_REJ_RESOURCES = 0x9004,
	L2CAP_RESULT_CONFTAB_EXHAUSTED = 0x9019,
	L2CAP_RESULT_PEER_ABORTED      = 0x901a,
	L2CAP_CONNECTION_KEY_MISSING   = 0x9802
}L2CAP_Errors;

/*************************************************************************************************************/

typedef enum IWEvents
{
 ERING,
 ENO_CARRIER,
 EREADY
 
}IWEvents;
/*************************************************************************************************************/
typedef enum IWCommands{
	DOG,
	AIO,
	AT, 
	AUTH,
	AVRCP_PDU,
	BATTERY,
	BCSP_ENABLE,
	BER,
	BLINK,
	BOOT,
	CALL,
	CLOCK,
	CLOSE,
	CONNAUTH,
	CONNECT,
	ECHO,
	DEFRAG,
	DELAY,
	HID_GET,
	HID_SET,
	INQUIRY,
	IC,
	IDENT,
	INFO,
	KILL,
	L2CAP,
	LICENSE,
	LIST,
	LOOPBACK,
	NAMEE,
	PAIR,
	PIO,
	PLAY,
	RFCOMM,
	RESETE,
	RSSI,
	SCO_ENABLE,
	SCO_OPEN,
	SDP,
	SDP_ADD,
	SELECT,
	SETE,
	SET_BT_AUTH,
	SET_BT_BDADDR,
	SET_BT_CLASS,
	SET_BT_FILTER,
	SET_BT_IDENT,
	SET_BT_LAP,
	SET_BT_MTU,
	SET_BT_NAME,
	SET_BT_PAIRCOUNT,
	SET_BT_PAGEMODE,
	SET_BT_PAIR,
	SET_BT_POWER,
	SET_BT_ROLE,
	SET_BT_SCO,
	SET_BT_SNIFF,
	SET_BT_SSP,
	SET_CONTROL_AUDIO,
	SET_CONTROL_AUTOCALL,
	SET_CONTROL_AUTOPAIR,
	SET_CONTROL_BATTERY,
	SET_CONTROL_BAUD,
	SET_CONTROL_BIND,
	SET_CONTROL_CD,
	SET_CONTROL_CODEC,
	SET_CONTROL_CONFIG,
	SET_CONTROL_ECHO,
	SET_CONTROL_ESCAPE,
	SET_CONTROL_EXTCODEC,
	SET_CONTROL_GAIN,
	SET_CONTROL_INIT,
	SET_CONTROL_MICBIAS,
	SET_CONTROL_MUX,
	SET_CONTROL_MSC,
	SET_CONTROL_PIO,
	SET_CONTROL_PREAMP,
	SET_CONTROL_RINGTONE,
	SET_CONTROL_READY,
	SET_CONTROL_VOLSCALE,
	SET_CONTROL_VREGEN,
	SET_ACTIVE,
	SET_MASTER,
	SET_MSC,
	SET_SLAVE,
	SET_SNIFF,
	SET_SUBRATE,
	SET_SELECT,
	SET_PROFILE,
	SET_RESET,
	SLEEP,
	SSPAUTH,
	SSP_CONFIRM,
	SSP_PASSKEY,
	SSP_GETOOB,
	SSP_SETOOB,
	TEMP,
	TEST,
	TESTMODE,
	TXPOWER,
	PBAP,
	VOLUME
}IWCommands;
/*************************************************************************************************************/
//IWARP Software comand
typedef enum AIO_Source
{
	AIO0_on_WT32i               = 0,
	AIO1_on_all_except_WT12     = 1,
	Internal_voltage_reference  = 4


}AIO_Source;
/*************************************************************************************************************/
   ///See in folder original
class BlueGigaComandParser
{
public:

	  BlueGigaComandParser(void);
	void Auth(int Pin,SBT_Adr &DeviceAdres);
	void Aio(AIO_Source Source);
	void Boot(boot_mode BootMode);
	void Call(SBT_Adr &DeviceAdres,uint8_t Chanell,uint16_t Mtu,uint16_t PayloadSize);//RFCOMM
	void Call(SBT_Adr &DeviceAdres, Utarget Target,uint16_t Mtu,uint16_t PayloadSize);//Generic
	void Close(uint8_t link_id);
	void RfComm(void);
	void Reset(void);
	void Data(uint8_t* Source,size_t Length);
	void Data(uint16_t* Source,size_t Length);
	void Rssi(uint8_t link_id,int16_t rssi,SBT_Adr &DeviceAdres);
	void Battery(void);
	void BindRx(char Rx[]);
	void BindTx(char Tx[]);
	void SetLinkID(uint8_t ID);
	virtual ~BlueGigaComandParser(void);
	IWEvents GetEvent(void);
	IWEvents GetEvents(uint16_t Position);
private:
	char* RxQ;
	char* TxQ;
	void InsertCommand(IWCommands Command);
	void ParseRx(void);
	SBT_Adr GetBluetoothAdress( const char* Source);
	void AddNewLine(char* Source);
	void AddSpace(void);
	void AddIntegerNode(int16_t Node);
	void AddTargetAddress(SBT_Adr &DeviceAdres);
	void ClearTxQ(void);
	void ClearRxQ(void);
	uint8_t LinkID;
//	char DebugInput[];
	
};
#endif
