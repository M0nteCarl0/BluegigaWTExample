#include "SysTickTimer.hpp"
__IO u32 Time;
 void InitSystick(Resolution Res)
 {
   SysTick_CLKSourceConfig(SysTick_CLKSource_HCLK);
  SysTick_Config(SystemCoreClock/ Res);
  Time = 0;
   
   
 };
 
 void TimeInc(void)
 {
   
   Time--;
   
 };
 
 
 void Delay(u32 Delay)
 {
    Time = Delay;
   while(Time!=0)
   {
     
    __no_operation();
   };
   
   
 };
 
 
 u32 GetTime(void)
 {
   
   return Time;
   
 };
 
 
 u32 ElapsedTime(u32 Begin,u32 End)
 {
   u32 Elaps = 0;
   if(Begin < End)
   {
     Elaps = End - Begin;
     
   }
   if(Begin> End)
   {
     
     Elaps = (End + 0xFFFFFFFF) - Begin;
     
   }
   return Elaps;
 };