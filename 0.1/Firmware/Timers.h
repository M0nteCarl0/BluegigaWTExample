#include "stm32f4xx.h"

#ifdef __cplusplus
 extern "C" {
#endif 
   
uint16_t ComputeTimerMs(uint16_t ms);
void InitMsTimer(void);
void InitTimer4_50us(void);
void InitTimer3_Time_Counter(void);

#ifdef __cplusplus
 }
#endif 
  