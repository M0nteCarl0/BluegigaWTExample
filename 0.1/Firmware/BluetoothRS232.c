#include "BluetoothRS232.h"
#include "SysTickTimer.hpp"
/*****************************************************************************/
void InitRS232(void)
{
   
   RCC_AHB1PeriphClockCmd (RCC_AHB1Periph_GPIOA , ENABLE);
   RCC_APB2PeriphClockCmd (RCC_APB2Periph_USART1 ,ENABLE);
   GPIO_PinAFConfig(GPIOA, GPIO_PinSource10, GPIO_AF_USART1);
   GPIO_PinAFConfig(GPIOA, GPIO_PinSource11, GPIO_AF_USART1);
   GPIO_PinAFConfig(GPIOA, GPIO_PinSource12, GPIO_AF_USART1);//!!
   GPIO_PinAFConfig(GPIOA, GPIO_PinSource9, GPIO_AF_USART1);
   
  
   
   GPIO_InitTypeDef GPIO_InitStructure;
   GPIO_StructInit(&GPIO_InitStructure);
   /* Configure USART Tx/RTS as alsternate function  */
   GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
   GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
   GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
   GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9|GPIO_Pin_12;
   
   GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
   GPIO_Init(GPIOA, &GPIO_InitStructure);
   /* Configure USART Rx/CTS as alternate function  */
   //GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
   GPIO_InitStructure.GPIO_PuPd =  GPIO_PuPd_UP;
   GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
   GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_10|GPIO_Pin_11 ;
   GPIO_Init(GPIOA, &GPIO_InitStructure);
   
  
   GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
   GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_8;//|GPIO_Pin_12;
   GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
   GPIO_InitStructure.   GPIO_PuPd    =    GPIO_PuPd_DOWN;
   GPIO_Init(GPIOA, &GPIO_InitStructure);
   
   USART_InitTypeDef RS485;
   USART_StructInit(&RS485);
   RS485.USART_BaudRate = 384000;
   RS485.USART_WordLength = USART_WordLength_8b;
  
   RS485.USART_Parity = USART_Parity_No;
   RS485.USART_StopBits = USART_StopBits_1;

   RS485.USART_Mode = USART_Mode_Rx|USART_Mode_Tx;
   RS485.USART_HardwareFlowControl =USART_HardwareFlowControl_RTS_CTS ;
  //RS485.USART_HardwareFlowControl =   USART_HardwareFlowControl_None;
   USART_Init (USART1, &RS485);

   USART_Cmd (USART1, ENABLE);
   NVIC_EnableIRQ(USART1_IRQn);
   USART_ITConfig(USART1,USART_IT_RXNE,ENABLE);
   USART_ITConfig(USART1,USART_IT_TXE,DISABLE);
   
   NVIC_InitTypeDef NVIC_InitStructure;
   NVIC_PriorityGroupConfig (NVIC_PriorityGroup_0);
   NVIC_InitStructure.NVIC_IRQChannel                    = USART1_IRQn;
   NVIC_InitStructure.NVIC_IRQChannelSubPriority         = 0;
   NVIC_InitStructure.NVIC_IRQChannelCmd                 = ENABLE;
   NVIC_Init (&NVIC_InitStructure);
   
}
/*****************************************************************************/
void EnableTransmit(void)
{
    USART_ITConfig(USART1,USART_IT_TC,ENABLE);
};
/*****************************************************************************/
void ResetBluetooth(void)
{
   GPIO_SetBits(GPIOA,GPIO_Pin_8);
   Delay(40);
   GPIO_ResetBits(GPIOA,GPIO_Pin_8);  
}
/*****************************************************************************/