#include "Commands.h"
#include <string.h>
#include <math.h>
#include "DAC.h"
 /******************************************************************************
  * @file    Commands.c 
  * @author  Molotaliev A.O
  * @version V1.0.0
  * @date    20-September-2013
  * @brief   ����������� ������� ������.
  *         
  *          
/*****************************************************************************/
uint8_t NAME[4] = {'D','O',0,0};
uint8_t SW[2] ={1,1};
uint8_t HW[2] ={2,0};
Dozparams DozimetrP; //��������� ���������
//FunctorQ FunctionSet;//������ �������

bool DataReady = false;
bool  m_FlgCheckCRCSignature = true;
const u32 AddresModule = 0x08080000;
const u8 StateFlash = 8;
extern u16 DataCounter;
extern  char DataBuff[512];
 /*****************************************************************************/
void InitFunctorQ(void)
{
  
};
  
/*****************************************************************************/


void ExeComand(uint8_t Index,uint8_t* Data,int Number)
{
  
  
};
/*****************************************************************************/

  void TestStructure(void)
{

  
   
  
  
//             
//                DozimetrP.SummPerChannel[0]+=DozimetrP.CurrentPerChannel[0];
//                DozimetrP.SummPerChannel[1]+=DozimetrP.CurrentPerChannel[1];
//                   DozimetrP.SummPerChannel[2]+=DozimetrP.CurrentPerChannel[2];
//                      DozimetrP.SummPerChannel[3]+=DozimetrP.CurrentPerChannel[3];
//                         DozimetrP.SummPerChannel[4]+=DozimetrP.CurrentPerChannel[4];
//                            DozimetrP.SummPerChannel[5]+=DozimetrP.CurrentPerChannel[5];
//                               DozimetrP.SummPerChannel[6]+=DozimetrP.CurrentPerChannel[6];
//                                  DozimetrP.SummPerChannel[7]+=DozimetrP.CurrentPerChannel[7];
//                                     DozimetrP.SummPerChannel[8]+=DozimetrP.CurrentPerChannel[8];
//                                        DozimetrP.SummPerChannel[9]+=DozimetrP.CurrentPerChannel[9];
//                                           DozimetrP.SummPerChannel[10]+=DozimetrP.CurrentPerChannel[10];
//                                              DozimetrP.SummPerChannel[11]+=DozimetrP.CurrentPerChannel[11];
//                                                 DozimetrP.SummPerChannel[12]+=DozimetrP.CurrentPerChannel[12];
//   
   
   
  
  
  DozimetrP.SiMP_Voltage = 0;
};
/*****************************************************************************/
void WordtoBytes(u16 Source ,u8 Dest1,u8 Dest2)
{
  Dest1 = Source & 0x7F;
  Dest2 = (Source  >> 7) & 0x7F;
  
};

/****************************************************************************/
void  BytestoWord(u16  Dest,u8 Source1,u8 Source2)
{
  
  Dest = ((u16)Source1  & 0x7F) | (((u16)Source2   & 0x7F) << 7);
  
};

/*****************************************************************************/
void RS485_Enable ()
{

  GPIO_SetBits(GPIOB,GPIO_Pin_12);
  DataCounter = 0;
  ::memset(DataBuff,0,512);
  USART_ITConfig (USART1, USART_IT_TC,  ENABLE);
  //USART_ITConfig (USART1, USART_IT_RXNE,  DISABLE); 
  //USART_ITConfig(USART1,USART_IT_TXE,ENABLE);
};

/*****************************************************************************/

void RS485_Disable ()
{
  GPIO_ResetBits(GPIOB,GPIO_Pin_12);
  
};
/*****************************************************************************/
/*                      ������� ��� ������                                   */
/*****************************************************************************/

void Force_Measure(uint8_t *Buff, int N)
{
   if (Buff[3] != 5)   return;
  __disable_irq();
  DozimetrP.CurrentCommand = FORCE_START_STATIC ;
   TIM_SetCounter(TIM2, 0);
  for(u8  i= 0;i<12;i++)
  {
   memset(&DozimetrP.BufferSampler[i],0,2048);
  }
  
  DozimetrP.StateWord|=( 1 << 1);
  ClearIterator();
  __enable_irq();

  
};
/*****************************************************************************/
void ReadAccBuffer(uint8_t *Buff, int N)
{
  
   if (Buff[3] != 7)   return;
   uint8_t Ch = Buff[5];
   __disable_irq();


for(u16 i = 0;i<2048;i++)
{
  u16 j = 5 + 2*i;
  WordtoBytes(DozimetrP.BufferSampler[Ch].Sample[i],Buff[j],Buff[j+1]);
  
  
};

   
   N = Buff[3] = 2048* 2 + 5;
   
__enable_irq();
   
   int Ret = SendCommand (Buff, N);
   RS485_Enable ();
  
  
  
};
/*****************************************************************************/
void ReadADCperCh(uint8_t *Buff, int N)
{
  
  if (Buff[3] != 7)   return;
   uint8_t Ch = Buff[5] & 0x7F;
  __disable_irq();
  Buff[5] = DozimetrP.CurrentPerChannel[Ch] & 0x7F;
  Buff[6] = (DozimetrP.CurrentPerChannel[Ch] >> 7) & 0x7F;
   N = Buff[3] = 8; 
  __enable_irq();
   
   int Ret = SendCommand (Buff, N);
   RS485_Enable ();
  
  
  
  
};
/****************************************************************************/
void ReadVersion(uint8_t *Buff, int N)
{
  
  if (Buff[3] != 5)   return;
   
__disable_irq();

   uint8_t SB = 0x01;
   Buff[7] = Buff[8] = 0;
   Buff[5] = NAME[0];
   if (NAME[0] & 0x80) Buff[8] |= SB;
   SB = SB << 1;
   Buff[6] = NAME[1];
   if (NAME[1] & 0x80) Buff[8] |= SB;
   
   Buff[9]  = HW[0];
   Buff[10] = HW[1];
   Buff[11] = SW[0];
   Buff[12] = SW[1];
   
   N = Buff[3] = 14;
   
__enable_irq();
   
   int Ret = SendCommand (Buff, N);
   RS485_Enable ();
  
  
};
/*****************************************************************************/
void ReadControl(uint8_t *Buff, int N)
{ //5 + 1 + crc  = 7
  
  if (Buff[3] != 5)   return;
   
  __disable_irq();
  Buff[5] = DozimetrP.m_SourceOscp& 0x7F; 
   N = Buff[3] = 7 ;
  __enable_irq();
   int Ret = SendCommand (Buff, N);
   RS485_Enable ();
};
/*****************************************************************************/
void ReadSiMP_Thershold(uint8_t *Buff, int N)
{
  //without flash
  // 5byte + 1 word + crc = 8
  
  if (Buff[3] != 5)   return; 
  __disable_irq();
  Buff[5] = DozimetrP.SiMP_Thershold & 0x7F;
  Buff[6] = (DozimetrP.SiMP_Thershold  >> 7) & 0x7F; 
  N = Buff[3] = 8 ;
  __enable_irq();
   int Ret = SendCommand (Buff, N);
   RS485_Enable ();
  
  
  
  
};
 /*****************************************************************************/
void ReadStatIsDone(uint8_t *Buff, int N)
{
  
  if (Buff[3] != 5)   return;
   
__disable_irq();
  Buff[5] = DozimetrP.StatState &0x7F; 
  N = Buff[3] = 7 ;
__enable_irq();
   int Ret = SendCommand (Buff, N);
   RS485_Enable ();
  
  
  
  
};
/*****************************************************************************/
  void ReadState(uint8_t *Buff, int N)
  {
    
    
     if (Buff[3] != 5)   return; 
  __disable_irq();
  Buff[5] = DozimetrP.StateWord & 0x7F; 
  N = Buff[3] = 7 ;
  __enable_irq(); 
  int Ret = SendCommand (Buff, N);
  RS485_Enable ();
    
    
    
  };
/*****************************************************************************/
void ReadOscParams(uint8_t *Buff, int N)
{//5+3 + crc = 9
      
      
      if (Buff[3] != 5)   return;
   
__disable_irq();

  Buff[5] = DozimetrP.OcsParam.ScaleinPixels & 0x7F;
  Buff[6] = DozimetrP.OcsParam.SourceOfSignal & 0x7F;
  Buff[7]  = DozimetrP.OcsParam.FilterInPixels &0x7F; 
  N = Buff[3] = 9 ;
__enable_irq();
   
   int Ret = SendCommand (Buff, N);
   RS485_Enable ();
      
};
/*****************************************************************************/
void ReadStateOsc(uint8_t *Buff, int N)
{//5+1 + crc = 7
   if (Buff[3] != 5)   return;
   
__disable_irq();

Buff[5] = DozimetrP.OcsParam.StateWord & 0x7f;   
  N = Buff[3] = 7 ;
   
__enable_irq();
   
   int Ret = SendCommand (Buff, N);
   RS485_Enable ();
  
};
/*****************************************************************************/
void ReadSiMP_Voltage(uint8_t *Buff, int N)
{
  //5+2+crc  = 8
  
   if (Buff[3] != 5)   return;
   //ADC_DMACmd(ADC1,DISABLE);
  __disable_irq();
  Buff[5] = DozimetrP.SiMP_Voltage;
//memcpy(&Buff[5],&DozimetrP.SiMP_Voltage,sizeof(uint16_t));
  N = Buff[3] = 7 ; 
  __enable_irq();
//ADC_DMACmd(ADC1,ENABLE);
   int Ret = SendCommand (Buff, N);
   RS485_Enable ();

};
/*****************************************************************************/
void ReadExpositionParams(uint8_t *Buff, int N)
{//5 + 24  + 24 + crc = 35
  if (Buff[3] != 5)   return;
   
  __disable_irq();
  Buff[5] = DozimetrP.ExpParams.MeasuredTime & 0x7F;
  Buff[6] =(DozimetrP.ExpParams.MeasuredTime  >> 7) & 0x7F;
  for( uint8_t i = 0;i<12;i++)
  {
    u8 j = 7 + 2*i;
    WordtoBytes(DozimetrP.AvgPerChannel[i],Buff[j],Buff[j+1]);
  }

  for( uint8_t i = 0;i<12;i++)
  {
    u8 j = 34 + 2*i;
    WordtoBytes(DozimetrP.SummPerChannel[i],Buff[j],Buff[j+1]);
  }
  Buff[60]  = DozimetrP.ExpParams.Ciclic_Count_Bef_PWR;
  N = Buff[3] = 60 ; 
  __enable_irq();
  int Ret = SendCommand (Buff, N);
  RS485_Enable ();
  
  
  
  
  
};
/*****************************************************************************/
void ReadStaticPerChannel(uint8_t *Buff, int N)
{
 
  
  
  //5 + 8 + crc = 14
  if (Buff[3] != 7)   return;
  
  uint8_t Channel = Buff[5];
  __disable_irq();
  Buff[5] =  DozimetrP.MaxPerChannel[Channel] & 0x7F;
  Buff[6] = (DozimetrP.MaxPerChannel[Channel]  >> 7) & 0x7F;
  Buff[7] = DozimetrP.MinPerChannel[Channel] & 0x7F;
  Buff[8] = (DozimetrP.MinPerChannel[Channel]  >> 7) & 0x7F;
  Buff[9] = DozimetrP.AvgPerChannel[Channel] & 0x7F;
  Buff[10] = (DozimetrP.AvgPerChannel[Channel]  >> 7) & 0x7F;
  Buff[11] = DozimetrP.RMSPerChannel[Channel] & 0x7F;
  Buff[12] = (DozimetrP.RMSPerChannel[Channel]  >> 7) & 0x7F;

  N = Buff[3] = 14 ;
   
__enable_irq();
   
   int Ret = SendCommand (Buff, N);
   RS485_Enable ();
  
  
}
/*****************************************************************************/
void ReadADCValues(uint8_t *Buff, int N)
{
   
  //5 + 0 = 5
  if (Buff[3] != 5)   return;
  int j  = 0;
  __disable_irq();
  //ADC_DMACmd(ADC1,DISABLE);
  for(int i = 0;i<12;i++)
  {
    j = 5 + i*2;
    Buff[j] = DozimetrP.CurrentPerChannel[i] & 0x7f;
    Buff[j+1] = (DozimetrP.CurrentPerChannel[i] >> 7) & 0x7F;
  }
//5+24 + crc = 30
  N = Buff[3] = 31 ;
 // ADC_DMACmd(ADC1,ENABLE);
  __enable_irq(); 
  int Ret = SendCommand (Buff, N);
  RS485_Enable ();
 
  
};
/*****************************************************************************/
void ReadAmpsOsc(uint8_t *Buff, int N)
{
  
  
 
  
  //5 + 2 + crc = 8
  if (Buff[3] != 9)   return;
  
uint8_t Channel = Buff[5] & 0x7F;

u8 j = 0;
__disable_irq();
uint16_t Offset =  (Buff[6] & 0x7F) | (Buff[7] &  0x7F)<< 7;
uint16_t RealOffset  = 0;
for(uint8_t i = 0;i<64;i++)
{
   j = 5 + i*2;
   RealOffset = (uint16_t)Offset + i; 
  
   Buff[j] = DozimetrP.BufferSampler[Channel].Sample[RealOffset] & 0x7F;
   Buff[j+1] = (DozimetrP.BufferSampler[Channel].Sample[RealOffset] >> 7) & 0x7F;


}
//5 + 128 + crc = 134
  N = Buff[3] = 136 ;
   
__enable_irq();
   
   int Ret = SendCommand (Buff, N);
   RS485_Enable ();
  
  
  
};
/*****************************************************************************/                 
 void ReadModuleNumber(uint8_t *Buff, int N)
 {
   
   if (Buff[3] != 5)   return;

 
     
   N = Buff[3] = 39;
   
   int Ret = SendCommand (Buff, N);
   RS485_Enable ();
   
   
   
 }
 /*****************************************************************************/
 void ReadExternalTrigerTime(uint8_t *Buff, int N)
 {
   
   
   
    if (Buff[3] != 5)   return;

__disable_irq();
Buff[5] = DozimetrP.TimeExt.MeasuredTime & 0x7F;
Buff[6] = (DozimetrP.TimeExt.MeasuredTime  >> 7) & 0x7F;

  N = Buff[3] = 8 ;
   
__enable_irq();

   int Ret = SendCommand (Buff, N);
   RS485_Enable ();
   
   
 };
 
 /*****************************************************************************/
 /*                      ������� ��� ������                                   */
 /*****************************************************************************/
void WriteSiMP_Thershold(uint8_t *Buff, int N)
{
  
  
   
  //5 + 2 + crc = 8
  if (Buff[3] != 8)   return;
  
__disable_irq();


DozimetrP.SiMP_Thershold =((uint16_t)Buff[5]  & 0x7F) | (((uint16_t)Buff[6]   & 0x7F) << 7);
//SaveState();
   
__enable_irq();
   
 
   RS485_Enable ();
  
  
  
  
  
}


/*****************************************************************************/
void WriteControlReg(uint8_t *Buff, int N)
{
  
  //5 + 1 + crc = 8
  if (Buff[3] != 7)   return;
  
__disable_irq();

DozimetrP.m_SourceOscp = Buff[5];

   
__enable_irq();
   
  
   RS485_Enable ();
  
  
  
};
/*****************************************************************************/
void ClearStateWord(uint8_t *Buff, int N)
{
  
  if (Buff[3] != 5)   return;
  __disable_irq();
  DozimetrP.StateWord = 0;

//5 + 128 + crc = 134
 /// N = Buff[3] = 134 ;
   
  __enable_irq();
   
  /// int Ret = SendCommand (Buff, N);
 RS485_Enable ();
};
/*****************************************************************************/
void WriteOscParams(uint8_t *Buff, int N)
{
  
  //5+1 + crc  =  9
  if (Buff[3] != 7)   return;
  
  __disable_irq();
  DozimetrP.OcsParam.ScaleinPixels = Buff[5];
  __enable_irq();
   
  /// int Ret = SendCommand (Buff, N);
  RS485_Enable ();
  
  
}
/*****************************************************************************/
void WaitMeasureOsc(uint8_t *Buff, int N)
{
  
   //5+3 + crc  =  9
  if (Buff[3] != 5)   return;
  
  __disable_irq();

  DozimetrP.StateWord |= (1 << 0 );
  DozimetrP.StateWord &= ~(1 << 1 );
  DozimetrP.OcsParam.OscState = WatiMesure;
  DozimetrP.ExpParams.ExpoWork  = WORK;
 
  __enable_irq();
   
  /// int Ret = SendCommand (Buff, N);
  RS485_Enable ();
  
};
/*****************************************************************************/
void WriteSiMP_Voltage(uint8_t *Buff, int N)
{
  
  //5+2 + crc  =  8
  if (Buff[3] != 7)   return;
  
  __disable_irq();

  DozimetrP.SiMP_Voltage = Buff[5];
  SaveState();
  if(DozimetrP.SiMP_Voltage < 1 )
  {
     
   DozimetrP.SiMP_Voltage = 1;  
  }
  WriteDAC(DozimetrP.SiMP_Voltage*2);
  __enable_irq();
   
  /// int Ret = SendCommand (Buff, N);
   RS485_Enable ();
  
};
/*****************************************************************************/
void WriteModuleNumber(uint8_t *Buff, int N)
{
// 0x3F    �������� ����� ������        5+3+30+1=39
// FLASH
{
   if (Buff[3] != 39)   return;

   uint8_t Crc[2];
   uint16_t *C = (uint16_t*) Crc;
   int J  = 8;
   int N0 = J+30-2;
   int N1 = J+30-1;
   
   if (m_FlgCheckCRCSignature){
      *C = MakeCRC16 (&Buff[J], 30-2);
      if (Crc[0] == Buff[N0] && Crc[1] == Buff[N1]){
         Buff[N0] = Buff[N1] = 0;                            // CRC = 0
        
      }
   }
  
   int Ret = SendCommand (Buff, N);
   RS485_Enable ();
};
  
  
  
  
  
};
/*****************************************************************************/
void StartStatist(uint8_t *Buff, int N)
{
  __disable_irq();
  TIM_SetCounter(TIM3, 0);
  
  DozimetrP.OcsParam.OscState = InProgress;
  DozimetrP.StaticParams.WorkState = WORK;
  
  ClearIterator();
  __enable_irq();
  RS485_Enable ();
}
/*****************************************************************************/
uint8_t CRC8 (uint8_t Crc, uint8_t data)
{
   uint8_t polynomial = 0x07;
   Crc ^= data;

   for (int i = 0; i < 8; ++i)
      Crc = (Crc << 1) ^ ((Crc & 0x80) ? polynomial : 0);

   return Crc;
};

/*****************************************************************************/
void MakeCRC8 (uint8_t *Data, int N, uint8_t *Crc)
{
   uint8_t mCRC = 0;

   for (int i = 0; i < N; i++){
      mCRC = CRC8 (mCRC, Data[i]);
   }

   Crc[0] = mCRC & 0x7F;
   if (mCRC & 0x80)  Crc[1] = 0x01;
   else              Crc[1] = 0x00;
};
/*****************************************************************************/
uint16_t MakeCRC16 (uint8_t *Block, int Len)
{
   uint16_t crc = 0xFFFF;
   int i;
 
   while (Len--)
   {
       crc ^= *Block++ << 8;
 
       for (i = 0; i < 8; i++)
           crc = crc & 0x8000 ? (crc << 1) ^ 0x1021 : crc << 1;
   }
   crc &= 0x7F7F;                                              //  !!! 16, 8-bits Set NULL
   return crc;
};
/*****************************************************************************/

uint8_t MeasureMeanRmsOne (void)
{
//  uint8_t FlgOff = 0;
//  uint32_t Time = MyGetTime24 (MeanRms->Time0);
//  if (Time > 100000){                              //  ����� ��������� 100ms
//    
//    if (MeanRms->Count){
//      MeanRms->Amean /= MeanRms->Count;
//    }
//    else{
//       MeanRms->Amean = 0;
//    }
//    
//    if (MeanRms->Count - 1000 > 0){
//      MeanRms->Arms  /= (MeanRms->Count - 1000);
//    }
//    else{
//       MeanRms->Arms = 0;
//    }
//    
//    FlgOff = 1;      
//  }
  return 0;
};
/*****************************************************************************/


/*****************************************************************************/
void GetSiMPTrigerLevel(void)
{
  //����������
  
  
  if( DozimetrP.SiMP_CurrentValue >= DozimetrP.SiMP_Thershold  && DozimetrP.ExpParams.Expostate ==  WatiMesure && DozimetrP.ExpParams.ExpoWork  == WORK )
  {
    DozimetrP.ExpParams.Begin =  0;
    DozimetrP.ExpParams.Expostate  = InProgress  ;
     
  }
  
  
  if(DozimetrP.SiMP_CurrentValue < DozimetrP.SiMP_Thershold   && DozimetrP.ExpParams.Expostate == InProgress  && DozimetrP.ExpParams.ExpoWork  == WORK )
  {
    
    DozimetrP.StatState = Complete  ;
    DozimetrP.ExpParams.End = 0;
    DozimetrP.ExpParams.ExpoWork  =  NOT_WORK;
  }
  
};
/*****************************************************************************/
void GetExpTrigerState(void)
{
  //����������
   
   static uint8_t Bit0     = 0;
   static uint8_t FlgFront = 0;
   static uint8_t FlgBack  = 0;
   static int     RepeateF = 0; 
   static int     RepeateB = 0; 
  uint8_t Bit;
  
  __disable_irq();
  //for(u8 i = 0;i<10;i++)
  //{
   Bit  = GPIO_ReadInputDataBit(GPIOC,GPIO_Pin_8);
   
   if (Bit0 == 0 && Bit != 0 ){//&& DozimetrP.TrigEvent ==  EXTERNAL_EXPE && DozimetrP.ExpParams.Expostate ==  WatiMesure && DozimetrP.ExpParams.ExpoWork  == WORK ){
      if (!FlgFront){
         FlgFront = 1;
         RepeateF = 0;
        
         DozimetrP.ExpParams.Begin =   0;
         DozimetrP.StatState =    InProgress  ;
         DozimetrP.TimeExt.Begin    =  0;
         
      }
   }
  
                                                         
if(Bit0 != 0 && Bit == 0)
{
///&& DozimetrP.TrigEvent ==  EXTERNAL_EXPE && DozimetrP.ExpParams.Expostate == InProgress  && DozimetrP.ExpParams.ExpoWork  == WORK){
    if (!FlgBack){
       FlgBack  = 1;
        RepeateB = 0;
        DozimetrP.StatState = Complete;      
  DozimetrP.ExpParams.MeasuredTime = 0;
  DozimetrP.TimeExt.MeasuredTime  = 0;
         
      }
}
  Bit0 = Bit;
  __enable_irq(); 
 }
 
  
  
 
/*****************************************************************************/
void CheckTrigerEvents(void)
{
  
  
  
  
  switch(DozimetrP.m_SourceOscp){
    
case SIPME:
  {
    GetSiMPTrigerLevel();
    
  }break;
  
case EXTERNAL_EXPE:
  {
    
   GetExpTrigerState();
    
  }break;
  
  };
    
  
  

};
/*****************************************************************************/
void DoneMeasure(void)
{
  if(DozimetrP.ExpParams.Expostate== Complete && DozimetrP.ExpParams.ExpoWork  == WORK)//���� ����������� ���������� �� ������� ������� ��������������
   {
     DozimetrP.ExpParams.ExpoWork  = NOT_WORK;
     DozimetrP.ExpParams.Ciclic_Count_Bef_PWR++;
     for(int  it = 0;it<13;it++)
     {
       
       DozimetrP.AvgPerChannel[it] = DozimetrP.SummPerChannel[it] /  DozimetrP.ExpParams.CountSamplers;
       
     }
     
     
   };
   
//   if(DozimetrP.OcsParam.OscState  == Complete  && DozimetrP.StaticParams.WorkState == WORK)//���� ����������   ����������
//   {
//     
//   
//     ComputeStatistic(&DozimetrP);
//     DozimetrP.StaticParams.WorkState = NOT_WORK;
//     
//   }
  
  
  
  
};


/*****************************************************************************/
void ComputeStatistic(Dozparams* handler)
{
    for(int i = 0;i<13;i++)//���� �� �������
     {
       for(int j = 0;j<1024;j++)
       {
  handler->SummPerChannel[i]=+handler->BufferSampler[i].Sample[j];
  
  if(handler->BufferSampler[i].Sample[j] >handler->MaxPerChannel[i])
  {
    
    handler->MaxPerChannel[i] = handler->BufferSampler[i].Sample[j];
    
  }
  
   if(handler->BufferSampler[i].Sample[j] <handler->MinPerChannel[i])
  {
    
       handler->MinPerChannel[i] = handler->BufferSampler[i].Sample[j];
    
  };
  
  
       }
     } 
  for(int i = 0;i<13;i++)//���� �� �������
    {
       handler->AvgPerChannel[i] = handler->SummPerChannel[i]/1024;  
    }
    ComputeTrueRMS(handler);
}
/*****************************************************************************/
void ComputeTrueRMS(Dozparams* handler)
{
  for(int i = 0;i<13;i++)//���� �� �������
  {
    handler->SummPerChannel[i] =0 ;
  }
   for(int i = 0;i<13;i++)//���� �� �������
  {
       for(int j = 0;j<1024;j++)
     {
         handler->SummPerChannel[i]=+handler->BufferSampler[i].Sample[j];
         if( handler->SummPerChannel[i]  > 0x3FF)
          {
            handler->SummPerChannel[i] = 0x3FF;
          }
       }
     }
      for(int i = 0;i<13;i++)//���� �� �������
     {
      handler->RMSPerChannel[i] =handler->SummPerChannel[i]/1024;
     } 
  
  }
/*****************************************************************************/
void ClearIterator(void)
{
   DozimetrP.CounterIt = 0;
  
};
/*****************************************************************************/

void CheckStatisticTimeOut(void)
{
 // DozimetrP.ExpParams.End =  Clock_16b();
  //DozimetrP.StaticParams.MeasuredTime = GetTime(DozimetrP.ExpParams.Begin,DozimetrP.ExpParams.End);
  
  if(DozimetrP.StaticParams.MeasuredTime < DozimetrP.StaticParams.TimeOut)
  {
    DozimetrP.StatState= InProgress;
    DozimetrP.OcsParam.OscState = InProgress;
  }
  if(DozimetrP.StaticParams.MeasuredTime == DozimetrP.StaticParams.TimeOut)
  {
    
    
    DozimetrP.OcsParam.OscState = Complete;
    DozimetrP.StatState = Complete;
    ClearIterator();
  }
}
/*****************************************************************************/
void InitDozimetrP(Dozparams* handler)
{
 
  handler ->StaticParams.TimeOut = 200;
  for(int i = 0;i<13;i++)
  {
  handler->MinPerChannel[i] = 65535;
  }
  handler->SiMP_Thershold = 3000;
  handler->TrigEvent = SIPME;
  handler->StateWord = 0;
  handler->ControlReg = 0;
  handler->m_SourceOscp  = 0;
  handler->OcsParam.FilterInPixels = 0;
  handler->StaticParams.WorkState = NOT_WORK;
};
/*****************************************************************************/
void DeviceConfInit(void)
{
  SystemInit();
  RCC_HSEConfig(RCC_HSE_ON);

  while(!RCC_WaitForHSEStartUp())
  {
    
    
    
  }
  
  InitDozimetrP(&DozimetrP);
}
/*****************************************************************************/
void ADC_InjectMulti(uint8_t NumADC)
{
 // static uint16_t  Counter;
  switch(NumADC)
  {
    
  case 0:
    {
      
      
    }break;
    
  case 1:
    {
      
      
      
    }break;
    
  case 2:
    {
      
      
      
    }break;
    
    
    
  };
  
  
  
  
};
/*****************************************************************************/
void ADC_DMA_GET(void)
{
  
 
  
  
  
};
/*****************************************************************************/
void ADC_Interupt(void)
{
  
  
  
//   #ifdef _ADC_INTERUPT_MODE
//    ADC_ClearITPendingBit(ADC1,ADC_IT_EOC);
//    #endif
    //#ifdef _ADC_INJECT_MODE
    ADC_ClearITPendingBit(ADC1,ADC_IT_JEOC);
    DozimetrP.CurrentPerChannel[0]  = ADC_GetInjectedConversionValue(ADC1,ADC_InjectedChannel_1);
    DozimetrP.CurrentPerChannel[1]  = ADC_GetInjectedConversionValue(ADC1,ADC_InjectedChannel_2);
    DozimetrP.CurrentPerChannel[2]  = ADC_GetInjectedConversionValue(ADC1,ADC_InjectedChannel_3);
    DozimetrP.CurrentPerChannel[3]  = ADC_GetInjectedConversionValue(ADC1,ADC_InjectedChannel_4);
    //#endif
    static uint16_t  Counter;
    DozimetrP.SiMP_CurrentValue  = DozimetrP.CurrentPerChannel[10];
  if((DozimetrP.StateWord  & ( 1 << 1) ) && DozimetrP.CounterIt < 1024 && DataReady == true)
   {

       if( Counter % DozimetrP.OcsParam.ScaleinPixels == 0)
     {
//  #ifdef _ADC_INTERUPT_MODE
//      for(int i = 0;i<13;i++)
//      {
//        DozimetrP.BufferSampler[i].Sample[DozimetrP.CounterIt] = DozimetrP.CurrentPerChannel[i];
//
//      };
//   #endif
   //#ifdef _ADC_INJECT_MODE
   DozimetrP.BufferSampler[0].Sample[DozimetrP.CounterIt] =  ADC_GetInjectedConversionValue(ADC1,ADC_InjectedChannel_1);
   DozimetrP.BufferSampler[1].Sample[DozimetrP.CounterIt] =  ADC_GetInjectedConversionValue(ADC1,ADC_InjectedChannel_2);
   DozimetrP.BufferSampler[2].Sample[DozimetrP.CounterIt] =  ADC_GetInjectedConversionValue(ADC1,ADC_InjectedChannel_3);
   DozimetrP.BufferSampler[3].Sample[DozimetrP.CounterIt] =  ADC_GetInjectedConversionValue(ADC1,ADC_InjectedChannel_4);
   //#endif
   
      DozimetrP.CounterIt++; 
      GPIO_SetBits(GPIOB,GPIO_Pin_6);
      DozimetrP.StatState  = InProgress;
   }
   }
   if(DozimetrP.CounterIt == 1024)
   {
     GPIO_ResetBits(GPIOB,GPIO_Pin_6);
     DozimetrP.StatState  = Complete;
     DozimetrP.CurrentCommand =  RESERVED;
     DozimetrP.StateWord&=~( 1 << 1);
   }
    
  
  
  
  
  
}
/*****************************************************************************/
void ADC_TimeBased(void)
{
  
  
  
  if(TIM_GetITStatus(TIM2,TIM_IT_Update)!= RESET)
  {
  //#ifdef _TIMER_INTERUPT
    static uint16_t  Counter = 0;
    DozimetrP.SiMP_CurrentValue  = DozimetrP.CurrentPerChannel[10];
  if((DozimetrP.StateWord  & ( 1 << 1) ) && DozimetrP.CounterIt < 1024)
   {

       if( Counter % DozimetrP.OcsParam.ScaleinPixels == 0)
     {
  
      for(int i = 0;i<13;i++)
      {
        DozimetrP.BufferSampler[i].Sample[DozimetrP.CounterIt] = DozimetrP.CurrentPerChannel[i];

      };  
      DozimetrP.CounterIt++; 
      GPIO_SetBits(GPIOB,GPIO_Pin_6);
      DozimetrP.StatState  = InProgress;
   }
   }
   if(DozimetrP.CounterIt == 1024)
   {
     GPIO_ResetBits(GPIOB,GPIO_Pin_6);
     DozimetrP.StatState  = Complete;
     DozimetrP.CurrentCommand =  RESERVED;
     DozimetrP.StateWord&=~( 1 << 1);
   }  
    
  //#endif _TIMER_INTERUPT
    
 TIM_ClearITPendingBit(TIM2,TIM_IT_Update);
 //GPIOB->ODR^=GPIO_Pin_14;
  }
  
  
  
  
  
};



void Set_CS_PIN(uint8_t *Buff, int N)
{
   
   if (Buff[3] != 5)   return;
  
  __disable_irq();
//void EnableTransimt(void);
DissableTransimt();
  
 
  __enable_irq();
   
  /// int Ret = SendCommand (Buff, N);
  
   
   
}
void Reset_CS_PIN(uint8_t *Buff, int N)
{
   
   
    if (Buff[3] != 5)   return;
  
  __disable_irq();

  EnableTransimt();
 
  __enable_irq();
   
  /// int Ret = SendCommand (Buff, N);
 
   
   
   
}


void RestroreState(void)
{
   
   
    WriteDAC(DozimetrP.SiMP_Voltage*2);
      
  
   
   
}






void SaveState(void)
{
   
  
   
   
   
   
};
