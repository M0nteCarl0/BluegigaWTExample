#pragma once 
#include "stm32f4xx_flash.h"
#include "stm32f4xx_crc.h"
#include "stm32f4xx.h"
#include <string>
#include <string.h>
#ifndef __FLASH_IO
#define __FLASH_IO
/**************************************************************************
                     Template based flash library
                             30.1.2015
                           Molotaliev A.0
Example:
uxx/struct/class Data;(Max  object size = 64kb)
uxx/struct/class Data[Datacount];(Max objects 655535 with limit ROM Sector);
FlashIO Flash = FlashIO()
Flash.Write(Addr,Data,Datacount); -> For Static Allocated Types(Non determinated behavior on Dynamic allocated object);
Flash.Read(Addr,Data,Datacount);  -> For Static Allocated Types

Flash.Write(Addr,Data,Datacount,SizeOfOneObject); -> For Dynamic Allocated Types
Flash.Read(Addr,Data,Datacount,SizeOfOneObject);  -> For Dynamic  Allocated Types
*****************************************************************************/
typedef  enum FLASH_ERROR
{ 
  FLASH_ERROR_SUCCESFULY       = 0x0, 
  FLASH_ERROR_LARGE_OBJECT     = 0x10,//������� ������� ������ 
  FLASH_ERROR_CORUPTED_DATA    = 0x11,//������ ����������
  FLASH_ERROR_BAD_ADDRESS      = 0x12,//�������� �����
  FLASH_ERROR_BAD_SECTOR_INDEX = 0x13,
  FLASH_ERROR_OUT_OF_MEMORY    = 0x14
}FLE;

class FlashIO
{
public:
  FlashIO(void);
 ~FlashIO(void);
  FlashIO(FlashIO& FlashIO){delete this;};//DO NOT COPY !!
  /*************************************************************************
  Read/Write Function Overload
  **************************************************************************/
  template <typename T> FLASH_ERROR  Write(u32 Addres, T* Data, u32 Length );                 //Only for FS
  /**************************************************************************/
  template <typename T> FLASH_ERROR  Read(u32 Addres, T* Data, u32 Length);                   //Only For FS
  /**************************************************************************/
  template <typename T> FLASH_ERROR  Write(u32 Addres, T* Data, u32 Length,u32 SizeofVbrObj );//Only for FS
  /**************************************************************************/
  template <typename T> FLASH_ERROR  Read(u32 Addres, T* Data, u32 Length,u32 SizeofVbrObj);  //Only for FS
  /**************************************************************************/
  template <typename T> FLASH_ERROR  Write(u8 Sector, T* Data, u32 Length );                   //Simple User Friendly
  /**************************************************************************/
  template <typename T> FLASH_ERROR  Read(u8 Sector, T* Data, u32 Length);                     //Simple User Friendly
  /**************************************************************************/
  template <typename T> FLASH_ERROR  Write(u8 Sector, T* Data, u32 Length,u32 SizeofVbrObj );   //Simple User Friendly
  /**************************************************************************/
  template <typename T> FLASH_ERROR  Read(u8 Sector, T* Data, u32 Length,u32 SizeofVbrObj);      //Simple User Friendly
  //WITH AFRESS OFFSET - FOR ADVANCED IO(FOR EXAMPLE FILE SYSTEM ABSTRACTION)
  template <typename T> FLASH_ERROR  Write(u32 Addres,u32 Offset, T* Data, u32 Length );                 //Only for FS
  /**************************************************************************/
  template <typename T> FLASH_ERROR  Read(u32 Addres,u32 Offset, T* Data, u32 Length);                   //Only For FS
  /**************************************************************************/
  template <typename T> FLASH_ERROR  Write(u32 Addres,u32 Offset, T* Data, u32 Length,u32 SizeofVbrObj );//Only for FS
  /**************************************************************************/
  template <typename T> FLASH_ERROR  Read(u32 Addres,u32 Offset, T* Data, u32 Length,u32 SizeofVbrObj);  //Only for FS
  /**************************************************************************/
  template <typename T> FLASH_ERROR  Write(u8 Sector,u32 Offset,T* Data, u32 Length );                   //Simple User Friendly
  /**************************************************************************/
  template <typename T> FLASH_ERROR  Read(u8 Sector,u32 Offset, T* Data, u32 Length);                     //Simple User Friendly
  /**************************************************************************/
  template <typename T> FLASH_ERROR  Write(u8 Sector,u32 Offset, T* Data, u32 Length,u32 SizeofVbrObj );   //Simple User Friendly
  /**************************************************************************/
  template <typename T> FLASH_ERROR  Read(u8 Sector,u32 Offset,T* Data, u32 Length,u32 SizeofVbrObj);      //Simple User Friendly
  FLASH_ERROR GetLastError(void);
  void SetVoltageRange(uint8_t VoltageRange);
  void SetLatency(uint32_t  Latency); // -->see defines  STM32F4 Perph lib doc   
  void SetPrefetchBufferState(FunctionalState   PrefetchBufferState);
  void SetInstructionCacheState(FunctionalState InstructionCacheState);
  void SetDataCacheState(FunctionalState DataCacheState);
  void SetCheckDataState(FunctionalState CheckState);
  uint8_t  GetVoltageRange(void);
  uint32_t GetLatency(void);
  FunctionalState GetPrefetchBufferState(void);
  FunctionalState GetInstructionCacheState(void);
  FunctionalState GetDataCacheState(void);
  FunctionalState GetCheckDataState(void);
  void InstructionCacheReset(void);
  void DataCacheReset(void);
  void FormatFlash(void);
private:
 /**************************************************************************/
   template <typename T>  void Serialize(T& Object,uint8_t* Output);
  /**************************************************************************/
   template <typename T>  void Serialize(T& Object,uint8_t* Output,u32 SizeofVbrObj);
   /**************************************************************************/
   template <typename T>   T Deserialize(uint8_t* Input, T& ObjectType);
  /**************************************************************************/
   template <typename T>   T Deserialize(uint8_t* Input, T& ObjectType,u32 SizeofVbrObj);
   /**************************************************************************/
   template <typename T>  FLASH_ERROR CheckData( u32 Addres, T* Data, u32 Length);
   /**************************************************************************/
   template <typename T>  FLASH_ERROR  CheckData( u32 Addres, T* Data, u32 Lengt,u32 SizeofVbrObj);
   /**************************************************************************/
   bool SectorIsClean(uint8_t Sector);
   u32 GetHighAdressSector(uint8_t Sector);
   u32 GetFarAdressSector(uint8_t Sector);
   uint16_t GetSectorID(u32 Addres);
   uint8_t  GetSectorIndex(u32 Addres);
   uint32_t GetSectorSize(u32 Addres);
   bool _DataWriteSuccesfull;
   bool _CheckWritedData;
   uint8_t          _VoltageRange;
   uint32_t         _Latency;
   FunctionalState  _PrefetchBufferState;
   FunctionalState  _DataCacheState;
   FunctionalState  _InstructionCacheState;
   
};

/******************************************************************************/
template <typename T> FLASH_ERROR FlashIO::Write(u32 Addres, T* Data, u32 Length )
  {
        u32  it  = 0;
        u32 obit  = 0; 
        u32 obc = 0;
        uint8_t* Databuff = new uint8_t[sizeof(T)];
        FLASH_ERROR Error = FLASH_ERROR_SUCCESFULY;
        if(sizeof(T) > 65535)
        {
         return   FLASH_ERROR_LARGE_OBJECT ;
          
        }
        uint64_t SummaryObjSize = sizeof(T) * Length;
        if(SummaryObjSize >   GetSectorSize(Addres))
        {
          
          return  FLASH_ERROR_OUT_OF_MEMORY;
          
        }
        
        FLASH_Unlock();        
        FLASH_EraseSector(GetSectorID(Addres), _VoltageRange);
        do
        {
          Serialize(Data[obit],Databuff);
          it = 0;
        do{
        FLASH_ProgramByte(Addres, Databuff[it]);
        it++;
        obc++;
        Addres += obc;
        }while(it < sizeof(T));
        
        obit++;                 
        }
        while(obit <  Length );
        FLASH_Lock();
       if(_CheckWritedData)
       {
        Error =  CheckData(Addres, Data,  Length);
       };
        delete[]  Databuff;
        return Error;
  }
/******************************************************************************/
template <typename T> FLASH_ERROR  FlashIO::Write(u32 Addres, T* Data, u32 Length,u32 SizeofVbrObj )
  {
        u32  it  = 0;
        u32 obit  = 0; 
        u32 obc = 0;
        uint8_t* Databuff = new uint8_t[SizeofVbrObj];
        FLASH_ERROR Error = FLASH_ERROR_SUCCESFULY;
         if( SizeofVbrObj  > 65535)
        {
         return   FLASH_ERROR_LARGE_OBJECT;
          
        }
        uint32_t SummaryObjSize = Length *  SizeofVbrObj;
        if(SummaryObjSize >   GetSectorSize(Addres))
        {
          
          return  FLASH_ERROR_OUT_OF_MEMORY;
          
        }
        FLASH_Unlock();        
        FLASH_EraseSector(GetSectorID(Addres), _VoltageRange);
        do
        {
          Serialize(Data[obit],Databuff, SizeofVbrObj);
          it = 0;
        do{
        FLASH_ProgramByte(Addres, Databuff[it]);
        it++;
        obc++;
        Addres += obc;
        }while(it <  SizeofVbrObj);
        
        obit++;                 
        }
        while(obit <  Length );
        FLASH_Lock();
        
       if(_CheckWritedData)
       {
        Error =  CheckData(Addres, Data,  Length,SizeofVbrObj);
       }
        
        delete[]  Databuff;
    
        return  Error;
  }
/******************************************************************************/
template <typename T> FLASH_ERROR  FlashIO:: Read(u32 Addres, T* Data, u32 Length)
  {
    u32 it    = 0;
    u32 obit  = 0;
    u32 count = Length;
    u32 Obc = 0;
    FLASH_ERROR Error = FLASH_ERROR_SUCCESFULY;
    if( sizeof(T)  >  65535)
        {
         return   FLASH_ERROR_LARGE_OBJECT;
          
        }
    
     uint8_t* Databuff = new uint8_t[sizeof(T)] ;
     uint32_t SummaryObjSize = Length * sizeof(T);
        if(SummaryObjSize >   GetSectorSize(Addres))
        {
          
          return  FLASH_ERROR_OUT_OF_MEMORY;
          
        }
    
    
    
    do{
     
      do{
   Databuff[it] = *(u8*)Addres;
   it++;
   Obc++;
   Addres+=Obc;
  
        }while(it < sizeof(T));
      it = 0;
    Data[obit] = Deserialize(Databuff, Data[obit]) ;
    obit++;
      }while( obit < count);
    delete[]  Databuff;
    return Error;
  }

/******************************************************************************/
template <typename T> FLASH_ERROR FlashIO:: Read(u32 Addres, T* Data, u32 Length,u32  SizeofVbrObj)
  {
    u32 it    = 0;
    u32 obit  = 0;
    u32 count = Length;
    u32 Obc = 0;
   FLASH_ERROR Error = FLASH_ERROR_SUCCESFULY;
    if( SizeofVbrObj  > 65535)
        {
         return   FLASH_ERROR_LARGE_OBJECT;
 
        };
    uint8_t* Databuff = new uint8_t[SizeofVbrObj] ;
    
        uint32_t SummaryObjSize = Length * Length;
        if(SummaryObjSize >   GetSectorSize(Addres))
        {
          
          return  FLASH_ERROR_OUT_OF_MEMORY;
          
        }
    do{
     
      do{
   Databuff[it] = *(u8*)Addres;
   it++;
   Obc++;
   Addres+=Obc;
  
        }while(it <  SizeofVbrObj);
      it = 0;
    Data[obit] = Deserialize(Databuff, Data[obit],SizeofVbrObj) ;
    obit++;
      }while( obit < count);
    delete[]  Databuff;
    return Error;
  }
  /****************************************************************************
             Sector oriented overloads
             For Beginer Users
  
******************************************************************************/
template <typename T> FLASH_ERROR  FlashIO::Write(u8 Sector, T* Data, u32 Length )
  {
     
        u32  Addres  = GetFarAdressSector(Sector);
        FLASH_ERROR Error =   Write( Addres,Data,Length);
        return Error;
    
  }
/******************************************************************************/
template <typename T> FLASH_ERROR  FlashIO::Write(u8 Sector, T* Data, u32 Length,u32 SizeofVbrObj )
  {
        u32  Addres  = GetFarAdressSector(Sector);
        FLASH_ERROR Error =  Write( Addres,Data,Length,SizeofVbrObj);
        return Error;   
  }
/******************************************************************************/
template <typename T> FLASH_ERROR  FlashIO:: Read(u8 Sector, T* Data, u32 Length)
  {
    u32  Addres  = GetFarAdressSector(Sector);
    FLASH_ERROR Error = Read(Addres,Data,Length);
    return Error;
  }

/******************************************************************************/
template <typename T> FLASH_ERROR  FlashIO:: Read(u8 Sector, T* Data, u32 Length,u32  SizeofVbrObj)
  {
    u32  Addres  = GetFarAdressSector(Sector);
    FLASH_ERROR Error =  Read( Addres,Data,Length,SizeofVbrObj);
    return Error;
  }

  /****************************************************************************
                  Function overloads with offset parametr
                  For FS 
  
******************************************************************************/
template <typename T> FLASH_ERROR  FlashIO::Write(u8 Sector,u32 Offset, T* Data, u32 Length )
  {
        u32  Addres  = GetFarAdressSector(Sector) + Offset;
        FLASH_ERROR Error =   Write(Addres,Data,Length);
        return Error;
  }
/******************************************************************************/
template <typename T> FLASH_ERROR  FlashIO::Write(u8 Sector,u32  Offset, T* Data, u32 Length,u32 SizeofVbrObj )
  {
        u32  Addres  = GetFarAdressSector(Sector) + Offset;
        FLASH_ERROR Error =  Write( Addres,Data,Length,SizeofVbrObj);
        return Error;
  }
/******************************************************************************/
template <typename T> FLASH_ERROR  FlashIO:: Read(u8 Sector,u32 Offset, T* Data, u32 Length)
  {
    u32  Addres  = GetFarAdressSector(Sector) + Offset;
    FLASH_ERROR Error  =  Read( Addres,Data,Length);
    return Error;
  }

/******************************************************************************/
template <typename T> FLASH_ERROR  FlashIO:: Read( u8 Sector, u32 Offset ,T* Data, u32 Length,u32  SizeofVbrObj)
  {
     u32  Addres  = GetFarAdressSector(Sector) + Offset;
     FLASH_ERROR Error=  Read( Addres,Data,Length,SizeofVbrObj);
     return Error;
  }
  
  
/******************************************************************************/
template <typename T> FLASH_ERROR  FlashIO::Write(u32 Addres,u32 Offset, T* Data, u32 Length )
  {
        Addres  =  Addres + Offset;
        FLASH_ERROR Error =    Write(Addres,Data,Length);
        return Error;
  }
/******************************************************************************/
template <typename T> FLASH_ERROR  FlashIO::Write(u32 Addres,u32  Offset, T* Data, u32 Length,u32 SizeofVbrObj )
  {
        Addres  =  Addres + Offset;
        FLASH_ERROR Error = Write( Addres,Data,Length,SizeofVbrObj);
        return Error;

  }
/******************************************************************************/
template <typename T> FLASH_ERROR  FlashIO:: Read(u32 Addres,u32 Offset, T* Data, u32 Length)
  {
    Addres  =  Addres + Offset;
    FLASH_ERROR Error =  Read(Addres,Data,Length);
    return Error;
  }

/******************************************************************************/
template <typename T> FLASH_ERROR  FlashIO:: Read( u32 Addres, u32 Offset ,T* Data, u32 Length,u32  SizeofVbrObj)
  {
     Addres  =  Addres + Offset;
     FLASH_ERROR Error =  Read( Addres,Data,Length,SizeofVbrObj);
     return Error;
  }
  
/******************************************************************************/
template <typename T>  void FlashIO::Serialize(T& Object,uint8_t* Output)
   {
      memcpy(Output,(char*)&Object,sizeof(T));
   };
/******************************************************************************/
template <typename T>   T FlashIO:: Deserialize(uint8_t* Input, T& ObjectType)
   {
       T Object; 
       memcpy((char*)&Object,Input,sizeof(T));
       return Object;
   };
/******************************************************************************/
template <typename T>  void FlashIO::Serialize(T& Object,uint8_t* Output,u32 SizeofVbrObj )
   {
      memcpy(Output,(char*)&Object,SizeofVbrObj);
   };
/******************************************************************************/
template <typename T>   T FlashIO:: Deserialize(uint8_t* Input, T& ObjectType,u32 SizeofVbrObj)
   {
       T Object; 
       memcpy((char*)&Object,Input, SizeofVbrObj);
       return Object;
   };
/******************************************************************************/
template <typename T>  FLASH_ERROR FlashIO:: CheckData( u32 Addres, T* Data, u32 Length)
{
  FLASH_ERROR Error  =  FLASH_ERROR_CORUPTED_DATA;
  u32 SuccesCounter = 0;
  T* DataR = new T[Length];
  uint32_t* CRCR =  new uint32_t[Length];
  uint32_t* CRCW  = new uint32_t[Length];
  u32 Size = sizeof(T);
  CRC_ResetDR();
  
  Read(Addres, DataR, Length);
  for(u32 i = 0;i < Length;i++)
  {
    CRCR[i] =  CRC_CalcBlockCRC((uint32_t*)&DataR[i], Size);
    CRCW[i] =  CRC_CalcBlockCRC((uint32_t*)&Data[i], Size);
    if(CRCW[i] == CRCR[i])
    {
      
      SuccesCounter++;
      
    }
    
    
    if(SuccesCounter == Length)
    {
      Error =  FLASH_ERROR_SUCCESFULY;
      
      
    }
    
  }
  
  delete []  DataR;
  delete []  CRCR;
  delete []  CRCW;
  return Error;
}
/******************************************************************************/
template <typename T>  FLASH_ERROR FlashIO:: CheckData( u32 Addres, T* Data, u32 Length,u32 SizeofVbrOb)
{
  
  FLASH_ERROR Error  =  FLASH_ERROR_CORUPTED_DATA;
  u32 SuccesCounter = 0;
  T* DataR = new T[Length];
  uint32_t* CRCR =  new uint32_t[Length];
  uint32_t* CRCW  = new uint32_t[Length];
  CRC_ResetDR();
  Read(Addres, DataR, Length);
  for(u32 i = 0;i < Length;i++)
  {
    CRCR[i] =  CRC_CalcBlockCRC((uint32_t*)&DataR[i],SizeofVbrOb);
    CRCW[i] =  CRC_CalcBlockCRC((uint32_t*)&Data[i],SizeofVbrOb);
    if(CRCW[i] == CRCR[i])
    {
      
      SuccesCounter++;
      
    }
    
    
    if(SuccesCounter == Length)
    {
      Error =  FLASH_ERROR_SUCCESFULY;
      
      
    }
   
  }
  
  delete []  DataR;
  delete []  CRCR;
  delete []  CRCW;
  return Error;
};
/******************************************************************************/

#endif
//class File:private:FlashIO
//{
//  
// public:
// static  File& Open( const char* Name,const char* Atributes);
////  template<typename T>  operator << T* Input;
////  template<typename T>  operator >> T* Output;
//  void Flush();
//  char* GetFileName(void);
// private:
//  char*    _Name;
//  uint8_t  _Atributes;
//  uint32_t _Adress;
//  bool ReadOnly;
//  bool WriteOnly;
//};