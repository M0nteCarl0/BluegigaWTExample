#include "Protocol.hpp"

/*****************************************************************************/
IO::IO()
{
  
  Rx = new uint8_t[128];
  Tx = new uint8_t[128];
  TransferComplete  = false;
  RxOverFlow        = false;
  IndexTx = IndexRx = 0;
  Size = 128;
   
  
  
};
/*****************************************************************************/
IO::~IO()
{
  
 delete[] Rx ;
 delete[] Tx ;
   
  
  
};
/*****************************************************************************/
IO::IO( uint32_t SizeQ)
{
  
  Rx = new uint8_t[SizeQ];
  Tx = new uint8_t[SizeQ];
  TransferComplete  = false;
  RxOverFlow        = false;
  IndexTx = IndexRx = 0;
  Size = SizeQ;
   
  
  
};
/*****************************************************************************/
