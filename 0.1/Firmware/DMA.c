#include "DMA.h"
//#include "Commands.h"


uint16_t ADC_DATA;
uint8_t i;
/******************************************************************************
                      ������ ������ ������� � ������
*******************************************************************************/
void InitADC1_DMA(void)
{
  //#if _DMA_MODE || _ADC_INTERUPT_MODE 
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_DMA2,ENABLE);  

  DMA_InitTypeDef DMA_InitStructure; 
  DMA_InitStructure.DMA_Channel            = DMA_Channel_0;  
  DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)&ADC1->DR;
  DMA_InitStructure.DMA_Memory0BaseAddr    = (uint32_t)&ADC_DATA;
  DMA_InitStructure.DMA_DIR                = DMA_DIR_PeripheralToMemory;
  DMA_InitStructure.DMA_BufferSize         = 1; 
  DMA_InitStructure.DMA_PeripheralInc      = DMA_PeripheralInc_Disable;
  DMA_InitStructure.DMA_MemoryInc          = DMA_MemoryInc_Enable;
  DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
  DMA_InitStructure.DMA_MemoryDataSize     = DMA_MemoryDataSize_HalfWord;
  DMA_InitStructure.DMA_Mode               = DMA_Mode_Circular;
  DMA_InitStructure.DMA_Priority           = DMA_Priority_High ;
  DMA_InitStructure.DMA_FIFOMode           = DMA_FIFOMode_Disable;         
  DMA_InitStructure.DMA_FIFOThreshold      = DMA_FIFOThreshold_1QuarterFull;
  DMA_InitStructure.DMA_MemoryBurst        = DMA_MemoryBurst_Single;
  DMA_InitStructure.DMA_PeripheralBurst    = DMA_PeripheralBurst_Single;
  DMA_Init(DMA2_Stream0, &DMA_InitStructure);
  DMA_Cmd( DMA2_Stream0, ENABLE);
        
  DMA_ITConfig(DMA2_Stream0,DMA_IT_TC,ENABLE);
    Interupt();
  DMA_ITConfig(DMA2_Stream4,DMA_IT_TE|DMA_IT_DME| DMA_IT_FE, DISABLE);
//#endif
}

/*******************************************************************************/

void Interupt(void)
{
//#if _DMA_MODE
NVIC_InitTypeDef  NVT;
NVT.NVIC_IRQChannel                   = DMA2_Stream0_IRQn;
NVT.NVIC_IRQChannelCmd                = ENABLE;
NVT.NVIC_IRQChannelPreemptionPriority = 1;
NVT.NVIC_IRQChannelSubPriority        = 0;
NVIC_Init(&NVT);
NVIC_EnableIRQ(DMA2_Stream0_IRQn);
//#endif
  
};
