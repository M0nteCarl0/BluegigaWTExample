#include "RS485.h"
bool Receive = false;
bool Send = false;
/*-----------------------------------------------------------------------------*/
void InitRS485(void)
{ 
  IOInit();
  InteruptRS485();
  USART_InitTypeDef RS485;
  
  RS485.USART_BaudRate = 115200;
  
  RS485.USART_WordLength = USART_WordLength_9b;
  
  RS485.USART_Parity = USART_Parity_Odd;
  RS485.USART_StopBits = USART_StopBits_2;

  RS485.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
  RS485.USART_HardwareFlowControl =   USART_HardwareFlowControl_None;
  USART_Init (USART3, &RS485);
  USART_ITConfig (USART3,  USART_IT_TXE
                          |USART_IT_CTS|USART_IT_LBD
                          |USART_IT_TC |USART_IT_IDLE
                          |USART_IT_PE |USART_IT_ERR
                          |USART_IT_ORE|USART_IT_NE
                          |USART_IT_FE
                          ,DISABLE);
  USART_ITConfig (USART3, USART_IT_RXNE, ENABLE);
  USART_Cmd (USART3, ENABLE);
  
};
/*-----------------------------------------------------------------------------*/
void IOInit(void)
{

 
   RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB,ENABLE);
   RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART3,ENABLE);
   GPIO_PinAFConfig(GPIOB, GPIO_PinSource10, GPIO_AF_USART3);
   GPIO_PinAFConfig(GPIOB, GPIO_PinSource11, GPIO_AF_USART3);
   
  GPIO_InitTypeDef GPIO_InitStructure;
   /* Configure USART Tx as alternate function  */
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init(GPIOB, &GPIO_InitStructure);
  /* Configure USART Rx as alternate function  */
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_Pin =GPIO_Pin_11 ;
  GPIO_Init(GPIOB, &GPIO_InitStructure);
  /*Enable out*/

  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_12;
  ///GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
  GPIO_Init(GPIOB, &GPIO_InitStructure);
  
}
/*-----------------------------------------------------------------------------*/

void InteruptRS485(void)
{
  NVIC_InitTypeDef NVIC_InitStructure;
  NVIC_PriorityGroupConfig (NVIC_PriorityGroup_0);
  NVIC_InitStructure.NVIC_IRQChannel                    = USART3_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority         = 0;
  NVIC_InitStructure.NVIC_IRQChannelCmd                 = ENABLE;
  NVIC_Init (&NVIC_InitStructure);
}
/*-----------------------------------------------------------------------------*/
 void RS485DataProcess(void)
{
  uint8_t DataR;
  uint8_t DataT;
 
  int Ret;
   while (USART_GetITStatus (USART3, USART_IT_RXNE) != RESET){
      
      DataR = USART_ReceiveData (USART3);
      Ret = PutCharStack (DataR, &m_StackR);
      //DMA_ITConfig(DMA2_Stream0,DMA_IT_TC,ENABLE);
   }
  
   if (USART_GetITStatus(USART3, USART_IT_TXE) == SET){   
      
      Ret = GetCharStack (&DataT, &m_StackT);
      if (Ret){
         //DMA_ITConfig(DMA2_Stream0,DMA_IT_TC,ENABLE);
         USART_SendData (USART3, DataT);
         //DMA_ITConfig(DMA2_Stream0,DMA_IT_TC,ENABLE);
      }
      else{DMA_ITConfig(DMA2_Stream0,DMA_IT_TC,DISABLE);
         USART_ITConfig(USART3, USART_IT_TXE,  DISABLE);
         USART_ITConfig (USART3, USART_IT_TC,   ENABLE);    // 10.01.2013
         //DMA_ITConfig(DMA2_Stream0,DMA_IT_TC,ENABLE);
      }
   }
                                                            // 10.01.2013
   if (USART_GetITStatus(USART3, USART_IT_TC) != RESET){   
      
     //DMA_ITConfig(DMA2_Stream0,DMA_IT_TC,DISABLE);
      USART_ClearITPendingBit (USART3, USART_IT_TC);
      USART_ITConfig (USART3, USART_IT_TC,   DISABLE);
     // GPIO_ResetBits(GPIOB, GPIO_Pin_7);
      //DMA_ITConfig(DMA2_Stream0,DMA_IT_TC,ENABLE);
      RS485_Disable();
  
   }
  
  
  
  
  
}
