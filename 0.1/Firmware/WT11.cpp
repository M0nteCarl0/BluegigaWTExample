/******************************************************************************
         Bluetooth Module Driver version 0.1A
               Molotaliev A.O
            

Example:

 Driver_WT11 Bluetooth  = Driver_WT11();
 
 Bluetooth.Open();
 
 Bluetooth.Command(const char* Commmand);
 Bluetooth.Read(char Data,size_t Lenth);
 
 Bluetooth.Write(char Data,size_t Lenth);
 
 
 Bluetooth.Close(); is equal Bluetooth.~Driver_WT11();
 
 




*******************************************************************************/
#include "WT11.hpp"


 


/*****************************************************************************/
 Driver_WT11::Driver_WT11()
{
       
       
       
       
}
/*****************************************************************************/
Driver_WT11::~Driver_WT11()
{
   
   
   
}
/*****************************************************************************/
void  Driver_WT11:: Open()
{
   
   
   
   
   
}
/*****************************************************************************/
void   Driver_WT11:: Close()
{
   
   
   
}
/*****************************************************************************/
void    Driver_WT11:: Command(const char* Commmand)
{
   
}
/*****************************************************************************/
void  Driver_WT11:: Write(const char* Data,u16 DataCount)
{
   
   
   
   
}
/*****************************************************************************/
void  Driver_WT11::Write(const char Data)
{
   
   
}
/*****************************************************************************/
void  Driver_WT11:: Read(char* Data,u16 DataCount)
{
   
   
   
}
/*****************************************************************************/
void  Driver_WT11:: Read(char* Data)
{
   
   
   
}

/*****************************************************************************/
Driver_WT11_STATE  Driver_WT11:: ReadState(void)
{
   
   
   
}
/*****************************************************************************/
IWRAP_Mode    Driver_WT11::  ReadMode(void)
{
   
   
}
/*****************************************************************************/
void  Driver_WT11:: ResetDataCounters(void)
{
   _DataCounterRX =  0 ;
   _DataCounterTX = 0;
   
}
/*****************************************************************************/
void Driver_WT11:: ResetDataCounterRX(void)
{
   _DataCounterRX =  0 ;   
}
/*****************************************************************************/
void Driver_WT11:: ResetDataCounterTX(void)
{
   _DataCounterTX = 0;
   
}
/*****************************************************************************/
#if 0
private:
IWRAP_Mode  _Mode;
Driver_WT11_STATE _InternalState;
char _Command[];
char _DataBufferTX[BUFFER_SIZE_TX];
char _DataBufferRX[BUFFER_SIZE_RX];
u16 _DataCounterTX;
u16 _DataCounterRX;
#endif


