/**
*****************************************************************************
**
**  File        : stm32f4xx_it.c
**
**  Abstract    : Main Interrupt Service Routines.
**                This file provides template for all exceptions handler and
**                peripherals interrupt service routine.
**
**  Environment : Atollic TrueSTUDIO(R)
**                STMicroelectronics STM32F4xx Standard Peripherals Library
**
**  Distribution: The file is distributed
**                of any kind.
**
**  (c)Copyright Atollic AB.
**  You may use this file as-is or modify it according to the needs of your
**  project. Distribution of this file (unmodified or modified) is not
**  permitted. Atollic AB permit registered Atollic TrueSTUDIO(R) users the
**  rights to distribute the assembled, compiled & linked contents of this
**  file as part of an application binary file, provided that it is built
**  using the Atollic TrueSTUDIO(R) toolchain.
**
**
*****************************************************************************
*/

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_it.h"
#include <string.h>
 //extern bool trigered;
extern uint16_t ADC_DATA;
extern uint8_t i;
uint16_t ADC_Buffer[64]; //64 * 2s  = 128s = 2m 
static char DataBT;
static char DataPC;
extern  char DataBuff[512];
extern   u16 DataCounter;
extern  char SETCmd[] = "Hello WINDOWS  from STM32 + WT11i!\n";
extern char Commandbuff[128]; 
extern   int ComandLength ;
extern  u32 DataCounterTC;
extern bool _CommadMode;
static int  TXCOUNTER = 0;
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/

/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

/******************************************************************************/
/*            Cortex-M4 Processor Exceptions Handlers                         */
/******************************************************************************/

/**
  * @brief   This function handles NMI exception.
  * @param  None
  * @retval None
  */
void NMI_Handler(void)
{
}

/**
  * @brief  This function handles Hard Fault exception.
  * @param  None
  * @retval None
  */
void HardFault_Handler(void)
{
  /* Go to infinite loop when Hard Fault exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles Memory Manage exception.
  * @param  None
  * @retval None
  */
void MemManage_Handler(void)
{
  /* Go to infinite loop when Memory Manage exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles Bus Fault exception.
  * @param  None
  * @retval None
  */
void BusFault_Handler(void)
{
  /* Go to infinite loop when Bus Fault exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles Usage Fault exception.
  * @param  None
  * @retval None
  */
void UsageFault_Handler(void)
{
  /* Go to infinite loop when Usage Fault exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles SVCall exception.
  * @param  None
  * @retval None
  */
void SVC_Handler(void)
{
}

/**
  * @brief  This function handles Debug Monitor exception.
  * @param  None
  * @retval None
  */
void DebugMon_Handler(void)
{
}

/**
  * @brief  This function handles PendSVC exception.
  * @param  None
  * @retval None
  */
void PendSV_Handler(void)
{
}

/**
  * @brief  This function handles SysTick Handler.
  * @param  None
  * @retval None
  */
 void SysTick_Handler(void)
{
     //
   
  TimeInc();
}
/**
  * @brief  TIM2_IRQHandler
  *         This function handles Timer2 Handler.
  * @param  None
  * @retval None
  */
void TIM2_IRQHandler(void)
{
  
   if(TIM_GetITStatus(TIM2,TIM_IT_Update)!= RESET)
  {
     ///TIM_ClearITPendingBit(TIM2,TIM_IT_Update);
    ///GPIOD->ODR^= GPIO_Pin_13|GPIO_Pin_14|GPIO_Pin_15;
   // DAC_SetChannel1Data(DAC_Align_12b_R,10);
    
    
  }
  //USB_OTG_BSP_TimerIRQ();
}

/**
  * @brief  OTG_FS_IRQHandler
  *          This function handles USB-On-The-Go FS global interrupt request.
  *          requests.
  * @param  None
  * @retval None
//  */
//#ifdef USE_USB_OTG_FS
//void OTG_FS_IRQHandler(void)
//#else
//void OTG_HS_IRQHandler(void)
//#endif
//{
//  USBH_OTG_ISR_Handler(&USB_OTG_Core_dev);
//}



 void DMA2_Stream0_IRQHandler(void)
 {
   
 
 //DMA_ITConfig(DMA2_Stream0,DMA_IT_TC,ENABLE);
//  if(trigered)
//   {
//     if(i <64)
//     {
//     ADC_Buffer[i] = ADC_DATA;
//     };
//   i++;
//   if(i == 64)
//   {
//     
//     DMA_ITConfig(DMA2_Stream0,DMA_IT_TC,DISABLE);
//     i = 0;
//     trigered = false;
//     
//   }
//   
//     
// }
   
   //DMA_ITConfig(DMA2_Stream0,DMA_IT_TC,ENABLE);
   //DMA_ITConfig(DMA2_Stream0,DMA_IT_TC,DISABLE);
   
   
   
   
   
   
   
 };



 
 











void TIM3_IRQHandler(void)
{
  if(TIM_GetITStatus(TIM3,TIM_IT_Update)!= RESET)
  {
    
 //   DAC_SetChannel1Data(DAC_Align_12b_R,0);
//GPIOD->ODR^=GPIO_Pin_12;
   // i++;
 
//GPIOD->ODR^=(1<<11);
 
    
    //static uint16_t Counter;
    TIM_ClearITPendingBit(TIM3,TIM_IT_Update);
//{
//  static u8 Counter;
//  if( Counter> 13)
//  {
//  GPIOD->ODR^|=1<<Counter ;
//  }
//  else
//  {
//    Counter = 0;
//    
//  }

  }
};


void USART1_IRQHandler(void)
{
     
   uint8_t DataR;
  uint8_t DataT;
   
   
    int Ret;
   

 while(USART_GetITStatus (USART1, USART_IT_RXNE) != RESET){
      
        
        USART_ClearITPendingBit (USART1, USART_IT_RXNE);
        DataR = USART_ReceiveData (USART1) & 0xFF;
        if(_CommadMode)
        {
        DataBuff[DataCounter++] = DataR;
        if(DataCounter == 512)
        {
           DataCounter = 0;
             
        }
        }
        if(_CommadMode == false)
        {     
         
          Ret = PutCharStack (DataR, &m_StackR); 
          DataBuff[DataCounter] = DataR;
          DataCounter++;
        
           
        }
        
   };
 

 
 if(USART_GetITStatus (USART1, USART_IT_TC) != RESET){
      
        
          if(_CommadMode)
        {  
        USART_SendData(USART1, Commandbuff[DataCounterTC]);
        DataCounterTC++;

        u32 Size = ComandLength;
        if( DataCounterTC ==  Size)
        {
          USART_ClearITPendingBit (USART1, USART_IT_TC);
            USART_ITConfig(USART1,USART_IT_TC,DISABLE);
            
           
        };
   
     
   }
   if(_CommadMode == false)    
   {
       //USART_ClearITPendingBit (USART1, USART_IT_TC);
      // USART_ITConfig(USART1,USART_IT_TC,DISABLE); 
   Ret = GetCharStack (&DataT, &m_StackT);
   
       if(Ret)
       {
     
        //Ret = GetCharStack (&DataT, &m_StackT);
        USART_SendData (USART1, DataT);
        USART_ClearITPendingBit (USART1, USART_IT_TC);
        
       }

      if (Ret!= 1){
     
         
          //USART_ClearITPendingBit (USART1, USART_IT_TC);
            USART_ITConfig(USART1,USART_IT_TC,DISABLE);
            //USART_ITConfig (USART1, USART_IT_RXNE,  ENABLE);  
         
      }
      
      
   }
 
 
 
 
 
 
 
 }
 
 
 
 
// 
// 
//#if 0 
//
// if (USART_GetITStatus(USART1, USART_IT_TXE) == SET){   
//#if 0
//      Ret = GetCharStack (&DataT, &m_StackT);
//      if (Ret){
//       
//         USART_SendData (USART1, DataT);
//      }
//      else{
//         USART_ITConfig(USART1, USART_IT_TXE,  DISABLE);
//         USART_ITConfig (USART1, USART_IT_TC,   ENABLE);    // 10.01.2013
//        
//      }
//      
//#endif
//      
//      do
//       {
//        Ret = GetCharStack (&DataT, &m_StackT);
//        USART_SendData (USART1, DataT);
//        TXCOUNTER++;
//       }while(Ret);
//      
//      
//      if (Ret!= 1 ){
//     
//         
//          USART_ClearITPendingBit (USART1, USART_IT_TXE);
//           USART_ITConfig(USART1, USART_IT_TXE,  DISABLE);
//            USART_ITConfig(USART1,USART_IT_TC,ENABLE);
//      
//      
//   }
//
//#endif
// 
// }
// 
// {
// 
////
////
//// if(USART_GetITStatus (USART1, USART_IT_TC) != RESET){
////      
////     DataBT  =  USART1->DR;
////     USART_ClearITPendingBit (USART1, USART_IT_TC);
////     USART_ClearITPendingBit (USART1, USART_IT_TXE);
//    // USART3->DR = ::DataBT;               //USART_ReceiveData (USART3);
//     // Ret = PutCharStack (DataR, &m_StackR);
//     
//   //};
//}
//
//   
//   
//   
//}
}
   
   
   
   
   
   
   
   
   
      
        

   







void USART3_IRQHandler (void)
{

   
   
    uint8_t DataR;
  uint8_t DataT;
 
  int Ret;
   while (USART_GetITStatus (USART3, USART_IT_RXNE) != RESET){
      
      DataR = USART_ReceiveData (USART3);
      Ret = PutCharStack (DataR, &m_StackR);
      
   }
  
   if (USART_GetITStatus(USART3, USART_IT_TXE) == SET){   
      
      Ret = GetCharStack (&DataT, &m_StackT);
      if (Ret){
       
         USART_SendData (USART3, DataT);
      }
      else{
         USART_ITConfig(USART3, USART_IT_TXE,  DISABLE);
         USART_ITConfig (USART3, USART_IT_TC,   ENABLE);    // 10.01.2013
        
      }
   }
                                                            // 10.01.2013
   if (USART_GetITStatus(USART3, USART_IT_TC) != RESET){   
      
      USART_ClearITPendingBit (USART3, USART_IT_TC);
      USART_ITConfig (USART3, USART_IT_TC,   DISABLE);
      RS485_Disable();
  
   }
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
}












/******************************************************************************/
/*                 STM32F4xx Peripherals Interrupt Handlers                   */
/*  Add here the Interrupt Handler for the used peripheral(s) (PPP), for the  */
/*  available peripheral interrupt handler's name please refer to the startup */
/*  file (startup_stm32f4xx.s).                                               */
/******************************************************************************/

/**
  * @brief  This function handles PPP interrupt request.
  * @param  None
  * @retval None
  */
/*void PPP_IRQHandler(void)
{
}*/

