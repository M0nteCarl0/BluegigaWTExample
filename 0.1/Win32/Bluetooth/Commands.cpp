#include "StdAfx.h"
#include "Commands.h"


Commands::Commands(void)
{
	Sender = new MxComm(COMM_TYPE::WINDOWS_API,196);
	Sender->Configure(115200,8,0,1);
	m_FlgDebugS = true;
   m_FlgDebugR = true;
   m_FlgCheckCRCwrite = true;
   m_FlgCheckCRCread  = true;
}


Commands::~Commands(void)
{
	Sender->~MxComm();
}


void Commands::TurnOffLed(void)
{

	int NR =7;
	DWORD Numrec = 0;;
	m_DataS[2] = 0x2;

	WriteCommand(m_DataS,NR);



	//Sender->WRcommand(m_DataS,2,m_DataR,NR,&Numrec,false,false);




};



void Commands::TurnOnLed(void)
{


	int NR =7;
	DWORD Numrec = 0;;
	m_DataS[2] = 0x1;

	WriteCommand(m_DataS,NR);



}


int Commands::WriteCommand (BYTE *DataS, int NS)
{

	DataS [0] = mID;
	DataS [1] = CurrentModuleAddres;
     DataS [3] = NS;                   // Amount of bytes

   if (m_FlgCheckCRCwrite)
      AddCRC (DataS, NS);

   int NR = NS;
   DWORD NumRec;

   bool Ret = Sender->WRcommand (DataS, NS, m_DataR, NR, &NumRec, m_FlgDebugS, m_FlgDebugR);

   if (!Ret)                     return 0x01;
   if (NR != NumRec)             return 0x02;
   if (m_FlgCheckCRCread && !CheckCRC (m_DataR, NR))  return 0x03;

   int Er = CheckErrorWR (DataS, NS, m_DataR, NR);
   return Er;



}



void Commands::AddCRC (BYTE *Data, int N)
{
   if (N < 5) return;

   Data [4] = MakeCRC (Data, 4);

   if (N > 6){
      Data [N-1] = MakeCRC (&Data[5], N-6);
   }
};




bool Commands::CheckCRC (BYTE *Data, int N)
{
   if (N < 5) return false;

   BYTE CRC = MakeCRC (Data, 4);
   if (CRC != Data[4]) 
      return false;

   if (N > 6){
      CRC = MakeCRC (&Data[5], N-6);
      if (CRC != Data[N-1]) 
         return false;
   }
   return true;
};




BYTE Commands::MakeCRC (BYTE *Data, int N)
//    ���������� CRC ����� �������� 
{
   int Summ = 0;
   for (int i = 0; i < N; i++){
      Summ += Data[i];
   }
   int SummH = (Summ >> 7) & 0x7F;

   BYTE mCRC = (BYTE) (SummH + (Summ & 0x7F));

   mCRC &= 0x7F;
   return mCRC;
};



int Commands::CheckErrorWR (BYTE *DataS, int NS, BYTE *DataR, int NR)
{
   if (NR < 5)                return 0x10;
//   if (NS != NR)              return 0x20;
   if (DataR[3] != NR)        return 0x30;

   return 0;
};