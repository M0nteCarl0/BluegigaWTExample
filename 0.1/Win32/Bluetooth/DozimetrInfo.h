#include <stdint.h>
#include <Windows.h>
#pragma once
typedef  enum EComamnd:uint8_t{
  
  WRITE_SiPM_THERSH       = 0x1,//����� ������� SimP
  WRITE_CONTROL_DOZ       = 0x2, //�������� ������� ���������
  START_STATISTIC         = 0x3, //�����, �������� ���������� ���, ���, �������, ��� ��� 13������� ����������
  CLEAR_STATE_WORD        = 0x4, //�������� ��������� �����
  SET_PARAM_OSCIL         = 0x5, //������ ��������� ������������
  SET_WAIT_OSCIL          = 0x6, //������ �������� ��������� ������������	������ �� ��������� ������ ��������, ���������� ������
  WRITE_SiMP_VOLTAGE      = 0x7, //������ ���������� �� SiPM
  WRITE_SERIAL            = 0x3F,//������ ������ ������ (FLASH)
  READ_SW_HW              = 0x40,//������ ��� � ������ ������
  READ_SiMP_THERSH        = 0x41,//����� �������� (FLASH)
  READ_CONTROLR           = 0x42,//������� ����������
  READ_COMPLETE_CONV      = 0x43,//��������� ����������� �� ��������� ����������
  READ_STATE_WORD         = 0x44,//������ ��������� ����� ���������
  READ_OSC_PARAMS         = 0x45,//������ ��������� ������������
  READ_STATE_WORD_OSC     = 0x46,//������ ��������� ����� ������������ 
  READ_SiMP_VOLTAGE       = 0x47,//������ ���������� �� SiPM
  READ_PARAMS_MESURED_EXP = 0x50,//��������� ���������� ����������  
  READ_STATE_PER_CHANEL   = 0x51,//��������� ���������� ���, ���, �������, ��� ��� 13������� ����������
  READ_CURRENT_ADC_VALUES = 0x52,//������ ������� �������� ADC 13������� ����������
  READ_AMPLITUDES_OSC     = 0x53,//������ ��������� ������������ 
  READ_SERIAL             = 0x7F //������ ����� ������ (FLASH)
}Command;
