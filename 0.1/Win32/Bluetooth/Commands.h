#pragma once
#include "MXCOMM.H"
#define  NCOM   128
#define mID 0x96 
#define CurrentModuleAddres 0x4

class Commands
{
public:
	Commands(void);
	~Commands(void);
	int WriteCommand (BYTE *DataS, int NS);
	void TurnOnLed(void);
	void TurnOffLed(void);


private:
	  MxComm* Sender;
	  BYTE     m_DataS [NCOM];
      BYTE     m_DataR [NCOM];

	  bool  m_FlgCheckCRCwrite;
	  bool  m_FlgCheckCRCread;
	  bool  m_FlgDebugS;
	  bool  m_FlgDebugR;

	  BYTE  MakeCRC (BYTE *Data, int N);
	  void	  AddCRC (BYTE *Data, int N);
	  bool CheckCRC (BYTE *Data, int N);
	  int CheckErrorWR (BYTE *DataS, int NS, BYTE *DataR, int NR);
};

