//----------------------------------------------------------------------------
//    Create by Novikov
//    data 25.12.2012
//----------------------------------------------------------------------------
#ifndef __MYCOMMAND_H__
#define __MYCOMMAND_H__
#pragma once
#include "BluetoothRFCOMM.h"
#include <Windows.h>
#include "mxcomm.h"
#include <stdint.h>
#include "DozimetrInfo.h"

//LEGACY
#define  NCOM   128
#define  mID    0x96
#define  mADDR  0x01
#define  AKadr  0x02 
#define  BKadr  0x03

//----------------------------------------------------------------------------


enum DeviceAdress:BYTE
{
	BE = 0x01,
	AK = 0x02,
	BK = 0x03, 
	DO = 0x04
	
};

class MyCommand
{
   public:
      bool m_FlgDebugS;
      bool m_FlgDebugR;
      bool m_FlgCheckCRCwrite;
      bool m_FlgCheckCRCread;
	  bool InitSucces;
	  int LastError;
	  UINT CurrentModuleID;
	  UINT CurrentModuleAddres;
	  unsigned long DataBufferSize;
      MxComm   m_Comm;
	  BluetoothRFCOMM _BT;
	  UINT  CurrBaudrate;
	  DeviceAdress DeviceAllis;
      BYTE     m_DataS [NCOM];
      BYTE     m_DataR [NCOM];
      MyCommand ();
	  MyCommand(DeviceAdress Device);
	  
	  MyCommand (UINT Device);
      virtual ~MyCommand ();

	  bool InitComm ();
	  void SetDeviceTarget(DeviceAdress TargetDevice);
	  void ClearDataBuffes(void);

	  void SetDeviceTarget(BYTE TargetDevice);

	  void BindAk(void);
	  void BindBK(void);
	  void BindBE(void);

      void TwoBytesWord (BYTE Byte1, BYTE Byte2, int *WordOut);

      BYTE MakeCRC   (BYTE *Data, int N);
      void AddCRC    (BYTE *Data, int N);
      bool CheckCRC  (BYTE *Data, int N);

      int  CheckErrorWR (BYTE *DataS, int NS, BYTE *DataR, int NR);
      int  WriteCommand (BYTE *DataS, int NS);
      int  ReadCommand  (BYTE *DataS, int NS, BYTE *DataR, int NR);

                                                              // Return 0 == OK
      int WriteLedTurnOn      ();
      int WriteLedTimeAmpl    (int Time, int Ampl);
      int WriteDozaLevel      (int DozaLevel1, int DozaLevel2, int DozaLevel3);
      int WriteTriggerLevel   (int TriggerLevel, BYTE *Crypto_Buff);
      int WriteControl        (BYTE Control);
      int WriteStartStatistic (BYTE Statistic);
      int WriteDozeDivider (int DozeDivider1, int DozeDivider2, int DozeDivider3, BYTE *Crypto_Buff);
      int WriteClearSW        ();
      int WriteParamOscp      (int Scale, int Source, int Filter);
      int WriteStartOscp      ();
      int WriteCoverTime      (int Time);      // ����� ����������� � ����������, ���� �������� ������� ��������, (��)  5+2=7
      int WriteModuleNumber   (int Year, int Number, BYTE *Crypto_Buff);

	  //BK_WR
	  int WriteActiveRawFlash    (WORD *Raw, BYTE *Crypto_Buff);
	  int WriteReferenseRawFlash (WORD *Raw, BYTE *Crypto_Buff);

	  //BK_R
	  int ReadActiveRawFlash     (WORD *Raw, BYTE *Crypto_Buff);
	  int ReadReferenseRawFlash  (WORD *Raw, BYTE *Crypto_Buff);
      int ReadVersion      (BYTE *Name, BYTE *Hard, BYTE *Soft);
      int ReadLedTimeAmpl  (int *Time, int *Ampl);
      int ReadDozaLevel    (int *DozaLevel1, int *DozaLevel2, int *DozaLevel3);
      int ReadTrigerLevel  (int *TrigerLevel, BYTE *Crypto_Buff);
      int ReadControl      (BYTE *Control);
      int ReadDozeDivider  (int *DozeDivider1, int *DozeDivider2, int *DozeDivider3, BYTE *Crypto_Buff);
      int ReadStatus       (BYTE *Status1, BYTE *Status2);
      int ReadExpositionData (int *Time, int *TrgMean, int *TrgDoza, int *IntMean1, int *IntDoza1, int *IntMean2, int *IntDoza2, BYTE *CounterExpo);
	 // int ReadExpositionData (int *Time, int *Avergage, int *TrgDoza, int *IntMean1, int *IntDoza1, int *IntMean2, int *IntDoza2, BYTE *CounterExpo);
      int ReadStatisticData   (BYTE Source, int *Amax, int *Amin, int *Amean, int *Arms);
      int ReadAmplitudes      (int *Atrg, int *AtrgF, int *Aint1, int *Aint2);
      int ReadAmpOscp         (int Source, int J, int *Ampl);
      int ReadStatusOscp      (int *StatusOscp);
      int ReadCoverTime       (int *CoverTime);
      int ReadModuleNumber    (int *Year, int *Number, BYTE *Crypto_Buff);
//void ReadVersion(uint8_t *Buff, int N);//������ ��� � ������ ������
	  void ReadSiMP_Thershold(uint16_t *Thershold);//������ ����� SiMP
	  void ReadControlDoz(uint8_t *Control);       //������ ���������� ������� ���������
	  void ReadStatIsDone(uint8_t *StateStatic);    //������ �� ���������� �� ����� ����������
	  void ReadState(uint8_t *StateWord);         //������ ��������� ����� ���������
	  void ReadOscParams(uint8_t *ScaleInPix);     //������ ��������� �����������
	  void ReadStateOsc(uint8_t *OscStateWord);      //������ ��������� ����� ������������ 
	  void ReadSiMP_Voltage(uint8_t *SiMP_Volt);  //������ ���������� �� SiMP 
	  void ReadExpositionParams(uint16_t *Time,uint16_t *Avergage,uint8_t* Count);//��������� ���������� ����������
	  void ReadStaticPerChannel(uint8_t *Chanel, uint16_t Max,uint16_t* Min,uint16_t* Avg,uint16_t* RMS);//��������� ���������� ���, ���, �������, ��� ��� 13������� ����������
	  void ReadADCValues(uint16_t *Values);       //������ ���(�������) �������� �������  
	  void ReadAmpsOsc(uint16_t *Amplitudes,uint8_t Chanel,uint8_t Offset);         //������ ��������� ������������ 
	  void ReadModuleNumber(uint32_t *Year,uint32_t *Number, uint8_t *Crypto_Buffer);
	  void WriteSiMP_Thershold(uint16_t *Thershold);//�������� ����� ������������ �������� SiMP
	  void WriteControlReg(uint8_t *Contro);    //�������� ����������� ������� ���������
	  void StartStatist(void);       //������ ��������� ����������
	  void ClearStateWord(void);     //�������� ��������� �����
	  void WriteOscParams(uint8_t* ScaleInPixel);     //�������� ��������� �����������
	  void WaitMeasureOsc(void);     //������ �������� 
	  void WriteSiMP_Voltage(uint8_t* Voltage);
	  void WriteModuleNumber(uint32_t *Year,uint32_t *Number, uint8_t *Crypto_Buffer);
	  bool SetSpeed(UINT Baudrate);
};
//----------------------------------------------------------------------------
#endif
