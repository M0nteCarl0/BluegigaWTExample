//----------------------------------------------------------------------------
//    Create by Novikov
//    data 25.12.2012
//----------------------------------------------------------------------------
//#include <process.h>
#include <stdio.h>

#include "MyCommand.h"


void MyCommand::ClearDataBuffes(void)
{

	memset(m_DataR,0,128);

	memset(m_DataS,0,128);


}
MyCommand::MyCommand (UINT Device)
{
   m_FlgDebugS = true;
   m_FlgDebugR = true;
   m_FlgCheckCRCwrite = true;
   m_FlgCheckCRCread  = true;
   InitSucces = false;
//   DeviceAllis = Device;
   CurrentModuleAddres = Device;
   CurrentModuleID = mID;
};
//----------------------------------------------------------------------------
MyCommand::MyCommand (DeviceAdress Device)
{
   m_FlgDebugS = true;
   m_FlgDebugR = true;
   m_FlgCheckCRCwrite = true;
   m_FlgCheckCRCread  = true;
   InitSucces = false;
   DeviceAllis = Device;
   CurrentModuleAddres = (UINT)Device;
   CurrentModuleID = mID;
   
};

//----------------------------------------------------------------------------
MyCommand::MyCommand ()
{
   m_FlgDebugS = true;
   m_FlgDebugR = true;
   m_FlgCheckCRCwrite = true;
   m_FlgCheckCRCread  = true;
   InitSucces = false;
   //DeviceAllis = mADDR;
   //BluetoothRFCOMM();
   CurrentModuleAddres = mADDR;
   CurrentModuleID = mID;
};
//----------------------------------------------------------------------------
MyCommand::~MyCommand ()
{

};
//----------------------------------------------------------------------------
bool MyCommand::InitComm ()
{
   UINT nSpeed    = 19200; 
   UINT nDataBits = 8; 
   UINT nParity   = 1; 
   UINT nStopBits = 2;
   int TimeOut    = 0;

   CurrBaudrate = nSpeed;
   bool Ret = m_Comm.Configure (nSpeed, 
                                 nDataBits, 
                                 nParity, 
                                 nStopBits);


   

   if (Ret)
   {
	   InitSucces = true;
	  m_Comm.SetTimeout (120);
	  return true;
   }
   else
   {

	   InitSucces = false;
	   return false;
   }
 
};



//------------------------------------------------------------------------------
void MyCommand::TwoBytesWord (BYTE Byte1, BYTE Byte2, int *WordOut)
{

  *WordOut  = (Byte1 & 0x7F);
  *WordOut |= ((((int) Byte2) & 0x7F) << 7);
};
//----------------------------------------------------------------------------
//    WRITE       WRITE       WRITE       WRITE       WRITE       WRITE       
//----------------------------------------------------------------------------
BYTE MyCommand::MakeCRC (BYTE *Data, int N)
//    ���������� CRC ����� �������� 
{
   int Summ = 0;
   for (int i = 0; i < N; i++){
      Summ += Data[i];
   }
   int SummH = (Summ >> 7) & 0x7F;

   BYTE mCRC = (BYTE) (SummH + (Summ & 0x7F));

   mCRC &= 0x7F;
   return mCRC;
};
//----------------------------------------------------------------------------
/*
BYTE MyCommand::MakeCRC (BYTE *Data, int N)
{
   BYTE CRC = 0xFF;
   for (int i = 0; i < N; i++){
      CRC = CRC ^ Data[i];
   }
   CRC &= 0x7F;
   return CRC;
};
*/
//----------------------------------------------------------------------------
void MyCommand::AddCRC (BYTE *Data, int N)
{
   if (N < 5) return;

   Data [4] = MakeCRC (Data, 4);

   if (N > 6){
      Data [N-1] = MakeCRC (&Data[5], N-6);
   }
};
//----------------------------------------------------------------------------
bool MyCommand::CheckCRC (BYTE *Data, int N)
{
   if (N < 5) return false;

   BYTE CRC = MakeCRC (Data, 4);
   if (CRC != Data[4]) 
      return false;

   if (N > 6){
      CRC = MakeCRC (&Data[5], N-6);
      if (CRC != Data[N-1]) 
         return false;
   }
   return true;
};
//----------------------------------------------------------------------------
int MyCommand::CheckErrorWR (BYTE *DataS, int NS, BYTE *DataR, int NR)
{
   if (NR < 5)                return 0x10;
//   if (NS != NR)              return 0x20;
   if (DataR[3] != NR)        return 0x30;

   return 0;
};
//----------------------------------------------------------------------------
int MyCommand::WriteCommand (BYTE *DataS, int NS)
{
	DataS [0] = mID;
	DataS [1] = CurrentModuleAddres;
   DataS [3] = NS;                   // Amount of bytes

   if (m_FlgCheckCRCwrite)
      AddCRC (DataS, NS);

   int NR = NS;
   DWORD NumRec;
   _BT.WriteOnly = true;
   //bool Ret = m_Comm.WRcommand (DataS, NS, m_DataR, NR, &NumRec, m_FlgDebugS, m_FlgDebugR);
 bool Ret=  _BT.WRcommand_BT (DataS, NS, m_DataR, NR, &NumRec, m_FlgDebugS, m_FlgDebugR);
   if (!Ret)                     return 0x01;
   if (NR != NumRec)             return 0x02;
   if (m_FlgCheckCRCread && !CheckCRC (m_DataR, NR))  return 0x03;

   int Er = CheckErrorWR (DataS, NS, m_DataR, NR);
   return Er;
};

//----------------------------------------------------------------------------

void MyCommand::SetDeviceTarget(DeviceAdress TargetDevice)
{

	

	
	//m_Comm.PurgeRX();
	//m_Comm.PurgeTX();
	// m_Comm.Close();

 //  UINT nSpeed    =  CurrBaudrate; //115200; 
 //  UINT nDataBits = 8; 
 //  UINT nParity   = 1; 
 //  UINT nStopBits = 2;
 //  int TimeOut    = 0;
 //  
 //  bool Ret = m_Comm.Configure (nSpeed, 
 //                                nDataBits, 
 //                                nParity, 
 //                                nStopBits);

	CurrentModuleID = mID;
	CurrentModuleAddres = (BYTE)TargetDevice;

	DeviceAllis = TargetDevice;


};

//----------------------------------------------------------------------------

void MyCommand::SetDeviceTarget(BYTE TargetDevice)
{

	//m_Comm.PurgeRX();
	//m_Comm.PurgeTX();

	// m_Comm.Close();

 //  UINT nSpeed    =  CurrBaudrate; //115200; 
 //  UINT nDataBits = 8; 
 //  UINT nParity   = 1; 
 //  UINT nStopBits = 2;
 //  int TimeOut    = 0;
 //  
 //  bool Ret = m_Comm.Configure (nSpeed, 
 //                                nDataBits, 
 //                                nParity, 
 //                                nStopBits);

	CurrentModuleID = mID;
	CurrentModuleAddres = TargetDevice;

	
	







};

//----------------------------------------------------------------------------

void MyCommand::BindAk(void)
{

	CurrentModuleAddres = AKadr;

}

//----------------------------------------------------------------------------
void MyCommand::BindBE(void)
{

	CurrentModuleAddres = mADDR;

}


//----------------------------------------------------------------------------

void MyCommand::BindBK(void)
{

	CurrentModuleAddres = BKadr;

}


//----------------------------------------------------------------------------
int MyCommand::ReadCommand (BYTE *DataS, int NS, BYTE *DataR, int NR)
{
	DataS [0] =  mID;
	DataS [1] = CurrentModuleAddres;

   DataS [3] = NS;                   // Amount of bytes

   if (m_FlgCheckCRCwrite)
      AddCRC (DataS, NS);

   DWORD NumRec;
   _BT.WriteOnly = false;
  // bool Ret = m_Comm.WRcommand (DataS, NS, DataR, NR, &NumRec, m_FlgDebugS, m_FlgDebugR);
  bool Ret =   _BT.WRcommand_BT (DataS, NS, m_DataR, NR, &NumRec, m_FlgDebugS, m_FlgDebugR);
  //if(_BT.GetLastError() == RF
   if (!Ret)                     return 0x01;
   if (NR != NumRec)             return 0x02;
   if (m_FlgCheckCRCread && !CheckCRC (m_DataR, NR))  return 0x03;

   int Er = CheckErrorWR (DataS, NS, DataR, NR);
   LastError = Er;
   return Er;
};
//----------------------------------------------------------------------------
int MyCommand::WriteLedTurnOn ()
{
   int NS = 7;                         // Amount of Bytes
   m_DataS [2] = 0x01;                 // Commanda

   m_DataS [5] = 0x01;

   int Er = WriteCommand (m_DataS, NS);

   return Er;
};
//----------------------------------------------------------------------------
int MyCommand::WriteLedTimeAmpl (int Time, int Ampl)
{
   int NS = 10;                        // Amount of Bytes
   m_DataS [2] = 0x02;                 // Commanda

   m_DataS [5] =  Time & 0x7F;
   m_DataS [6] = (Time >> 7) & 0x7F;
   m_DataS [7] =  Ampl       & 0x7F;;
   m_DataS [8] = (Ampl >> 7) & 0x7F;

   int Er = WriteCommand (m_DataS, NS);
   LastError = Er;
   return Er;
};
//----------------------------------------------------------------------------
int MyCommand::WriteDozaLevel (int DozaLevel1, int DozaLevel2, int DozaLevel3)
{
   int NS = 12;                        // Amount of Bytes 5+6+1=12
   m_DataS [2] = 0x03;                 // Commanda

   m_DataS [5] =  DozaLevel1       & 0x7F;
   m_DataS [6] = (DozaLevel1 >> 7) & 0x7F;

   m_DataS [7] =  DozaLevel2       & 0x7F;
   m_DataS [8] = (DozaLevel2 >> 7) & 0x7F;

   m_DataS [9]  =  DozaLevel3       & 0x7F;
   m_DataS [10] = (DozaLevel3 >> 7) & 0x7F;

   int Er = WriteCommand (m_DataS, NS);
   LastError = Er;
   return Er;
};
//----------------------------------------------------------------------------
int MyCommand::WriteTriggerLevel (int TriggerLevel, BYTE *Crypto_Buff)
{
   int NS = 38;                        // Amount of Bytes  5+32+1=48
   m_DataS [2] = 0x04;                 // Commanda

   m_DataS [5] =  TriggerLevel       & 0x7F;
   m_DataS [6] = (TriggerLevel >> 7) & 0x7F;

//   BYTE Txt[30] = {1,2,3,4,5,6,7,8,9};
   memcpy (&m_DataS[7], Crypto_Buff, 30);

   int Er = WriteCommand (m_DataS, NS);
   LastError = Er;
   return Er;
};
//----------------------------------------------------------------------------
int MyCommand::WriteControl (BYTE Control)
{
   int NS = 7;                         // Amount of Bytes
   m_DataS [2] = 0x05;                 // Commanda

   m_DataS [5] =  Control;

   int Er = WriteCommand (m_DataS, NS);
   LastError = Er;
   return Er;
};
//----------------------------------------------------------------------------
int MyCommand::WriteStartStatistic (BYTE Statistic)
{
   int NS = 7;                         // Amount of Bytes
   m_DataS [2] = 0x06;                 // Commanda

   m_DataS [5] =  Statistic;

   int Er = WriteCommand (m_DataS, NS);
   LastError = Er;
   return Er;
};
//----------------------------------------------------------------------------
int MyCommand::WriteDozeDivider (int DozeDivider1, int DozeDivider2, int DozeDivider3, BYTE *Crypto_Buff)
// FLASH
{
   int NS = 42;                        // Amount of Bytes  5 + 36 +1 = 42
   m_DataS [2] = 0x07;                 // Commanda

   m_DataS [5] =  DozeDivider1       & 0x7F;
   m_DataS [6] = (DozeDivider1 >> 7) & 0x7F;

   m_DataS [7] =  DozeDivider2       & 0x7F;
   m_DataS [8] = (DozeDivider2 >> 7) & 0x7F;

   m_DataS [9]  =  DozeDivider3       & 0x7F;
   m_DataS [10] = (DozeDivider3 >> 7) & 0x7F;

//   BYTE Txt[30] = {1,2,3,4,5,6,7,8,9};
   memcpy (&m_DataS[11], Crypto_Buff, 30);

   int Er = WriteCommand (m_DataS, NS);
   LastError = Er;
   return Er;
};
//----------------------------------------------------------------------------
int MyCommand::WriteClearSW      ()
{
   int NS = 5;                         // Amount of Bytes   5+0+0=5
   m_DataS [2] = 0x08;                 // Commanda

   int Er = WriteCommand (m_DataS, NS);
   LastError = Er;
   return Er;
};
//----------------------------------------------------------------------------
int MyCommand::WriteParamOscp      (int Scale, int Source, int Filter)
{
   int NS = 9;                         // Amount of Bytes
   m_DataS [2] = 0x09;                 // Commanda

   m_DataS [5] =  Scale  & 0x7F;
   m_DataS [6] =  Source & 0x7F;
   m_DataS [7] =  Filter & 0x7F;

   int Er = WriteCommand (m_DataS, NS);
   LastError =Er;
   return Er;
};
//----------------------------------------------------------------------------
int MyCommand::WriteStartOscp      ()
{
   int NS = 5;                         // Amount of Bytes
   m_DataS [2] = 0x0A;                 // Commanda

   int Er = WriteCommand (m_DataS, NS);
   LastError = Er;
   return Er;
};
//----------------------------------------------------------------------------
int MyCommand::WriteCoverTime      (int Time)      // ����� ����������� � ����������, ���� �������� ������� ��������, (��)  5+2=7
{
   int NS = 7;                         // Amount of Bytes
   m_DataS [2] = 0x0B;                 // Commanda

   m_DataS [5] =  Time  & 0x7F;

   int Er = WriteCommand (m_DataS, NS);
   LastError = Er;
   return Er;
};
//----------------------------------------------------------------------------
int MyCommand::WriteModuleNumber (int Year, int Number, BYTE *Crypto_Buff)
// FLASH
{
   int NS = 39;                        // Amount of Bytes  5 + 3+30 +1 = 39
   m_DataS [2] = 0x3F;                 // Commanda

   m_DataS [5] =  Year         & 0x7F;

   m_DataS [6] =  Number       & 0x7F;
   m_DataS [7] = (Number >> 7) & 0x7F;

   memcpy (&m_DataS[8], Crypto_Buff, 30);

   int Er = WriteCommand (m_DataS, NS);
   LastError = Er;
   return Er;
};
//----------------------------------------------------------------------------
int MyCommand::WriteReferenseRawFlash (WORD *Raw, BYTE *Crypto_Buff)
{
   int NS = 37;                        // Amount of Bytes  5+1+30+1=37
   m_DataS [2] = 0x05;                 // Commanda

   m_DataS [5] =  *Raw   & 0x7F;

   memcpy (&m_DataS[6], Crypto_Buff, 30);

   int Er = WriteCommand (m_DataS, NS);

   return Er;
};
//----------------------------------------------------------------------------
int MyCommand::WriteActiveRawFlash (WORD *Raw, BYTE *Crypto_Buff)
{
   int NS = 37;                        // Amount of Bytes  5+1+30+1=37
   m_DataS [2] = 0x04;                 // Commanda

   m_DataS [5] =  *Raw   & 0x7F;

   memcpy (&m_DataS[6], Crypto_Buff, 30);

   int Er = WriteCommand (m_DataS, NS);

   return Er;
};


//----------------------------------------------------------------------------
//    READ     READ     READ     READ     READ     READ     READ     READ     READ     READ     READ     READ     READ     
//----------------------------------------------------------------------------
int MyCommand::ReadVersion (BYTE *Name, BYTE *Hard, BYTE *Soft)
{
   int NS = 5;                         // Amount of Bytes  SEND
   m_DataS [2] = 0x40;                 // Commanda

   int NR = 14;                         // Amount of Bytes  RECEIVE 5+8+1=14

   int Er = ReadCommand (m_DataS, NS, m_DataR, NR);

   BYTE SB = 0x01;
   Name [2] = Name [3] = 0;
   Name [0] = m_DataR[5];
   if (m_DataR[8] & SB) Name[0] != 0x80;
   SB = SB << 1;

   Name [1] = m_DataR[6];
   if (m_DataR[8] & SB) Name[1] != 0x80;
   SB = SB << 1;

   Name[0] = m_DataR[5];
   Name[1] = m_DataR[6];
   Name[2] = m_DataR[7];
   Name[3] = m_DataR[8];

   Hard[0] = m_DataR[9];
   Hard[1] = m_DataR[10];
   Soft[0] = m_DataR[11];
   Soft[1] = m_DataR[12];
   LastError = Er;
   return Er;
};
//------------------------------------------------------------------------------
int MyCommand::ReadLedTimeAmpl (int *Time, int *Ampl)
{
   int NS = 5;                         // Amount of Bytes  SEND
   m_DataS [2] = 0x42;                 // Commanda

   int NR = 10;                         // Amount of Bytes  RECEIVE

   int Er = ReadCommand (m_DataS, NS, m_DataR, NR);

   *Time    = ((int)m_DataR[5]  & 0x7F) | (((int)m_DataR[6]  & 0x7F) << 7); 
   *Ampl    = ((int)m_DataR[7]  & 0x7F) | (((int)m_DataR[8]  & 0x1F) << 7); 
   LastError = Er;
   return Er;
};
//------------------------------------------------------------------------------
int MyCommand::ReadDozaLevel (int *DozaLevel1, int *DozaLevel2, int *DozaLevel3)
{
   int NS = 5;                         // Amount of Bytes  SEND
   m_DataS [2] = 0x43;                 // Commanda

   int NR = 12;                         // Amount of Bytes  RECEIVE  5+6+1-12

   int Er = ReadCommand (m_DataS, NS, m_DataR, NR);

   *DozaLevel1  = ((int)m_DataR[5]  & 0x7F) | (((int)m_DataR[6]  & 0x7F) << 7); 
   *DozaLevel2  = ((int)m_DataR[7]  & 0x7F) | (((int)m_DataR[8]  & 0x7F) << 7); 
   *DozaLevel3  = ((int)m_DataR[9]  & 0x7F) | (((int)m_DataR[10]  & 0x7F) << 7); 
   LastError = Er;
   return Er;
};
//------------------------------------------------------------------------------
int MyCommand::ReadTrigerLevel (int *TrigerLevel, BYTE *Crypto_Buff)
{
   int NS = 5;                         // Amount of Bytes  SEND
   m_DataS [2] = 0x44;                 // Commanda

   int NR = 38;                         // Amount of Bytes  RECEIVE     5+32+1=38

   int Er = ReadCommand (m_DataS, NS, m_DataR, NR);

   *TrigerLevel  = ((int)m_DataR[5]  & 0x7F) | (((int)m_DataR[6]  & 0x7F) << 7); 

//   char Txt[30];
   memcpy (Crypto_Buff, &m_DataR[7], 30);
   LastError = Er;
   return Er;
};
//----------------------------------------------------------------------------
int MyCommand::ReadControl (BYTE *Control)
{
   int NS = 5;                         // Amount of Bytes  SEND
   m_DataS [2] = 0x45;                 // Commanda

   int NR = 7;                         // Amount of Bytes  RECEIVE

   int Er = ReadCommand (m_DataS, NS, m_DataR, NR);

   *Control = m_DataR[5];
   LastError = Er;
   return Er;
};
//------------------------------------------------------------------------------
int MyCommand::ReadDozeDivider (int *DozeDivider1, int *DozeDivider2, int *DozeDivider3, BYTE *Crypto_Buff)
{
   int NS = 5;                         // Amount of Bytes  SEND
   m_DataS [2] = 0x47;                 // Commanda

   int NR = 42;                         // Amount of Bytes  RECEIVE  5+36+1=42

   int Er = ReadCommand (m_DataS, NS, m_DataR, NR);

   *DozeDivider1  = ((int)m_DataR[5]  & 0x7F) | (((int)m_DataR[6]   & 0x7F) << 7); 
   *DozeDivider2  = ((int)m_DataR[7]  & 0x7F) | (((int)m_DataR[8]   & 0x7F) << 7); 
   *DozeDivider3  = ((int)m_DataR[9]  & 0x7F) | (((int)m_DataR[10]  & 0x7F) << 7); 

//   char Txt[30];
   memcpy (Crypto_Buff, &m_DataR[11], 30);

   return Er;
};
//----------------------------------------------------------------------------
int MyCommand::ReadStatus (BYTE *Status1, BYTE *Status2)
{
   int NS = 5;                         // Amount of Bytes  SEND
   m_DataS [2] = 0x48;                 // Commanda

   int NR = 8;                         // Amount of Bytes  RECEIVE 5+2+1=8

   int Er = ReadCommand (m_DataS, NS, m_DataR, NR);

   *Status1 = m_DataR[5];
   *Status2 = m_DataR[6];
     LastError = Er;
   return Er;
};
//------------------------------------------------------------------------------
int MyCommand::ReadCoverTime (int *CoverTime)
{
   int NS = 5;                         // Amount of Bytes  SEND
   m_DataS [2] = 0x4B;                 // Commanda

   int NR = 7;                         // Amount of Bytes  RECEIVE

   int Er = ReadCommand (m_DataS, NS, m_DataR, NR);

   *CoverTime  = ((int)m_DataR[5]  & 0x7F); 
     LastError = Er;
   return Er;
};
//----------------------------------------------------------------------------
int MyCommand::ReadExpositionData (int *Time, int *TrgMean, int *TrgDoza, int *IntMean1, int *IntDoza1, int *IntMean2, int *IntDoza2, BYTE *CounterExpo)
{
   int NS = 5;                         // Amount of Bytes  SEND
   m_DataS [2] = 0x50;                 // Commanda

   int NR = 21;                         // Amount of Bytes  RECEIVE  5+15+1=21

   int Er = ReadCommand (m_DataS, NS, m_DataR, NR);

   *Time       = ((int)m_DataR[5]  & 0x7F) | (((int)m_DataR[6]  & 0x7F) << 7); 
   *TrgMean    = ((int)m_DataR[7]  & 0x7F) | (((int)m_DataR[8]  & 0x7F) << 7); 
   *TrgDoza    = ((int)m_DataR[9]  & 0x7F)  | (((int)m_DataR[10] & 0x7F) << 7); 
   *IntMean1    = ((int)m_DataR[11] & 0x7F) | (((int)m_DataR[12] & 0x7F) << 7); 
   *IntDoza1    = ((int)m_DataR[13] & 0x7F) | (((int)m_DataR[14] & 0x7F) << 7); 
   *IntMean2    = ((int)m_DataR[15] & 0x7F) | (((int)m_DataR[16] & 0x7F) << 7); 
   *IntDoza2    = ((int)m_DataR[17] & 0x7F) | (((int)m_DataR[18] & 0x7F) << 7); 
   *CounterExpo= ((int)m_DataR[19]  & 0x7F); 
     LastError = Er;
   return Er;
};
//----------------------------------------------------------------------------
int MyCommand:: ReadStatisticData (BYTE Source, int *Amax, int *Amin, int *Amean, int *Arms)
{
   int NS = 7;                         // Amount of Bytes  SEND
   m_DataS [2] = 0x51;                 // Commanda

   m_DataS [5] = Source;                 // 

   int NR = 14;                         // Amount of Bytes  RECEIVE

   int Er = ReadCommand (m_DataS, NS, m_DataR, NR);

   *Amax    = ((int)m_DataR[5]  & 0x7F) | (((int)m_DataR[6]  & 0x7F) << 7); 
   *Amin    = ((int)m_DataR[7]  & 0x7F) | (((int)m_DataR[8]  & 0x7F) << 7); 
   *Amean   = ((int)m_DataR[9]  & 0x7F) | (((int)m_DataR[10] & 0x7F) << 7); 
   *Arms    = ((int)m_DataR[11] & 0x7F) | (((int)m_DataR[12] & 0x7F) << 7); 
     LastError = Er;
   return Er;
};
//----------------------------------------------------------------------------
int MyCommand:: ReadAmplitudes (int *Atrg, int *AtrgF, int *Aint1, int *Aint2)
{
   int NS = 5;                         // Amount of Bytes  SEND
   m_DataS [2] = 0x52;                 // Commanda

   int NR = 14;                         // Amount of Bytes  RECEIVE

   int Er = ReadCommand (m_DataS, NS, m_DataR, NR);

   *Atrg    = ((int)m_DataR[5]  & 0x7F) | (((int)m_DataR[6]  & 0x7F) << 7); 
   *AtrgF   = ((int)m_DataR[7]  & 0x7F) | (((int)m_DataR[8]  & 0x7F) << 7); 
   *Aint1   = ((int)m_DataR[9]  & 0x7F) | (((int)m_DataR[10] & 0x7F) << 7); 
   *Aint2   = ((int)m_DataR[11] & 0x7F) | (((int)m_DataR[12] & 0x7F) << 7); 
     LastError = Er;
   return Er;
};
//----------------------------------------------------------------------------
int MyCommand:: ReadAmpOscp (int Source, int J, int *Ampl)
{
   int NS = 8;                         // Amount of Bytes  SEND
   m_DataS [2] = 0x53;                 // Commanda

   m_DataS [5] = Source;            // 
   m_DataS [6] = J;                 // 

   int NR = 134;                       // Amount of Bytes  RECEIVE

   int Er = ReadCommand (m_DataS, NS, m_DataR, NR);

   int N = 64;
   for (int i = 0; i < N; i++){
      int j = 5 + i*2;
      TwoBytesWord (m_DataR[j], m_DataR[j+1], &Ampl[i]);
   }
     LastError = Er;
   return Er;
};
//----------------------------------------------------------------------------
int MyCommand:: ReadStatusOscp (int *StatusOscp)
{
   int NS = 5;                         // Amount of Bytes  SEND
   m_DataS [2] = 0x54;                 // Commanda

   int NR = 7;                       // Amount of Bytes  RECEIVE

   int Er = ReadCommand (m_DataS, NS, m_DataR, NR);

   *StatusOscp  = m_DataR[5];
     LastError = Er;
   return Er;
};
//------------------------------------------------------------------------------
int MyCommand::ReadModuleNumber (int *Year, int *Number, BYTE *Crypto_Buff)
{
   int NS = 5;                         // Amount of Bytes  SEND
   m_DataS [2] = 0x7F;                 // Commanda

   int NR = 39;                         // Amount of Bytes  RECEIVE  5+3+30+1=39

   int Er = ReadCommand (m_DataS, NS, m_DataR, NR);

   *Year    = ((int)m_DataR[5]  & 0x7F); 
   *Number  = ((int)m_DataR[6]  & 0x7F) | (((int)m_DataR[7]   & 0x7F) << 7); 

   memcpy (Crypto_Buff, &m_DataR[8], 30);
     LastError = Er;
   return Er;
};
//----------------------------------------------------------------------------

int MyCommand::ReadActiveRawFlash (WORD *Raw, BYTE *Crypto_Buff)
{
   int NS = 5;                         // Amount of Bytes  SEND
   m_DataS [2] = 0x44;                 // Commanda

   int NR = 37;                         // Amount of Bytes  RECEIVE     5+1+30+1=37

   int Er = ReadCommand (m_DataS, NS, m_DataR, NR);

   *Raw  = ((int)m_DataR[5]  & 0x7F); 

   memcpy (Crypto_Buff, &m_DataR[6], 30);

   return Er;
};
//----------------------------------------------------------------------------

int MyCommand::ReadReferenseRawFlash (WORD *Raw, BYTE *Crypto_Buff)
{
   int NS = 5;                         // Amount of Bytes  SEND
   m_DataS [2] = 0x45;                 // Commanda

   int NR = 37;                         // Amount of Bytes  RECEIVE     5+1+30+1=37

   int Er = ReadCommand (m_DataS, NS, m_DataR, NR);

   *Raw  = ((int)m_DataR[5]  & 0x7F); 

   memcpy (Crypto_Buff, &m_DataR[6], 30);

   return Er;
};
//---------------------------------------------------------------------------


bool MyCommand::SetSpeed(UINT Baudrate)
 {
	 //m_Comm.Close();

   //UINT nSpeed    = Baudrate; //115200; 
   //UINT nDataBits = 8; 
   //UINT nParity   = 1; 
   //UINT nStopBits = 2;
   //int TimeOut    = 0;
   //CurrBaudrate = Baudrate;
   //bool Ret = m_Comm.Configure (nSpeed, 
   //                              nDataBits, 
   //                              nParity, 
   //                              nStopBits);


	 bool Ret = m_Comm.SetCOMSpeed(Baudrate);
   if (Ret)
	   return true;
   else
	   return false;

  // m_Comm.SetTimeout (120);



 };
//---------------------------------------------------------------------------
//void ReadVersion(uint8_t *Buff, int N);//������ ��� � ������ ������
/********************************************************************************/
void MyCommand:: ReadSiMP_Thershold(uint16_t *Thershold)
{
	int NS = 5;                         // Amount of Bytes  SEND
	m_DataS [2] = EComamnd::READ_SiMP_THERSH;                 // Commanda

   int NR = 8;                         // Amount of Bytes  RECEIVE     5+1+30+1=37

   int Er = ReadCommand (m_DataS, NS, m_DataR, NR);

   *Thershold =((uint16_t)m_DataR[5]  & 0x7F) | (((uint16_t)m_DataR[6]   & 0x7F) << 7);
   ///*Raw  = ((int)m_DataR[5]  & 0x7F); 

   //memcpy (Crypto_Buff, &m_DataR[6], 30);

   //memcpy(Thershold, &m_DataR[5],sizeof(uint16_t));

};
/********************************************************************************/
void MyCommand:: ReadControlDoz(uint8_t *Control)
{
		
		
		
	int NS = 5;                         // Amount of Bytes  SEND
	m_DataS [2] =    EComamnd::READ_CONTROLR;              // Commanda

   int NR = 7;                         // Amount of Bytes  RECEIVE     5+1+30+1=37

   int Er = ReadCommand (m_DataS, NS, m_DataR, NR);

   ///*Raw  = ((int)m_DataR[5]  & 0x7F); 

   //memcpy (Crypto_Buff, &m_DataR[6], 30);
      *Control = m_DataR[5];
  // memcpy(Control, &m_DataR[5],sizeof(uint16_t));
		
		
};
/********************************************************************************/
void  MyCommand:: ReadStatIsDone(uint8_t *StateStatic)
{
		
		
		
		int NS = 5;                         // Amount of Bytes  SEND
		m_DataS [2] =    EComamnd::READ_COMPLETE_CONV;              // Commanda
		
   int NR = 7;                         // Amount of Bytes  RECEIVE     5+1+30+1=37

   int Er = ReadCommand (m_DataS, NS, m_DataR, NR);

   ///*Raw  = ((int)m_DataR[5]  & 0x7F); 

   //memcpy (Crypto_Buff, &m_DataR[6], 30);

  // memcpy(StateStatic, &m_DataR[5],sizeof(uint8_t));
		
		
		   *StateStatic = m_DataR[5];
		
		
		
		
		
};
/********************************************************************************/
void MyCommand::ReadState(uint8_t *StateWord)
{
		
		
		int NS = 5;                         // Amount of Bytes  SEND
		m_DataS [2] =    EComamnd::READ_STATE_WORD;              // Commanda
		
   int NR = 7;                         // Amount of Bytes  RECEIVE     5+1+30+1=37

   int Er = ReadCommand (m_DataS, NS, m_DataR, NR);

   ///*Raw  = ((int)m_DataR[5]  & 0x7F); 

   //memcpy (Crypto_Buff, &m_DataR[6], 30);

 //  memcpy(StateWord, m_DataR,sizeof(uint8_t));

	  *StateWord = m_DataR[5];		
		
		
		
};
/********************************************************************************/
void MyCommand:: ReadOscParams(uint8_t *ScaleInPix)
{


	int NS = 5;                         // Amount of Bytes  SEND
	m_DataS [2] =    EComamnd::READ_OSC_PARAMS;              // Commanda
		
   int NR = 9;                         // Amount of Bytes  RECEIVE     5+1+30+1=37

   int Er = ReadCommand (m_DataS, NS, m_DataR, NR);

   ///*Raw  = ((int)m_DataR[5]  & 0x7F); 

   //memcpy (Crypto_Buff, &m_DataR[6], 30);

 //  memcpy(ScaleInPix, m_DataR,sizeof(uint8_t));

     *ScaleInPix = m_DataR[5];


};
/********************************************************************************/
void MyCommand::ReadStateOsc(uint8_t *OscStateWord)
{	
	
	int NS = 5;                         // Amount of Bytes  SEND
	m_DataS [2] =    EComamnd::READ_OSC_PARAMS;              // Commanda
		
   int NR = 7;                         // Amount of Bytes  RECEIVE     5+1+30+1=37

   int Er = ReadCommand (m_DataS, NS, m_DataR, NR);

   ///*Raw  = ((int)m_DataR[5]  & 0x7F); 

   //memcpy (Crypto_Buff, &m_DataR[6], 30);

//   memcpy(OscStateWord, m_DataR,sizeof(uint8_t));
   *OscStateWord = m_DataR[5];


};	//������ ��������� ����� ������������ 
/********************************************************************************/
void MyCommand:: ReadSiMP_Voltage(uint8_t *SiMP_Volt)
{

	int NS = 5;                         // Amount of Bytes  SEND
	m_DataS [2] = EComamnd::READ_SiMP_VOLTAGE;              // Commanda
		
   int NR = 7;                         // Amount of Bytes  RECEIVE     5+1+30+1=37

   int Er = ReadCommand (m_DataS, NS, m_DataR, NR);

   ///*Raw  = ((int)m_DataR[5]  & 0x7F); 

   //memcpy (Crypto_Buff, &m_DataR[6], 30);

  // memcpy(SiMP_Volt, m_DataR,sizeof(uint16_t));
   *SiMP_Volt =   m_DataR[5];


};	//������ ���������� �� SiMP 
/********************************************************************************/
void MyCommand:: ReadExpositionParams(uint16_t *Time,uint16_t *Avergage,uint8_t* Count)
{

   int NS = 5;                         // Amount of Bytes  SEND
   m_DataS [2] =    EComamnd::READ_PARAMS_MESURED_EXP;              // Commanda
		
   int NR = 35;                         // Amount of Bytes  RECEIVE     5+1+30+1=37

   int Er = ReadCommand (m_DataS, NS, m_DataR, NR);

   ///*Raw  = ((int)m_DataR[5]  & 0x7F); 

   //memcpy (Crypto_Buff, &m_DataR[6], 30);
//
//   
//memcpy(&Time,&m_DataR[5],sizeof(uint16_t));
//memcpy(&Avergage,&m_DataR[7],sizeof(uint16_t) * 13);
//memcpy(&Count,&m_DataR[34],sizeof(uint8_t));
//


  // memcpy(SiMP_Volt, m_DataR,sizeof(uint16_t));

   int j = 0;
     *Time = ((int)m_DataR[5]  & 0x7F) | (((int)m_DataR[6]   & 0x7F) << 7);
   for(uint8_t i = 0;i<13;i++)
   {
j = 7 + 2*i;

Avergage[i] = ((int)m_DataR[j]  & 0x7F) | (((int)m_DataR[j+1]   & 0x7F) << 7);

   };

   *Count = m_DataR[34];



};	//��������� ���������� ����������
/********************************************************************************/
void MyCommand::ReadStaticPerChannel(uint8_t *Chanel, uint16_t Max,uint16_t* Min,uint16_t* Avg,uint16_t* RMS)
{

	//int8_t Channel = Buff[5];
	 int NS = 5;
	 int NR = 14; 
	 m_DataS [2] =    EComamnd::READ_PARAMS_MESURED_EXP; 
	 m_DataS [5] = *Chanel;
 int Er = ReadCommand (m_DataS, NS, m_DataR, NR);
//memcpy(&Max,&m_DataR[5],sizeof(uint16_t));
//memcpy(&Min,&m_DataR[7],sizeof(uint16_t));
//memcpy(&Avg,&m_DataR[9],sizeof(uint16_t));
//memcpy(&RMS ,  &m_DataR[11],sizeof(uint16_t));


Max =  ((int)m_DataR[5]  & 0x7F) | (((int)m_DataR[6]   & 0x7F) << 7);

*Min = ((int)m_DataR[7]  & 0x7F) | (((int)m_DataR[8]   & 0x7F) << 7);

*Avg = ((int)m_DataR[9]  & 0x7F) | (((int)m_DataR[10]   & 0x7F) << 7);

*RMS = ((int)m_DataR[10]  & 0x7F) | (((int)m_DataR[11]   & 0x7F) << 7);




 // N = Buff[3] = 14 ;
   



};	//��������� ���������� ���, ���, �������, ��� ��� 13������� ����������
/********************************************************************************/
void MyCommand:: ReadADCValues(uint16_t *Values)
{

	 int NS = 5;
	 int NR = 32; 
	 m_DataS [2] =    EComamnd::READ_CURRENT_ADC_VALUES; 
	// m_DataS [5] = *Chanel;
	  int Er = ReadCommand (m_DataS, NS, m_DataR, NR);


	    for(int i = 0;i<13;i++)
{
 int j = 5 + i*2;

     Values[i]  = ((int)m_DataR[j]  & 0x7F) | (((int)m_DataR[j+1]   & 0x7F) << 7);

	  };

//memcpy(&Values,&m_DataR[5],sizeof(uint16_t)* 13);




};	//������ ���(�������) �������� �������  
/********************************************************************************/
void  MyCommand:: ReadAmpsOsc(uint16_t *Amplitudes,uint8_t Chanel,uint8_t Offset)
{
	 int NS = 8;
	 int NR = 134; 
	 m_DataS [2] =    EComamnd::READ_AMPLITUDES_OSC; 
	 m_DataS [5] = Chanel;
	 m_DataS [6] = Offset;
	 
	  int Er = ReadCommand (m_DataS, NS, m_DataR, NR);


	    for(int i = 0;i<64;i++)
{
 int j = 5 + i*2;

 Amplitudes[i]  = ((int)m_DataR[j]  & 0x7F) | (((int)m_DataR[j+1]   & 0x7F) << 7);

	  };


	  //memcpy(&Amplitudes,&m_DataR[5],sizeof(uint16_t)* 64);



};	//������ ��������� ������������ 
/********************************************************************************/
void  MyCommand:: ReadModuleNumber(uint32_t *Year,uint32_t *Number, uint8_t *Crypto_Buffer)
{




};

/********************************************************************************/
void  MyCommand:: WriteSiMP_Thershold(uint16_t *Thershold)
{
	int NS = 8;
	m_DataS[2] = EComamnd::WRITE_SiPM_THERSH;
	//memcpy (&m_DataS[5], &Thershold,sizeof(uint16_t));
	m_DataS[5] = *Thershold & 0x7F;
  m_DataS[6] = (*Thershold   >> 7) & 0x7F;
   int Er = WriteCommand (m_DataS, NS);




};	//�������� ����� ������������ �������� SiMP
/********************************************************************************/
void  MyCommand:: WriteControlReg(uint8_t *Contro)
{//�������� ����������� ������� ���������
	int NS = 7;
	m_DataS[2] = EComamnd::WRITE_SiPM_THERSH;
	//memcpy (&m_DataS[5], &Contro,sizeof(uint8_t));
	m_DataS[5] = *Contro;
   int Er = WriteCommand (m_DataS, NS);

};
/********************************************************************************/
void  MyCommand:: StartStatist(void)
{
		int NS = 5;
		m_DataS[2] = EComamnd::START_STATISTIC;
	//memcpy (&m_DataS[5], &Contro,sizeof(uint8_t));

   int Er = WriteCommand (m_DataS, NS);



};	//������ ��������� ����������
/********************************************************************************/
void  MyCommand:: ClearStateWord(void)
{
int NS = 5;
m_DataS[2] = EComamnd::CLEAR_STATE_WORD;
	//memcpy (&m_DataS[5], &Contro,sizeof(uint8_t));

   int Er = WriteCommand (m_DataS, NS);



};	//�������� ��������� �����
/********************************************************************************/
void  MyCommand:: WriteOscParams(uint8_t* ScaleInPixel)
{	//�������� ��������� �����������
	int NS = 7;
	m_DataS[2] = EComamnd::SET_PARAM_OSCIL;
	//memcpy (&m_DataS[5], &ScaleInPixel,sizeof(uint8_t));
	m_DataS[5] = *ScaleInPixel;
   int Er = WriteCommand (m_DataS, NS);


};
/********************************************************************************/
void  MyCommand:: WaitMeasureOsc(void)
{	//������ �������� 
	int NS = 5;
	m_DataS[2] = EComamnd::SET_WAIT_OSCIL;
	//memcpy (&m_DataS[5], &Contro,sizeof(uint8_t));

   int Er = WriteCommand (m_DataS, NS);

};
/********************************************************************************/
void  MyCommand:: WriteSiMP_Voltage(uint8_t* Voltage)
{

	int NS = 7;
	m_DataS[2] = EComamnd::WRITE_SiMP_VOLTAGE;
	//memcpy (&m_DataS[5], &Voltage,sizeof(uint8_t));
m_DataS[5] = 	*Voltage ;
   int Er = WriteCommand (m_DataS, NS);


};
/********************************************************************************/
void MyCommand:: WriteModuleNumber(uint32_t *Year,uint32_t *Number, uint8_t *Crypto_Buffer)
{



};