#include "LegacyBluetoothPacket.h"




void CreateDataStamp(Datagram&  DG)
{
 time_t rawtime;
 tm * timeinfo = NULL;

  time (&rawtime);
  localtime_s (timeinfo,&rawtime);

  DG.Header.Stamp[0] = timeinfo->tm_hour;
  DG.Header.Stamp[1] = timeinfo->tm_min;
  DG.Header.Stamp[2] = timeinfo->tm_sec;
  DG.Header.Stamp[3] = timeinfo->tm_wday;
  DG.Header.Stamp[4] = timeinfo->tm_mon;
  DG.Header.Stamp[5] = timeinfo->tm_year;
};
/************************************************************************************************/
Error FormatHeader( 
	const  DataType DT,
	Datagram& DataG
	)
{

	if(DT == Test)
	{
		unsigned char is = 125;

		size_t Ds = sizeof(Datagram);
		size_t Hs = sizeof(Head);
		size_t DSs = sizeof(DataG.Data.DataBuffer);
		DataG.Header.DataType = DT;


		DataG.Header.Sender = 1;
	   DataG.Header.Receiver = 4;
		DataG.DSize = sizeof(Datagram);
		DataG.Header.HSize = sizeof(Head);
		DataG.Data.DataSize = sizeof(Datagram);
		CreateDataStamp(DataG);
		unsigned char CRCS = CRCXE((unsigned char*)&DataG.Header,sizeof(DataG.Header));
		DataG.Header.CRC = CRCS; 
		return Success;
	
	}
	else
	{




		return InvalidHeader;
	}

};
/************************************************************************************************/
Error FormatHeader( 
	const  DataType DT,
	Datagram& DataG,
	const size_t Sender,
    const size_t Receiver
	)
{
	
	
		unsigned char is = 125;

		size_t Ds = sizeof(Datagram);
		size_t Hs = sizeof(Head);
		size_t DSs = sizeof(DataG.Data.DataBuffer);
		DataG.Header.DataType = DT;

		DataG.Header.Sender = Sender;
	   DataG.Header.Receiver = Receiver;
		DataG.DSize = sizeof(Datagram);
		DataG.Header.HSize = sizeof(Head);
		DataG.Data.DataSize = sizeof(Datagram);
		CreateDataStamp(DataG);
		unsigned char CRCS = CRCXE((unsigned char*)&DataG.Header,sizeof(DataG.Header));
		DataG.Header.CRC = CRCS; 
	
	

		return Success;

		
	};



/************************************************************************************************/
Error FormatData( 
	Datagram& DataG,
	BYTE* Data,
	size_t NumberBytes
	)
{

	if(Data == NULL || NumberBytes == 0)
	{
		return EmptyData;

	}
	else
	{

	if(NumberBytes <= 512)
	{
		memcpy(&DataG.Data.DataBuffer,Data,NumberBytes);
	   
		DataG.Data.CRC = CRCXE(Data,NumberBytes);

		//for(size_t i = 0;i<NumberBytes;i++)
		//{
		//	DataG.Data.DataBuffer[i] = Data[i];

		//}

		return Success;
	//sockaddr HostAdress;

	//getsockname(0,HostAdress,sizeof(sockaddr));

		//printf("TIME STAMP %i h : %i m : %i d : %i wd  :\n",DataG.Head.Stamp[0],DataG.Head.Stamp[1], DataG.Head.Stamp[2],  DataG.Head.Stamp[3]  );
	}
	else
	{

		return BigDataIn;
	}


	}










};
/************************************************************************************************/




/************************************************************************************************/





/************************************************************************************************/

/************************************************************************************************/

Error FormatDatagramPacket( 
   DataType DT,
   Datagram& DataG,
   BYTE* Data,
   size_t  NumberofBytes
	)
{

	Error Ern;

	memset(&DataG,0,sizeof(Datagram));

	parallel_invoke(	

		[&]{Ern = FormatHeader(DT,DataG);},

	[&]{Ern = FormatData(DataG,Data,NumberofBytes);}
	);
	


	

	return Ern;


};

/************************************************************************************************/
   Error FormatDatagramPacket( 
   DataType DT,
   Datagram& DataG,
   size_t Sender,
   size_t Receiver,
   BYTE* Data,
   size_t  NumberofBytes
	)
{

	Error Ern;

	memset(&DataG,0,sizeof(Datagram));
	parallel_invoke(
	[&]{Ern = FormatHeader(DT,DataG,Sender,Receiver);},

	[&]{Ern = FormatData(DataG,Data,NumberofBytes);}
	);
	


	

	return Ern;


};


/************************************************************************************************/
unsigned char CRCXE(unsigned char* Data,size_t Size)
{
	//crcInit();
	/*unsigned char CRC = 0xBE;
	for(size_t i = 0;i>Size ;i++)
	{
	CRC&=Data[i];
	};

return	CRC^=0x3ff;*/
	//return crcFast(Data,Size);
	  int Summ = 0;

	  /////parallel_for(
	  int size = Size;
	  
	  parallel_for(0,size,1,
	  [&Summ,&Data](int i){
	  //////for (int i = 0; i < Size; i++){
      Summ += Data[i];
   });
   int SummH = (Summ >> 7) & 0x7F;

   BYTE CRC = (BYTE) (SummH + (Summ & 0x7F));

   CRC &= 0x7F;


   return CRC;



};



 unsigned char CRC8( unsigned char* Data,size_t Size)
{





	unsigned char  Result = 0;


	for(size_t i = Size;i>0;i--)
	{

		Result|=  (unsigned char)Data[i]<<i;

	}

	return Result;




};











 /************************************************************************************************/

void CommandParcer(Datagram& Datain,FunctionArray&   Functions)
{    
	
	int funtionNumber = Datain.Data.DataBuffer[0];
	size_t FunctionParametrs = Datain.Data.DataBuffer[1];
	BYTE* FunctParam = Datain.Data.DataBuffer;
	Functions[funtionNumber](FunctParam,FunctionParametrs);
	//FormatDatagramPacket(DataType::Test,Datain,0,1,FunctParam,512);
	
};


/************************************************************************************************/
 int  Byper(BYTE* InParams,size_t Count)
 {
	 printf("FUNC NAME\n");
	 Beep(1000,10);

	 return 0;
 }

/************************************************************************************************/
 int  PrintMessage(BYTE* InParams,size_t Count)
 {
	 printf("FUNC NAME\n");
	 //Beep(1000,10);

	 return 0;
 }

/************************************************************************************************/
  int  PrintMessageAdd(BYTE* InParams,size_t Count)
 {
	 int A = 0;
	 int B = 0;

	 int S = 0;
	 memcpy(&A,&InParams[2],sizeof(INT));
	 memcpy(&B,&InParams[6],sizeof(INT));

	 S = A + B;
	 memcpy(&InParams[10],&S,sizeof(int));
	 printf("Result %i + %i = % i  \n",A,B,S);
	 //Beep(1000,10);

	 return 0;
 }
/************************************************************************************************/
  int  BotIndent(BYTE* InParams,size_t Count)
  {
	 /*har* BotHostName = NULL;
	 char* ServerIP;
	 int Length = 60;
	 BYTE BotId = 1;
	


	 gethostname(BotHostName,Length);
	 size_t DataS = Length * sizeof(char);
	 InParams[2] = BotId;
	 InParams[3] = DataS;

	 memcpy(&InParams[4],&BotHostName,DataS);*/




	 char BotHostName[128];

	 int Length = 60;
	 BYTE BotId = 1;
	
	 gethostname(BotHostName,Length);
	 size_t DataS = strlen(BotHostName);
	 InParams[2] = BotId;
	 InParams[3] = DataS;
	 memcpy((char*)&InParams[4],&BotHostName,DataS);
	// SendDatagram(ClientConn,Input);
	 printf("Mashine Name = %s\n",BotHostName);



	






	  return 0;
  }

/************************************************************************************************/
  int  SendString(BYTE* InParams,size_t Count)
  {




	  return 0;
  }


/************************************************************************************************/
   int  BotSearch(BYTE* InParams,size_t Count)
   {
	   SOCKET Device;

	 

	   memcpy(&Device,&InParams[2],sizeof(SOCKET));


	   return 0;
   }






/*   ***********************************************************************************************/
EthError SendDatagramBT(BluetoothRFCOMM& Connector,Datagram& Data)
{
	char DataB[sizeof(Datagram)];
	memset(DataB,0,sizeof(Datagram));
	memcpy(DataB,&Data,sizeof(Datagram));
	size_t Ssize = sizeof(Datagram);
	int Ds = 0;


	
	 

   EthError Err   =( EthError)Connector.Write(DataB,Ssize); //recv(Connector,DataB,Ssize,0);
   memcpy(&Data,DataB,Ssize);
	 if( Err == SendSuccesful)
	 {
	return Succesful;
	 }
	 else
	 {
		 return SendDataForbiden;
	 }
}

/************************************************************************************************/
EthError ReceiveDatagramBT(BluetoothRFCOMM& Connector,Datagram& Data)
{
	size_t Ssize = sizeof(Datagram);
	char DataB[sizeof(Datagram)];
	memset(DataB,0,sizeof(Datagram));
	memset(&Data,0,sizeof(Datagram));

 EthError Err   =( EthError)Connector.Read(DataB,Ssize); //recv(Connector,DataB,Ssize,0);
memcpy(&Data,DataB,Ssize);
if( Err == SendSuccesful ) 
{
	return  Succesful;
}
else
{
	return ReceiveDataForbiden;

}
}





/************************************************************************************************/
void  IntToByte(INT& Source,BYTE* Dest)
{
	//BYTE* Dest = new BYTE[sizeof(INT)];
	memcpy(Dest,&Source,sizeof(INT));

	//delete[] Dest;



}





void  ByteToInt(BYTE* Source, INT* Dest)
{



	memcpy(Dest,Source,sizeof(INT));

}