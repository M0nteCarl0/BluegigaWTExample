
#include "EthernetFunctons.h"
#include "BluetoothRFCOMM.h"
static SOCKET TimerSoc;

/************************************************************************************************/
void  IntToByte(INT& Source,BYTE* Dest)
{
	//BYTE* Dest = new BYTE[sizeof(INT)];
	memcpy(Dest,&Source,sizeof(INT));

	//delete[] Dest;



}
/************************************************************************************************/
void  ByteToInt(BYTE* Source, INT* Dest)
{



	memcpy(Dest,Source,sizeof(INT));

}
/************************************************************************************************/
void CopyByteArrayFromTo(BYTE* Source,BYTE* Destinat,size_t Start,size_t End)
{

	memcpy(&Destinat,&Source[Start],End * sizeof(BYTE));
	//delete[] Dest;



}
/************************************************************************************************/
void  ByteToFloat(BYTE* Source, float* Dest)
{

	memcpy(Dest,Source,sizeof(float));

}
/************************************************************************************************/
void  FloatToByte(FLOAT& Source,BYTE* Dest)
{
	//BYTE* Dest = new BYTE[sizeof(INT)];
	memcpy(Dest,&Source,sizeof(FLOAT));

	//delete[] Dest;



}
/************************************************************************************************/
EthError EthernetInit(void)
{
	WSADATA WDat;

	WORD Wd = MAKEWORD(2,0);
	if(WSAStartup(Wd,&WDat)  == NO_ERROR)
	{
		printf("Wsa is %s\n",WDat.szSystemStatus);
		return EthError::Succesful;
	}
	else
	{
		return EthError::NotInitialise;


	}


}
/************************************************************************************************/
EthError EthernetTCPServerConfig(SOCKET& Connector,INT Port, sockaddr_in& ServerAdress)
{
	   //sockaddr_in SAdrres;

	     EthernetInit();
		 Connector = socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);
		 
		 ServerAdress.sin_family =  AF_INET;
		 ServerAdress.sin_addr.s_addr = htonl(INADDR_ANY); 
		ServerAdress.sin_port = htons(Port);

		return EthError::Succesful;

}
/************************************************************************************************/
EthError EthernetTCPClientConfig(SOCKET& Connector,INT Port,CONST char*  Adress)
{
	   sockaddr_in SAdrres;
	   memset(&SAdrres,0,sizeof(SAdrres));
	     EthernetInit();
		 Connector = socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);
		 
		 SAdrres.sin_family =  AF_INET;
		 SAdrres.sin_addr.s_addr = inet_addr(Adress);
		 SAdrres.sin_port = htons(Port);
		
		  
		 return EthernetTCPClientConnect(Connector,SAdrres);
	

}
/************************************************************************************************/
EthError SendDatagram(SOCKET& Connector,Datagram& Data)
{
	char DataB[sizeof(Datagram)];
	memset(DataB,0,sizeof(Datagram));
	memcpy(DataB,&Data,sizeof(Datagram));
	size_t Ssize = sizeof(Datagram);
	int Ds = 0;

	int idx;
	
	  Ds = send(Connector,DataB,Ssize,0);
	 

	 if( Ds == Ssize)
	 {
	return EthError::Succesful;
	 }
	 else
	 {
		 return EthError::SendDataForbiden;
	 }
}

/************************************************************************************************/
EthError SendDatagramBT(BluetoothRFCOMM& Connector,Datagram& Data)
{
	char DataB[sizeof(Datagram)];
	memset(DataB,0,sizeof(Datagram));
	memcpy(DataB,&Data,sizeof(Datagram));
	size_t Ssize = sizeof(Datagram);
	int Ds = 0;

	int idx;
	
	  ///Ds = send(Connector,DataB,Ssize,0);
	 

   EthError Err   =( EthError)Connector.Write((uint8_t&)DataB,Ssize); //recv(Connector,DataB,Ssize,0);
   memcpy(&Data,DataB,Ssize);
	 if( Err == EthError::SendSuccesful)
	 {
	return EthError::Succesful;
	 }
	 else
	 {
		 return EthError::SendDataForbiden;
	 }
}

/************************************************************************************************/
EthError ReceiveDatagram(SOCKET& Connector,Datagram& Data)
{
	size_t Ssize = sizeof(Datagram);
	char DataB[sizeof(Datagram)];
	memset(DataB,0,sizeof(Datagram));
	//memset(&Data,0,sizeof(Datagram));

int Rcv = 	recv(Connector,DataB,Ssize,0);
memcpy(&Data,DataB,Ssize);
if(Rcv == Ssize)
{
	return  EthError::Succesful;
}
else
{
	return EthError::ReceiveDataForbiden;

}
}


EthError ReceiveDatagramBT(BluetoothRFCOMM& Connector,Datagram& Data)
{
	size_t Ssize = sizeof(Datagram);
	char DataB[sizeof(Datagram)];
	memset(DataB,0,sizeof(Datagram));
	memset(&Data,0,sizeof(Datagram));

 EthError Err   =( EthError)Connector.Read((uint8_t&)DataB,Ssize); //recv(Connector,DataB,Ssize,0);
memcpy(&Data,DataB,Ssize);
if( Err == EthError::SendSuccesful ) 
{
	return  EthError::Succesful;
}
else
{
	return EthError::ReceiveDataForbiden;

}
}





/************************************************************************************************/
EthError EthernetTCPServerListen(SOCKET& Connector,INT Backlog)
{
	int LER = 	listen(Connector,Backlog);
	if(LER == 0)
	{
	return EthError::Succesful;
	}
	else
	{

		return EthError::BadResult;

	}
}
/************************************************************************************************/
EthError EthernetTCPServerBind(SOCKET& Connector,sockaddr_in&  ServerAdress)
{
	size_t SSize = sizeof(sockaddr_in);
	int LER = 	bind(Connector,(sockaddr*)&ServerAdress,SSize);
	if(LER == 0)
	{
	return EthError::Succesful;
	}
	else
	{

		return EthError::BadResult;

	}
}
/************************************************************************************************/
EthError EthernetTCPServerAccept(SOCKET& Connector, SOCKET& ClientSocket,sockaddr_in&  ServerAdress)
{
	int ErrorA = 0;
	int SSIZE = sizeof(ServerAdress);
	ClientSocket = accept(Connector,(sockaddr*)&ServerAdress,&SSIZE);
	if(ClientSocket !=0 )
	{

		return EthError::Succesful;
	}
	else
	{

		return EthError::BadResult;


	}

}

/************************************************************************************************/
EthError EthernetTCPClientConnect(SOCKET& Connector,sockaddr_in&  ServerAdress)
{
	int Err = connect(Connector,(sockaddr*)&ServerAdress,sizeof(ServerAdress));
	if(Err == 0)
	{
		return EthError::Succesful;
	}
	else
		printf("Error %i\n!",WSAGetLastError());
		return EthError::BadResult;

	//return EthError::Succesful;
}
/************************************************************************************************/
EthError EthernetCloseSocket(SOCKET& Connector)
{
	int EER = closesocket(Connector);
	if(EER == 0)
	{
	return EthError::Succesful;
	}
	else
	{


		return EthError::BadResult;
	}
}
/************************************************************************************************/
/************************************************************************************************/
/************************************************************************************************/
/************************************************************************************************/

/************************************************************************************************/


/************************************************************************************************/


/************************************************************************************************/
/************************************************************************************************/
/************************************************************************************************/

/************************************************************************************************/

/************************************************************************************************/

/************************************************************************************************/
//BTError BluetoothInit(void)
//{
//	WSADATA WDat;
//
//	WORD Wd = MAKEWORD(2,2);
//	if(WSAStartup(Wd,&WDat)  == NO_ERROR)
//	{
//		printf("Wsa is %s\n",WDat.szSystemStatus);
//		return Succesful;
//	}
//	else
//	{
//      return NotInitialise
//
//		//closesocket(
//	}
//
//
//}
/************************************************************************************************/
BTError BluetoothClose(void)
{

	WSACleanup();
	return Succesful;

}
/************************************************************************************************/
EthError EthernetClose(void)
{

	WSACleanup();

	return EthError::Succesful;
}
/************************************************************************************************/
//BTError BluetoohFindVisibleDevices(void)
//{
//	
//   
//
//	return  BTError::Succes;
//
//}
/************************************************************************************************/
void BluetothServerMode(void)
{
   HANDLE Threads[4];
	 structured_task_group Tasks;
	
	SOCKET ServerConn;
	SOCKET ClientConn;
	sockaddr_in ServerAdress;
	sockaddr_in ClientAdress;
	EthError EER;
	Datagram Input;
	BYTE InputB[512];
	FunctionArray Userfunction;
	int ssize =0;
	BYTE* TMP =  new BYTE[512];
	Userfunction[0] = Byper;
	Userfunction[1] = PrintMessage;
	Userfunction[2] = PrintMessageAdd;
	Userfunction[4] = BotIndent;
	Userfunction[5] = SendString;
	auto TaskReceiveMessage = make_task([&Input,&ClientConn]()
	 {

		 ReceiveDatagram(ClientConn,Input);

	 }
	);


	auto TaskSendMessage = make_task([&Input,&ClientConn]()
	 {

		 SendDatagram(ClientConn,Input);

	 }
	);




	START:
	printf("Welcome to server mode");

	printf("Init stage 1\n");
	printf("*_ _ _ \n");

   EER = BluetoothServerConfig(ServerConn,20,ServerAdress);
	if(EER == EthError::Succesful)
	{

		printf("First stage completed!\n");
	}
	else
	{
		printf("Errror in this stage!\n");

	}

	printf("Init stage 2\n");

	EER =  EthernetTCPServerBind(ServerConn,ServerAdress);

	if(EER == EthError::Succesful)
	{

		printf("Second stage completed!\n");
	}
	else
	{
		printf("Errror in this stage!\n");

	}

	printf("Init stage 3 \n");


	EER = EthernetTCPServerListen(ServerConn,10);

	if(EER == EthError::Succesful)
	{

		printf("Second stage completed!\n");
	}
	else
	{
		printf("Errror in this stage!\n");

	};

ACEPTER:
do
	EER = EthernetTCPServerAccept(ServerConn,ClientConn,ClientAdress);
while(EER!= EthError::Succesful);


	if(EER == EthError::Succesful)
	{
		//getpeername(ClientConn,(sockaddr*)ClientAdress,ssize);

		printf("Connected client with IP %s!\n", inet_ntoa(ClientAdress.sin_addr));

		//FormatDatagramPacket(DataType::Test,
		
		//do
	////	{
PARS:
		Tasks.run_and_wait(TaskReceiveMessage);
		//ReceiveDatagram(ClientConn,Input);
			printf("Sender %i\n",Input.Header.Sender);
			if(Input.Header.Sender!=1)
			{
goto ACEPTER;

			}
			switch(Input.Data.DataBuffer[0])
			{
		
			case 0:
		CommandParcer(Input,Userfunction);
		goto  PARS;
		break;
		case 1:
		CommandParcer(Input,Userfunction);
		goto  PARS;
		break;
		case 2:
		CommandParcer(Input,Userfunction);
		memcpy(TMP,Input.Data.DataBuffer,512);
		FormatDatagramPacket(DataType::Test,Input,0,1,TMP,512);
		Tasks.run_and_wait(TaskSendMessage);
		//SendDatagram(ClientConn,Input);
		goto PARS;
		break;
		case 4:
			{
			printf("I am Bot?\n");
			CommandParcer(Input,Userfunction);
			
			memcpy(TMP,Input.Data.DataBuffer,512);
			FormatDatagramPacket(DataType::Test,Input,0,1,TMP,512);
			Tasks.run_and_wait(TaskSendMessage);
			//SendDatagram(ClientConn,Input);
			
			//memcpy(&Input.Data.DataBuffer[10],&ServerConn,sizeof(SOCKET));
			/*char BotHostName[128];
	 char ServerIP[60];
	 int Length = 60;
	 BYTE BotId = 1;
	
	 gethostname(BotHostName,Length);
	 size_t DataS = strlen(BotHostName);
	 Input.Data.DataBuffer[2] = BotId;
	 Input.Data.DataBuffer[3] = DataS;
	 memcpy((char*)&Input.Data.DataBuffer[4],&BotHostName,DataS);
	 SendDatagram(ClientConn,Input);
	 printf("Mashine Name = %s\n",BotHostName);*/
			//CommandParcer(Input,Userfunction);
	 	
	 //EER= ReceiveDatagram(
			goto PARS;
			break;
			}

		case 12:
			{
				TMP[0] = 12;
				TMP[1] = 2;

				memcpy(TMP,Input.Data.DataBuffer,512);
			FormatDatagramPacket(DataType::Test,Input,0,1,TMP,512);
			Tasks.run_and_wait(TaskSendMessage);



			} goto PARS;
			break;

		case 6:
			printf("Client want close server");
			goto EXIT;
	break;

		default:
			goto EXIT;
			break;
			}
		//}
		//while(Input.Header.Sender != DataType::Test);
	
		
	

	}
	else
	{
		printf("Client not connect \n");

	};
EXIT:
	delete[] TMP;
	EthernetCloseSocket(ClientConn);

	EthernetCloseSocket(ServerConn);


   


};
/************************************************************************************************/

BTError BluetoothClose(SOCKET& Connector)
{

  return  EthernetCloseSocket(Connector);
   

};
/************************************************************************************************/
BTError BluetoothServerConfig(SOCKET& Connector,INT Port, sockaddr_in& SAdrres)
{
      EthernetInit();
      //Connector = socket(AF_BTH,SOCK_STREAM,IPPROTO_);
		 
       //SAdrres.sin_family =  AF_BTH;
		 SAdrres.sin_addr.s_addr = htonl(INADDR_ANY); 
		 SAdrres.sin_port = htons(Port);

		return EthError::Succesful;


}
/************************************************************************************************/
//BTError BluetoothServerListen(SOCKET& Connector,INT Backlog)
//{
//
//   EthernetTCPServerListen(Connector,Backlog);
//
//}
///************************************************************************************************/
//BTError BluetoothServerBind(SOCKET& Connector,sockaddr_in&  ServerAdress)
//{
//
//
//   EthernetTCPServerBind(Connector,ServerAdress);
//
//
//}
/************************************************************************************************/






/************************************************************************************************/



 /************************************************************************************************/
void Tests(void)
{

	char  N[2]  = {'�','�'};

	

	
	////Head* Demo  = NULL;


	HeadS h;

	CFS H;
	int i,j;

	i = 2;
	j=3;
//auto gg = 2;
auto g =[&i,&j]()->int{


return i+j;
};


auto Length = [&i,&j]()->float{

	if(i>j)
	{
	return i - j;
	}
	else
	{

return i;
	}

	
};


int  resik = 0;
//int  ress =  g();
float l = 0;  //Length();
int  ress = 0;

parallel_invoke(  [&] {ress =  g();},
	[&]{ l = Length();}
);
printf("%i\n   %f\n",ress,l);
memset(&h,0,sizeof(HeadS));

	memset(&H,0,sizeof(CFS));


	memcpy(h.Name,N,(sizeof(char) *2));


	memcpy(H.Name,h.Name,20);





	printf("��� � ��������� ���� %s\n",H.Name);


	SOCKET BlutoothConector;
SOCKET EthernetConnector;
sockaddr_in EthernetConnAdress;
//SOCKADDR_BTH BluetothAdress;
	//FILE* File;
	
	INT TSTS = 55555555;
	INT XTXX= 10000;

	int I1 = 0;
	int I2 = 0;
	float TTS = 12.450;
	float TT = 0.0;
	int T = 0;
	const int size = sizeof(INT) * 2;
	const int ofsett = sizeof(int) ;
	BYTE* DSS = new  BYTE[size];
	BYTE* FSS = new BYTE[sizeof(FLOAT)];
	FloatToByte(TTS,FSS);
	IntToByte(TSTS,&DSS[0]);
	IntToByte(XTXX,&DSS[ofsett]);

	EthError EER = EthError::BadResult;
	ByteToInt(DSS,&I1);
	ByteToInt(&DSS[ofsett],&I2);
	ByteToFloat(FSS,&TT);
	printf("After Copy %i\n",I1);
	printf("After Copy %i\n",I2);
	printf("After Copy %f\n",TT);



	BYTE Data[512];

	for(short i = 0;i<512;i++)
	{

		Data[i] = i;


	}


	Datagram Out;
	memset(&Out,0,sizeof(Datagram));
	Datagram In;
	FormatDatagramPacket(DataType::Test,In,(BYTE*)&Data,512);
	SOCKET Server;
	sockaddr_in ServerAddres;

	FormatDatagramPacket(DataType::Instant,In,12,2,(BYTE*)&Data,512);
	//EthernetInit();
	EER = EthernetTCPServerConfig(Server,28,ServerAddres);


	EER = EthernetTCPServerBind(Server,ServerAddres);
	EER = EthernetTCPServerListen(Server,1);
	EER = 	EthernetTCPClientConfig( EthernetConnector,28,"127.0.0.1");
	EER = SendDatagram(EthernetConnector,In);
	//EER = ReceiveDatagram(EthernetConnector,Out);

	

	EthernetClose();

	//BluetoothInit();
	if(In.Header.DataType == Out.Header.DataType)
	{
		printf("Succesful transer\n");

	}


	//BluetoothClose();

	Datagram DataG;
	Datagram DataOut;
	












	BYTE Datain[sizeof(Datagram)];
	BYTE Datain1[sizeof(Datagram)];
	memset(&DataG,0,sizeof(Datagram));
	memset(&DataOut,0,sizeof(Datagram));
	FormatHeader(DataType::Test,DataG);
	///memset(&DataG,0,sizeof(Datagram));
	printf("- - - - - - - - -  Source Datagram  - - - - - - - - - - -\n");

		printf("Data Type %i\n",DataG.Header.DataType);
		printf("Data Sender %x\n",DataG.Header.Sender);
		printf("Data Receiver %x\n",DataG.Header.Receiver);
		printf("Header size %i\n",DataG.Header.HSize);
		printf("Data size %i\n",DataG.Data.DataSize);
		printf("Total  Data size %i\n",DataG.DSize);
		printf("TIME STAMP  %i h : %i m : %i d : %i wd  :\n",DataG.Header.Stamp[0],DataG.Header.Stamp[1], DataG.Header.Stamp[2],  DataG.Header.Stamp[3]  );
		printf("CRC Header %x\n",DataG.Header.CRC);

		



		memcpy(Datain,&DataG,sizeof(Datagram));


		memcpy(&DataOut,Datain,sizeof(Datagram));
		//Datain = (BYTE*)&DataG;
		//DataOut = (Datagram*)&Datain;

		memset(&DataG,0,sizeof(Datagram));
			printf("- - - - - - - - -  Out Datagram  - - - - - - - - - - -\n");
		

		printf("Data Type %i\n",DataOut.Header.DataType);
		printf("Data Sender %x\n",DataOut.Header.Sender);
		printf("Data Receiver %x\n",DataOut.Header.Receiver);
		printf("Header size %i\n",DataOut.Header.HSize);
		printf("Data size %i\n",DataOut.Data.DataSize);
		printf("Total  Data size %i\n",DataOut.DSize);
		//printf("TIME STAMP  %i h : %i m : %i d : %i wd  :\n",DataG.Head.Stamp[0],DataG.Head.Stamp[1], DataG.Head.Stamp[2],  DataG.Head.Stamp[3]  );
		//printf("CRC Header %x\n",DataG.Head.CRC);
		//delete DataOut;
		///delete[] Datain;

		FormatDatagramPacket(DataType::Test,DataG,(BYTE*)&Data,512);

		printf("- - - - - - - - - Format Out Datagram  - - - - - - - - - - -\n");
		printf("Data Type %i\n",DataG.Header.DataType);
		printf("Data Sender %x\n",DataG.Header.Sender);
		printf("Data Receiver %x\n",DataG.Header.Receiver);
		printf("Header size %i\n",DataG.Header.HSize);
		printf("Data size %i\n",DataG.Data.DataSize);
		printf("Total  Data size %i\n",DataG.DSize);
		printf("TIME STAMP  %i h : %i m : %i d : %i wd  :\n",DataG.Header.Stamp[0],DataG.Header.Stamp[1], DataG.Header.Stamp[2],  DataG.Header.Stamp[3]  );
		printf("CRC Header %x\n",DataG.Header.CRC);
		

		
		
	for(short i = 0;i<5;i++)
	{

	printf("DT[%i] = %i\n",i,DataG.Data.DataBuffer[i]);


	}
	delete[] DSS;

		system("pause");


}
/************************************************************************************************/


void ServerModeMultiThread(void)
{
	HANDLE ThreadHandle; // ��������� �������
	DWORD ThreadId;	 //

	///HANDLE Threads[4];
	 //structured_task_group Tasks;
	
	SOCKET ServerConn;
	SOCKET ClientConn;
	sockaddr_in ServerAdress;
	sockaddr_in ClientAdress;
	EthError EER;
	Datagram Input;
	///BYTE InputB[512];
	//FunctionArray Userfunction;
	/*int ssize =0;
	BYTE* TMP =  new BYTE[512];
	Userfunction[0] = Byper;
	Userfunction[1] = PrintMessage;
	Userfunction[2] = PrintMessageAdd;
	Userfunction[4] = BotIndent;
	Userfunction[5] = SendString;*/
	/*auto TaskReceiveMessage = make_task([&Input,&ClientConn]()
	 {

		 ReceiveDatagram(ClientConn,Input);

	 }
	);


	auto TaskSendMessage = make_task([&Input,&ClientConn]()
	 {

		 SendDatagram(ClientConn,Input);

	 }
	);*/




	START:
	printf("Welcome to server mode multithread\n");

	printf("Init stage 1\n");
	printf("*_ _ _ \n");

	EER =  EthernetTCPServerConfig(ServerConn,20,ServerAdress);
	if(EER == EthError::Succesful)
	{

		printf("First stage completed!\n");
	}
	else
	{
		printf("Errror in this stage!\n");

	}

	printf("Init stage 2\n");

	EER =  EthernetTCPServerBind(ServerConn,ServerAdress);

	if(EER == EthError::Succesful)
	{

		printf("Second stage completed!\n");
	}
	else
	{
		printf("Errror in this stage!\n");

	}

	printf("Init stage 3 \n");


	EER = EthernetTCPServerListen(ServerConn,10);

	if(EER == EthError::Succesful)
	{

		printf("Second stage completed!\n");
	}
	else
	{
		printf("Errror in this stage!\n");

	};

ACEPTER:
do
	EER = EthernetTCPServerAccept(ServerConn,ClientConn,ClientAdress);
while(EER!= EthError::Succesful);


	if(EER == EthError::Succesful)
	{

		ThreadHandle = 	CreateThread(NULL,NULL,AcceptServerFunc,(LPVOID)ClientConn,NULL,&ThreadId);
		//getpeername(ClientConn,(sockaddr*)ClientAdress,ssize);

		printf("Connected client with IP %s!\n", inet_ntoa(ClientAdress.sin_addr));

	
		goto ACEPTER;

		//FormatDatagramPacket(DataType::Test,
		
		//do
	////	{

		EXIT:
	EthernetCloseSocket(ServerConn);
	

}
	}
/************************************************************************************************/

 DWORD WINAPI AcceptServerFunc(LPVOID Socket)
 {

	 //SOCKET ClientConn = (SOCKET)&Socket;
	sockaddr_in ServerAdress;
	sockaddr_in ClientAdress;
	EthError EER;
	Datagram Input;
	BYTE InputB[512];
	HANDLE HeartBeatfunc;
	FunctionArray Userfunction;
	int ssize =0;
	BYTE* TMP =  new BYTE[512];

	//CRITICAL_SECTION CS;
	//InitializeCriticalSection(&CS);
	Userfunction[0] = Byper;
	Userfunction[1] = PrintMessage;
	Userfunction[2] = PrintMessageAdd;
	Userfunction[4] = BotIndent;
	Userfunction[5] = SendString;
	

PARS:
		////Tasks.run_and_wait(TaskReceiveMessage);
		//ReceiveDatagram(ClientConn,Input);
	
	ReceiveDatagram((SOCKET&)Socket,Input);
			//printf("Sender %i\n",Input.Header.Sender);
			
			switch(Input.Data.DataBuffer[0])
			{
		
		case 9:
			{
		CommandParcer(Input,Userfunction);
		goto PARS;
			}
		break;

		case 1:
			{
		CommandParcer(Input,Userfunction);
		goto  PARS;
			}
		break;

		case 2:
			{
		CommandParcer(Input,Userfunction);
		memcpy(TMP,Input.Data.DataBuffer,512);
		FormatDatagramPacket(DataType::Test,Input,0,1,TMP,512);
		SendDatagram((SOCKET&)Socket,Input);

		//Tasks.run_and_wait(TaskSendMessage);
		//SendDatagram(ClientConn,Input);
		goto PARS;
			}
		break;

		case 4:
			{
			//printf("I am Bot?\n");
			CommandParcer(Input,Userfunction);
			
			memcpy(TMP,Input.Data.DataBuffer,512);
			FormatDatagramPacket(DataType::Test,Input,0,1,TMP,512);
			SendDatagram((SOCKET&)Socket,Input);

			///Tasks.run_and_wait(TaskSendMessage);
			//SendDatagram(ClientConn,Input);
			
			//memcpy(&Input.Data.DataBuffer[10],&ServerConn,sizeof(SOCKET));
			/*char BotHostName[128];
	 char ServerIP[60];
	 int Length = 60;
	 BYTE BotId = 1;
	
	 gethostname(BotHostName,Length);
	 size_t DataS = strlen(BotHostName);
	 Input.Data.DataBuffer[2] = BotId;
	 Input.Data.DataBuffer[3] = DataS;
	 memcpy((char*)&Input.Data.DataBuffer[4],&BotHostName,DataS);
	 SendDatagram(ClientConn,Input);
	 printf("Mashine Name = %s\n",BotHostName);*/
			//CommandParcer(Input,Userfunction);
	 	
	 //EER= ReceiveDatagram(
			goto PARS;
			break;
			}
		case 6:
			//printf("Client want close server");
			goto EXIT;
	           break;

		case 12:
			{
				TMP[0] = 12;
				TMP[1] = 2;

				memcpy(TMP,Input.Data.DataBuffer,512);
			FormatDatagramPacket(DataType::Test,Input,0,1,TMP,512);
			SendDatagram((SOCKET&)Socket,Input);
		

			}goto PARS;
			break;

		default:
		goto PARS;
			break;
			}
		//}
		//while(Input.Header.Sender != DataType::Test);
	
		
	

	
EXIT:
	delete[] TMP;
	EthernetCloseSocket((SOCKET&)Socket );
	//DeleteCriticalSection(&CS);
	return 0;






 };




/************************************************************************************************/
void ServerMode(void)
{
	HANDLE Threads[4];
	 structured_task_group Tasks;
	
	SOCKET ServerConn;
	SOCKET ClientConn;
	sockaddr_in ServerAdress;
	sockaddr_in ClientAdress;
	EthError EER;
	Datagram Input;
	BYTE InputB[512];
	FunctionArray Userfunction;
	int ssize =0;
	BYTE* TMP =  new BYTE[512];
	Userfunction[0] = Byper;
	Userfunction[1] = PrintMessage;
	Userfunction[2] = PrintMessageAdd;
	Userfunction[4] = BotIndent;
	Userfunction[5] = SendString;
	auto TaskReceiveMessage = make_task([&Input,&ClientConn]()
	 {

		 ReceiveDatagram(ClientConn,Input);

	 }
	);


	auto TaskSendMessage = make_task([&Input,&ClientConn]()
	 {

		 SendDatagram(ClientConn,Input);

	 }
	);




	START:
	printf("Welcome to server mode");

	printf("Init stage 1\n");
	printf("*_ _ _ \n");

	EER =  EthernetTCPServerConfig(ServerConn,20,ServerAdress);
	if(EER == EthError::Succesful)
	{

		printf("First stage completed!\n");
	}
	else
	{
		printf("Errror in this stage!\n");

	}

	printf("Init stage 2\n");

	EER =  EthernetTCPServerBind(ServerConn,ServerAdress);

	if(EER == EthError::Succesful)
	{

		printf("Second stage completed!\n");
	}
	else
	{
		printf("Errror in this stage!\n");

	}

	printf("Init stage 3 \n");


	EER = EthernetTCPServerListen(ServerConn,10);

	if(EER == EthError::Succesful)
	{

		printf("Second stage completed!\n");
	}
	else
	{
		printf("Errror in this stage!\n");

	};

ACEPTER:
do
	EER = EthernetTCPServerAccept(ServerConn,ClientConn,ClientAdress);
while(EER!= EthError::Succesful);


	if(EER == EthError::Succesful)
	{
		//getpeername(ClientConn,(sockaddr*)ClientAdress,ssize);

		printf("Connected client with IP %s!\n", inet_ntoa(ClientAdress.sin_addr));

		//FormatDatagramPacket(DataType::Test,
		
		//do
	////	{
PARS:
		Tasks.run_and_wait(TaskReceiveMessage);
		//ReceiveDatagram(ClientConn,Input);
			printf("Sender %i\n",Input.Header.Sender);
			if(Input.Header.Sender!=1)
			{
goto ACEPTER;

			}
			switch(Input.Data.DataBuffer[0])
			{
		
			case 0:
		CommandParcer(Input,Userfunction);
		goto  PARS;
		break;
		case 1:
		CommandParcer(Input,Userfunction);
		goto  PARS;
		break;
		case 2:
		CommandParcer(Input,Userfunction);
		memcpy(TMP,Input.Data.DataBuffer,512);
		FormatDatagramPacket(DataType::Test,Input,0,1,TMP,512);
		Tasks.run_and_wait(TaskSendMessage);
		//SendDatagram(ClientConn,Input);
		goto PARS;
		break;
		case 4:
			{
			printf("I am Bot?\n");
			CommandParcer(Input,Userfunction);
			
			memcpy(TMP,Input.Data.DataBuffer,512);
			FormatDatagramPacket(DataType::Test,Input,0,1,TMP,512);
			Tasks.run_and_wait(TaskSendMessage);
			//SendDatagram(ClientConn,Input);
			
			//memcpy(&Input.Data.DataBuffer[10],&ServerConn,sizeof(SOCKET));
			/*char BotHostName[128];
	 char ServerIP[60];
	 int Length = 60;
	 BYTE BotId = 1;
	
	 gethostname(BotHostName,Length);
	 size_t DataS = strlen(BotHostName);
	 Input.Data.DataBuffer[2] = BotId;
	 Input.Data.DataBuffer[3] = DataS;
	 memcpy((char*)&Input.Data.DataBuffer[4],&BotHostName,DataS);
	 SendDatagram(ClientConn,Input);
	 printf("Mashine Name = %s\n",BotHostName);*/
			//CommandParcer(Input,Userfunction);
	 	
	 //EER= ReceiveDatagram(
			goto PARS;
			break;
			}

		case 12:
			{
				TMP[0] = 12;
				TMP[1] = 2;

				memcpy(TMP,Input.Data.DataBuffer,512);
			FormatDatagramPacket(DataType::Test,Input,0,1,TMP,512);
			Tasks.run_and_wait(TaskSendMessage);



			} goto PARS;
			break;

		case 6:
			printf("Client want close server");
			goto EXIT;
	break;

		default:
			goto EXIT;
			break;
			}
		//}
		//while(Input.Header.Sender != DataType::Test);
	
		
	

	}
	else
	{
		printf("Client not connect \n");

	};
EXIT:
	delete[] TMP;
	EthernetCloseSocket(ClientConn);

	EthernetCloseSocket(ServerConn);






}

/************************************************************************************************/
void ClientMode(void)
{

	sockaddr_in ClentAdress;
	sockaddr_in* ServerAddeses = NULL;
	int BotsInThisWorkCounter = 0;
	SOCKET Clients;
	EthError ERR;
	Datagram Out;
	Datagram OutServer;
	Datagram ShowMes;
		BYTE DS[512];
		char IP[128];
	DS[0] = 0;
	DS[1] = 100;
	char* CurrentIP;
	int CurrentCommand = 0;
	memset(&OutServer,0,sizeof(Datagram));
	memset(&ShowMes,0,sizeof(Datagram));

	 structured_task_group Tasks;

	 auto TaskReceiveMessage = make_task([&OutServer,&Clients]()
	 {

		 ReceiveDatagram(Clients,OutServer);

	 }
	);


	auto TaskSendMessage = make_task([&Out,&Clients]()
	 {

		 SendDatagram(Clients,Out);

	 }
	);









	int Swither = 0;
	for( int i = 2 ;i<512;i++)
	{
		DS[i]= 0;



	};
	if(CurrentCommand!=8)
	{
		printf("Welcome to clien mode\n");

		printf("Entert target address:");
		scanf("%s",&IP);

		printf("Connect to LocalHost\n");
	}
		ERR = 	EthernetTCPClientConfig(Clients,20,IP);
		if(ERR == EthError::Succesful)
		{
			printf("Succes Conect\n");


		}
		
		else
		{

			printf("Not Conect\n");



		}


		
		STAGE:
		printf("Command\n   1- Test Command\n   2 - Compute on server\n  4 - BOT INDENT \n 6- Close Server and Client \n 5 -Send String to Server " );
		printf("Enter comamnd number!\n"); 
		scanf("%i",&Swither);
		switch(Swither)
		{

		case 6:
			goto EXIT;
			break;
		case 1:
			{
			DS[0] = 1;
			DS[1] = 2;
			FormatDatagramPacket(DataType::Test,Out,1,2,DS,512);
			Tasks.run_and_wait(TaskSendMessage);
		     //ERR = 	SendDatagram(Clients,Out);	
			 goto STAGE;
			 break;
			}
		case 2:
			{
			int Aw,Bw;
			DS[0] =2;
			printf("Enter A:");
			scanf("%i",&Aw);
			printf("Enter B:");
			scanf("%i",&Bw);
			IntToByte(Aw,&DS[2]);
			IntToByte(Bw,&DS[(2 + sizeof(int))]);
			FormatDatagramPacket(DataType::Test,Out,1,2,DS,512);
			Tasks.run_and_wait(TaskSendMessage);
			Tasks.run_and_wait(TaskReceiveMessage);
			//ERR = 	SendDatagram(Clients,Out);
			//ERR = 	ReceiveDatagram(Clients,OutServer);
			int res =0;
			ByteToInt(&OutServer.Data.DataBuffer[10],&res);
			printf("Resulst %i + %i = %i\n",Aw,Bw,res);
			goto STAGE;
			break;
			}
		case 7:
			{
				//
			DS[0] =7;
			printf("Close Server and Client!\n");
			FormatDatagramPacket(DataType::Test,Out,1,2,DS,512);
			Tasks.run_and_wait(TaskSendMessage);
			//ERR = 	SendDatagram(Clients,Out);
			goto EXIT;
			}

			case 4:
				{
					//������� ��� 
			char BotStation[128]; ;
			memset(BotStation,0,(128 * sizeof(char)));
			DS[0] = 4;
			DS[1] = 2;
			FormatDatagramPacket(DataType::Test,Out,1,2,DS,512);
			//memset(&OutServer,0,sizeof(Datagram));
		 Tasks.run_and_wait(TaskSendMessage);
			Tasks.run_and_wait(TaskReceiveMessage);
		     	//SendDatagram(Clients,Out);
			 	//ReceiveDatagram(Clients,OutServer);
				size_t SSize = OutServer.Data.DataBuffer[3];
				//BotStation = new char[SSize];
				//memset(BotStation,0,(SSize * sizeof(char)));
				memcpy(&BotStation,(char*)&OutServer.Data.DataBuffer[4],SSize);
	// SendDatagram(ClientConn,Input);
				printf("Mashine Name = %s\n",BotStation);
				printf("Name lenght %i chars\n",SSize);
				//delete[] BotStation;
			// memcpy(&BotStation,&OutServer.Data.DataBuffer[4],(size_t)&OutServer.Data.DataBuffer[3]);
			// printf("Bot Host %s\n",BotStation);

				goto STAGE;
			 break;
				}
			case 8:
				{
					//����� ��������
			printf("Client Search bots in lan\n");
			 DS[0] =8;
			FormatDatagramPacket(DataType::Test,Out,1,2,DS,512);
			//ERR = 	SendDatagram(Clients,Out);
			
			Tasks.run_and_wait(TaskSendMessage);	


			 break;
				}
		
		
		
		
		}







	


		/*do
		{
		ERR = 	SendDatagram(Clients,Out);	
		}
		while(ERR ==  EthError::SendSuccesful);*/
		EXIT:
		EthernetCloseSocket(Clients);
		

}
/************************************************************************************************/

void ClientModeMultiThread(void)
{


	DWORD Thid = 0;
	sockaddr_in ClentAdress;
	sockaddr_in* ServerAddeses = NULL;
	int BotsInThisWorkCounter = 0;
	SOCKET Clients;
	EthError ERR;
	Datagram Out;
	Datagram OutServer;
	Datagram ShowMes;
		BYTE DS[512];
		char IP[128];
	DS[0] = 0;
	DS[1] = 100;
	char* CurrentIP;
	int CurrentCommand = 0; 
	HANDLE HeartBeatThread;
	memset(&OutServer,0,sizeof(Datagram));
	memset(&ShowMes,0,sizeof(Datagram));

	// structured_task_group Tasks;

	// auto TaskReceiveMessage = make_task([&OutServer,&Clients]()
	// {

	//	 ReceiveDatagram(Clients,OutServer);

	// }
	//);


	//auto TaskSendMessage = make_task([&Out,&Clients]()
	// {

	//	 SendDatagram(Clients,Out);

	// }
	//);









	int Swither = 0;
	for( int i = 2 ;i<512;i++)
	{
		DS[i]= 0;



	};
	if(CurrentCommand!=8)
	{
		printf("Welcome to clien mode\n");

		printf("Entert target address:");
		scanf("%s",&IP);

		printf("Connect to LocalHost\n");
	}
		ERR = 	EthernetTCPClientConfig(Clients,20,IP);
		if(ERR == EthError::Succesful)
		{
			printf("Succes Conect\n");


		}
		
		else
		{

			printf("Not Conect\n");



		}
		TimerSoc = Clients;
		HeartBeatThread = CreateThread(NULL,NULL,HeartBeatFunc,(LPVOID)1,NULL,&Thid);
		
		STAGE:
		printf("Command\n   1- Test Command\n   2 - Compute on server\n  4 - BOT INDENT \n 6- Close Server and Client \n 5 -Send String to Server " );
		printf("Enter comamnd number!\n"); 
		scanf("%i",&Swither);
		switch(Swither)
		{

		case 6:
			goto EXIT;
			break;
		case 1:
			{
			DS[0] = 1;
			DS[1] = 2;
			FormatDatagramPacket(DataType::Test,Out,1,2,DS,512);
			SendDatagram(Clients,Out);
			///Tasks.run_and_wait(TaskSendMessage);
		     //ERR = 	SendDatagram(Clients,Out);	
			 goto STAGE;
			 break;
			}
		case 2:
			{
			int Aw,Bw;
			DS[0] =2;
			printf("Enter A:");
			scanf("%i",&Aw);
			printf("Enter B:");
			scanf("%i",&Bw);
			IntToByte(Aw,&DS[2]);
			IntToByte(Bw,&DS[(2 + sizeof(int))]);
			FormatDatagramPacket(DataType::Test,Out,1,2,DS,512);
			///Tasks.run_and_wait(TaskSendMessage);
			 SendDatagram(Clients,Out);
			//Tasks.run_and_wait(TaskReceiveMessage);
			  ReceiveDatagram(Clients,OutServer);
			//ERR = 	SendDatagram(Clients,Out);
			//ERR = 	ReceiveDatagram(Clients,OutServer);
			int res =0;
			ByteToInt(&OutServer.Data.DataBuffer[10],&res);
			printf("Resulst %i + %i = %i\n",Aw,Bw,res);
			goto STAGE;
			break;
			}
		case 7:
			{
				//
			DS[0] =7;
			printf("Close Server and Client!\n");
			FormatDatagramPacket(DataType::Test,Out,1,2,DS,512);
			 SendDatagram(Clients,Out);
			///Tasks.run_and_wait(TaskSendMessage);
			//ERR = 	SendDatagram(Clients,Out);
			goto EXIT;
			}

			case 4:
				{
					//������� ��� 
			char BotStation[128]; ;
			memset(BotStation,0,(128 * sizeof(char)));
			DS[0] = 4;
			DS[1] = 2;
			FormatDatagramPacket(DataType::Test,Out,1,2,DS,512);
			//memset(&OutServer,0,sizeof(Datagram));
		 //Tasks.run_and_wait(TaskSendMessage);
			 SendDatagram(Clients,Out);
		  ReceiveDatagram(Clients,OutServer);
			//Tasks.run_and_wait(TaskReceiveMessage);
		     	//SendDatagram(Clients,Out);
			 	//ReceiveDatagram(Clients,OutServer);
				size_t SSize = OutServer.Data.DataBuffer[3];
				//BotStation = new char[SSize];
				//memset(BotStation,0,(SSize * sizeof(char)));
				memcpy(&BotStation,(char*)&OutServer.Data.DataBuffer[4],SSize);
	// SendDatagram(ClientConn,Input);
				printf("Mashine Name = %s\n",BotStation);
				printf("Name lenght %i chars\n",SSize);
				//delete[] BotStation;
			// memcpy(&BotStation,&OutServer.Data.DataBuffer[4],(size_t)&OutServer.Data.DataBuffer[3]);
			// printf("Bot Host %s\n",BotStation);

				goto STAGE;
			 break;
				}
			case 8:
				{
					//����� ��������
			printf("Client Search bots in lan\n");
			 DS[0] =8;
			FormatDatagramPacket(DataType::Test,Out,1,2,DS,512);
			//ERR = 	SendDatagram(Clients,Out);
			 SendDatagram(Clients,Out);
			//Tasks.run_and_wait(TaskSendMessage);	


			 break;
				}
		
		
		
		
		}







	


		/*do
		{
		ERR = 	SendDatagram(Clients,Out);	
		}
		while(ERR ==  EthError::SendSuccesful);*/
		EXIT:
		EthernetCloseSocket(Clients);
		



}


/************************************************************************************************/
 DWORD WINAPI HeartBeatFunc(LPVOID TimerId)
 {



	   int Counter=0;
      MSG Msg;
      UINT TimerIds = SetTimer(NULL, 0, 5000, &TimerProc);

	  
    
	
	  //UINT TimerId1 = SetTimer(NULL, 1, 50, &TimerProc1);
	  bool ih = false;
     //cout << "TimerId: " << TimerId << '\n';
	  
      while (GetMessage(&Msg, NULL, 0, 0)) {
        ++Counter;
      if (Msg.message == WM_TIMER)
		  ih = false;
        ///cout << "Counter: " << Counter << "; timer message\n";
      else
		  ih = true;
       /// cout << "Counter: " << Counter << "; message: " << Msg.message << '\n';
      DispatchMessage(&Msg);
    }
  KillTimer(NULL, TimerIds);


	 return 0;
 }



/************************************************************************************************/


 VOID CALLBACK TimerProc(
     HWND hwnd,
    UINT uMsg,
   UINT_PTR idEvent,
    DWORD dwTime
)
 {
	 BYTE DS[512];
	 Datagram Out;
	 Datagram Serv;
	 ///char BotStation[128]; ;
			///memset(BotStation,0,(128 * sizeof(char)));
			DS[0] = 12;
			DS[1] = 2;
		
			FormatDatagramPacket(DataType::Test,Out,1,2,DS,512);
			//memset(&OutServer,0,sizeof(Datagram));
		 //Tasks.run_and_wait(TaskSendMessage);
			SendDatagram(TimerSoc,Out);
			ReceiveDatagram(TimerSoc,Serv);

			if(Serv.Data.DataBuffer[1] == 2)
			{

				printf("Server Online\n");
			}

			else
			{
               printf("Server Offline\n");
			}

 };

 /************************************************************************************************/


void ModeSwither(void)
{
	int ST;
	printf("Enter programm mode\n");
	printf(" 1- TCP Server\n 0 - TCP Client\n 3- TCP Multithread Server\n 4- TCP MultiThread Client\n");
	printf("\n");

	scanf("%i",&ST);

	switch(ST)
	{
	case 1:
		ServerMode();
		Sleep(100);
		exit(0);
		break;

	case 0:
		ClientMode();
		break;
	case 3:
ServerModeMultiThread();
exit(0);
	case 4:
		ClientModeMultiThread();
		break;
	}

}

/************************************************************************************************/

void CommandParcer(Datagram& Datain,FunctionArray&   Functions)
{    
	
	int funtionNumber = Datain.Data.DataBuffer[0];
	size_t FunctionParametrs = Datain.Data.DataBuffer[1];
	BYTE* FunctParam = Datain.Data.DataBuffer;
	Functions[funtionNumber](FunctParam,FunctionParametrs);
	//FormatDatagramPacket(DataType::Test,Datain,0,1,FunctParam,512);
	
};


/************************************************************************************************/
 int  Byper(BYTE* InParams,size_t Count)
 {
	 printf("FUNC NAME\n");
	 Beep(1000,10);

	 return 0;
 }

/************************************************************************************************/
 int  PrintMessage(BYTE* InParams,size_t Count)
 {
	 printf("FUNC NAME\n");
	 //Beep(1000,10);

	 return 0;
 }

/************************************************************************************************/
  int  PrintMessageAdd(BYTE* InParams,size_t Count)
 {
	 int A = 0;
	 int B = 0;

	 int S = 0;
	 memcpy(&A,&InParams[2],sizeof(INT));
	 memcpy(&B,&InParams[6],sizeof(INT));

	 S = A + B;
	 memcpy(&InParams[10],&S,sizeof(int));
	 printf("Result %i + %i = % i  \n",A,B,S);
	 //Beep(1000,10);

	 return 0;
 }
/************************************************************************************************/
  int  BotIndent(BYTE* InParams,size_t Count)
  {
	 /*har* BotHostName = NULL;
	 char* ServerIP;
	 int Length = 60;
	 BYTE BotId = 1;
	


	 gethostname(BotHostName,Length);
	 size_t DataS = Length * sizeof(char);
	 InParams[2] = BotId;
	 InParams[3] = DataS;

	 memcpy(&InParams[4],&BotHostName,DataS);*/




	 char BotHostName[128];
	 char ServerIP[60];
	 int Length = 60;
	 BYTE BotId = 1;
	
	 gethostname(BotHostName,Length);
	 size_t DataS = strlen(BotHostName);
	 InParams[2] = BotId;
	 InParams[3] = DataS;
	 memcpy((char*)&InParams[4],&BotHostName,DataS);
	// SendDatagram(ClientConn,Input);
	 printf("Mashine Name = %s\n",BotHostName);



	






	  return 0;
  }

/************************************************************************************************/
  int  SendString(BYTE* InParams,size_t Count)
  {




	  return 0;
  }


/************************************************************************************************/
   int  BotSearch(BYTE* InParams,size_t Count)
   {
	   SOCKET Device;

	   sockaddr_in BotAddres;

	   memcpy(&Device,&InParams[2],sizeof(SOCKET));


	   return 0;
   }